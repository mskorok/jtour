<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="sfGuardUserProfile")
 * @Entity
 */
class Sfguarduserprofile
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @Column(name="middle", type="string", length=45, nullable=true)
     */
    private $middle;

    /**
     * @var string
     *
     * @Column(name="prefix", type="string", length=45, nullable=true)
     */
    private $prefix;

    /**
     * @var string
     *
     * @Column(name="suffix", type="string", length=45, nullable=true)
     */
    private $suffix;

    /**
     * @var \DateTime
     *
     * @Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var integer
     *
     * @Column(name="salutation_id", type="integer", nullable=true)
     */
    private $salutationId;

    /**
     * @var string
     *
     * @Column(name="addr_street", type="string", length=100, nullable=true)
     */
    private $addrStreet;

    /**
     * @var string
     *
     * @Column(name="addr_street2", type="string", length=100, nullable=true)
     */
    private $addrStreet2;

    /**
     * @var string
     *
     * @Column(name="addr_region", type="string", length=100, nullable=true)
     */
    private $addrRegion;

    /**
     * @var string
     *
     * @Column(name="addr_locality", type="string", length=100, nullable=true)
     */
    private $addrLocality;

    /**
     * @var string
     *
     * @Column(name="addr_postalcode", type="string", length=45, nullable=true)
     */
    private $addrPostalcode;

    /**
     * @var string
     *
     * @Column(name="addr_country", type="string", length=100, nullable=true)
     */
    private $addrCountry;

    /**
     * @var string
     *
     * @Column(name="tel_work", type="string", length=45, nullable=true)
     */
    private $telWork;

    /**
     * @var string
     *
     * @Column(name="tel_cell", type="string", length=45, nullable=true)
     */
    private $telCell;

    /**
     * @var string
     *
     * @Column(name="tel_car", type="string", length=45, nullable=true)
     */
    private $telCar;

    /**
     * @var string
     *
     * @Column(name="tel_pager", type="string", length=45, nullable=true)
     */
    private $telPager;

    /**
     * @var string
     *
     * @Column(name="tel_home", type="string", length=45, nullable=true)
     */
    private $telHome;

    /**
     * @var string
     *
     * @Column(name="tel_fax_home", type="string", length=45, nullable=true)
     */
    private $telFaxHome;

    /**
     * @var string
     *
     * @Column(name="tel_over", type="string", length=45, nullable=true)
     */
    private $telOver;

    /**
     * @var string
     *
     * @Column(name="tel_prefer", type="string", length=45, nullable=true)
     */
    private $telPrefer;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="login_from", type="string", length=16, nullable=true)
     */
    private $loginFrom;

    /**
     * @var \DateTime
     *
     * @Column(name="password_changed_at", type="datetime", nullable=false)
     */
    private $passwordChangedAt;

    /**
     * @var \DateTime
     *
     * @Column(name="expires_at", type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ManyToMany(targetEntity="Domain", mappedBy="profile")
     */
    private $domain;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ManyToMany(targetEntity="Project", mappedBy="profile")
     */
    private $project;

    /**
     * @var \export\Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \export\SfGuardUser
     *
     * @ManyToOne(targetEntity="SfGuardUser")
     * @JoinColumns({
     *   @JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->domain = new \Doctrine\Common\Collections\ArrayCollection();
        $this->project = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
