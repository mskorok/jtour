<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="category")
 * @Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var \export\Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    function __get($name) {
        return $this->$name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \export\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


}
