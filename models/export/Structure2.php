<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="structure2")
 * @Entity
 */
class Structure2
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="lft", type="integer", nullable=false)
     */
    private $lft;

    /**
     * @var integer
     *
     * @Column(name="rgt", type="integer", nullable=false)
     */
    private $rgt;

    /**
     * @var integer
     *
     * @Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="full_title", type="string", length=255, nullable=true)
     */
    private $fullTitle;

    /**
     * @var string
     *
     * @Column(name="summary", type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @Column(name="menu_title", type="string", length=255, nullable=true)
     */
    private $menuTitle;

    /**
     * @var integer
     *
     * @Column(name="menu_index", type="integer", nullable=true)
     */
    private $menuIndex;

    /**
     * @var boolean
     *
     * @Column(name="hidden", type="boolean", nullable=true)
     */
    private $hidden;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @Column(name="url_alias", type="string", length=255, nullable=true)
     */
    private $urlAlias;

    /**
     * @var boolean
     *
     * @Column(name="enable_stats", type="boolean", nullable=true)
     */
    private $enableStats;

    /**
     * @var boolean
     *
     * @Column(name="published", type="boolean", nullable=true)
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @Column(name="publish_at", type="datetime", nullable=true)
     */
    private $publishAt;

    /**
     * @var \DateTime
     *
     * @Column(name="unpublish_at", type="datetime", nullable=true)
     */
    private $unpublishAt;

    /**
     * @var boolean
     *
     * @Column(name="searchable", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var boolean
     *
     * @Column(name="cacheable", type="boolean", nullable=true)
     */
    private $cacheable;

    /**
     * @var boolean
     *
     * @Column(name="empty_cache", type="boolean", nullable=true)
     */
    private $emptyCache;

    /**
     * @var string
     *
     * @Column(name="content_type", type="string", length=45, nullable=true)
     */
    private $contentType;

    /**
     * @var string
     *
     * @Column(name="content_disposition", type="string", length=45, nullable=true)
     */
    private $contentDisposition;

    /**
     * @var string
     *
     * @Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var integer
     *
     * @Column(name="item_id", type="integer", nullable=true)
     */
    private $itemId;

    /**
     * @var integer
     *
     * @Column(name="template_id", type="integer", nullable=true)
     */
    private $templateId;

    /**
     * @var integer
     *
     * @Column(name="domain_id", type="integer", nullable=false)
     */
    private $domainId;

    /**
     * @var string
     *
     * @Column(name="grouping_label", type="string", length=255, nullable=true)
     */
    private $groupingLabel;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var integer
     *
     * @Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;


}
