<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="version")
 * @Entity
 */
class Version
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="version_code_id", type="integer", nullable=true)
     */
    private $versionCodeId;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;


}
