<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="folder_permission")
 * @Entity
 */
class FolderPermission
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\Folder
     *
     * @ManyToOne(targetEntity="Folder")
     * @JoinColumns({
     *   @JoinColumn(name="folder_id", referencedColumnName="id")
     * })
     */
    private $folder;

    /**
     * @var \export\SfGuardUser
     *
     * @ManyToOne(targetEntity="SfGuardUser")
     * @JoinColumns({
     *   @JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}
