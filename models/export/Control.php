<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="control")
 * @Entity
 */
class Control
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Column(name="picture_name", type="string", length=50, nullable=true)
     */
    private $pictureName;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="xtype", type="string", length=45, nullable=true)
     */
    private $xtype;


}
