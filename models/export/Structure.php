<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="structure")
 * @Entity
 */
class Structure
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="lft", type="integer", nullable=false)
     */
    private $lft;

    /**
     * @var integer
     *
     * @Column(name="rgt", type="integer", nullable=false)
     */
    private $rgt;

    /**
     * @var integer
     *
     * @Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @Column(name="linked_structure", type="integer", nullable=false)
     */
    private $linkedStructure;

    /**
     * @var integer
     *
     * @Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="full_title", type="string", length=255, nullable=true)
     */
    private $fullTitle;

    /**
     * @var string
     *
     * @Column(name="summary", type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @Column(name="menu_title", type="string", length=255, nullable=true)
     */
    private $menuTitle;

    /**
     * @var integer
     *
     * @Column(name="menu_index", type="integer", nullable=true)
     */
    private $menuIndex;

    /**
     * @var boolean
     *
     * @Column(name="hidden", type="boolean", nullable=true)
     */
    private $hidden;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @Column(name="url_alias", type="string", length=255, nullable=true)
     */
    private $urlAlias;

    /**
     * @var boolean
     *
     * @Column(name="enable_stats", type="boolean", nullable=true)
     */
    private $enableStats;

    /**
     * @var boolean
     *
     * @Column(name="published", type="boolean", nullable=true)
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @Column(name="publish_at", type="datetime", nullable=true)
     */
    private $publishAt;

    /**
     * @var \DateTime
     *
     * @Column(name="unpublish_at", type="datetime", nullable=true)
     */
    private $unpublishAt;

    /**
     * @var boolean
     *
     * @Column(name="searchable", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var boolean
     *
     * @Column(name="cacheable", type="boolean", nullable=true)
     */
    private $cacheable;

    /**
     * @var boolean
     *
     * @Column(name="empty_cache", type="boolean", nullable=true)
     */
    private $emptyCache;

    /**
     * @var string
     *
     * @Column(name="content_type", type="string", length=45, nullable=true)
     */
    private $contentType;

    /**
     * @var string
     *
     * @Column(name="content_disposition", type="string", length=45, nullable=true)
     */
    private $contentDisposition;

    /**
     * @var string
     *
     * @Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @Column(name="grouping_label", type="string", length=255, nullable=true)
     */
    private $groupingLabel;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var \export\Domain
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumn(name="domain_id", referencedColumnName="id")
     */
    private $domain;

    /**
     * @var Template
     * @ManyToOne(targetEntity="Template")
     * @JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var Item
     * @ManyToOne(targetEntity="Item")
     * @JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @var Structure
     * @ManyToOne(targetEntity="Structure")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    #region Getters and Setters
    /**
     * @param \export\Template $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return \export\Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return boolean
     */
    public function getCacheable()
    {
        return $this->cacheable;
    }

    /**
     * @return string
     */
    public function getContentDisposition()
    {
        return $this->contentDisposition;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return \export\Domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return boolean
     */
    public function getEmptyCache()
    {
        return $this->emptyCache;
    }

    /**
     * @return boolean
     */
    public function getEnableStats()
    {
        return $this->enableStats;
    }

    /**
     * @return string
     */
    public function getFullTitle()
    {
        return $this->fullTitle;
    }

    /**
     * @return string
     */
    public function getGroupingLabel()
    {
        return $this->groupingLabel;
    }

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return int
     */
    public function getLinkedStructure()
    {
        return $this->linkedStructure;
    }

    /**
     * @return int
     */
    public function getMenuIndex()
    {
        return $this->menuIndex;
    }

    /**
     * @return string
     */
    public function getMenuTitle()
    {
        return $this->menuTitle;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Structure
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return \DateTime
     */
    public function getPublishAt()
    {
        return $this->publishAt;
    }

    /**
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @return int
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @return boolean
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return \DateTime
     */
    public function getUnpublishAt()
    {
        return $this->unpublishAt;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getUrlAlias()
    {
        return $this->urlAlias;
    }
    #endregion

}
