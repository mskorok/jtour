<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="folder")
 * @Entity
 */
class Folder
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=1000, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @Column(name="short_url", type="string", length=300, nullable=false)
     */
    private $shortUrl;

    /**
     * @var integer
     *
     * @Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var integer
     *
     * @Column(name="company_id", type="integer", nullable=false)
     */
    private $companyId;

    /**
     * @var integer
     *
     * @Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @param string $shortUrl
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


}
