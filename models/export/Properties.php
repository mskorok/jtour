<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="properties")
 * @Entity
 */
class Properties
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="variable", type="string", length=50, nullable=false)
     */
    private $variable;

    /**
     * @var string
     *
     * @Column(name="type", type="string", length=10, nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @Column(name="category_id", type="integer", nullable=false)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @Column(name="multiple_field", type="integer", nullable=false)
     */
    private $multipleField;

    /**
     * @var string
     *
     * @Column(name="delimeter", type="string", length=5, nullable=false)
     */
    private $delimeter;

    /**
     * @var string
     *
     * @Column(name="combobox_list", type="string", length=500, nullable=false)
     */
    private $comboboxList;

    public function getVariable(){
        return $this->variable;
    }
}
