<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="picture_size")
 * @Entity
 */
class PictureSize
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @Column(name="height", type="integer", nullable=false)
     */
    private $height;

    /**
     * @var string
     *
     * @Column(name="resolution", type="string", length=100, nullable=true)
     */
    private $resolution;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="watermark_url", type="string", length=512, nullable=false)
     */
    private $watermarkUrl;

    /**
     * @var string
     *
     * @Column(name="watermark_position", type="string", length=20, nullable=false)
     */
    private $watermarkPosition;

    /**
     * @var integer
     *
     * @Column(name="watermark_opacity", type="integer", nullable=false)
     */
    private $watermarkOpacity;

    /**
     * @var \export\Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param string $resolution
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * @return string
     */
    public function getResolution()
    {
        return $this->resolution;
    }

    /**
     * @param string $watermarkPosition
     */
    public function setWatermarkPosition($watermarkPosition)
    {
        $this->watermarkPosition = $watermarkPosition;
    }

    /**
     * @return string
     */
    public function getWatermarkPosition()
    {
        return $this->watermarkPosition;
    }


}
