<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="temp_size")
 * @Entity
 */
class TempSize
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="picture_size_id", type="integer", nullable=false)
     */
    private $pictureSizeId;

    /**
     * @var integer
     *
     * @Column(name="priority", type="integer", nullable=false)
     */
    private $priority;

    /**
     * @var string
     *
     * @Column(name="operation", type="string", length=50, nullable=false)
     */
    private $operation;


}
