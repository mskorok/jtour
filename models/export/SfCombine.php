<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="sf_combine")
 * @Entity
 */
class SfCombine
{
    /**
     * @var string
     *
     * @Column(name="assets_key", type="string", length=32, nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $assetsKey;

    /**
     * @var string
     *
     * @Column(name="files", type="text", nullable=false)
     */
    private $files;


}
