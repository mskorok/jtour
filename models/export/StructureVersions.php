<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="structure_versions")
 * @Entity
 */
class StructureVersions
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="menu_title", type="string", length=255, nullable=false)
     */
    private $menuTitle;

    /**
     * @var string
     *
     * @Column(name="meta_title", type="string", length=255, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @Column(name="meta_description", type="text", nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @Column(name="meta_keywords", type="text", nullable=false)
     */
    private $metaKeywords;

    /**
     * @var \export\Structure
     *
     * @ManyToOne(targetEntity="Structure")
     * @JoinColumns({
     *   @JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */
    private $structure;

    /**
     * @var \export\Version
     *
     * @ManyToOne(targetEntity="Version")
     * @JoinColumns({
     *   @JoinColumn(name="version_id", referencedColumnName="id")
     * })
     */
    private $version;


}
