<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;
/**
 * @Table(name="item")
 * @Entity
 */
class Item
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Column(name="modified_at", type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var string
     *
     * @Column(name="modified_by", type="string", length=255, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var boolean
     *
     * @Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @Column(name="status_code", type="boolean", nullable=true)
     */
    private $statusCode;

    /**
     * @var integer
     *
     * @Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var \DateTime
     *
     * @Column(name="expiration_date", type="date", nullable=true)
     */
    private $expirationDate;

    /**
     * @var Category
     *
     * @ManyToOne(targetEntity="Category")
     * @JoinColumns({
     *   @JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \export\Status
     *
     * @ManyToOne(targetEntity="Status")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @var \export\Structure
     * @OneToMany(targetEntity="\export\Structure", mappedBy="item")
     */
    private $structure;

    /**
     * @var \export\Value
     * @OneToMany(targetEntity="\export\Value", mappedBy="item")
     */
    private $value;

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @return \export\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return boolean
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param \export\Structure $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return \export\Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param \export\Value $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \export\Value
     */
    public function getValue()
    {
        return $this->value;
    }


}
