<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="file")
 * @Entity
 */
class File
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="extension", type="string", length=10, nullable=false)
     */
    private $extension;

    /**
     * @var string
     *
     * @Column(name="copyright", type="text", nullable=false)
     */
    private $copyright;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=false)
     */
    private $info;

    /**
     * @var string
     *
     * @Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @Column(name="folder_id", type="integer", nullable=false)
     */
    private $folderId;


}
