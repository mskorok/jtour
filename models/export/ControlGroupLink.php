<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="control_group_link")
 * @Entity
 */
class ControlGroupLink
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\CategoryControl
     *
     * @ManyToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;

    /**
     * @var \export\ControlGroups
     *
     * @ManyToOne(targetEntity="ControlGroups")
     * @JoinColumns({
     *   @JoinColumn(name="control_group_id", referencedColumnName="id")
     * })
     */
    private $controlGroup;

    /**
     * @var \export\Category
     *
     * @ManyToOne(targetEntity="Category")
     * @JoinColumns({
     *   @JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;


}
