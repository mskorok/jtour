<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="control_group_domains")
 * @Entity
 */
class ControlGroupDomains
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\ControlGroups
     *
     * @ManyToOne(targetEntity="ControlGroups")
     * @JoinColumns({
     *   @JoinColumn(name="control_group_id", referencedColumnName="id")
     * })
     */
    private $controlGroup;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;


}
