<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="control_property")
 * @Entity
 */
class ControlProperty
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="struct_link_type", type="integer", nullable=false)
     */
    private $structLinkType;

    /**
     * @var integer
     *
     * @Column(name="link_type", type="integer", nullable=false)
     */
    private $linkType;

    /**
     * @var \export\CategoryControl
     *
     * @ManyToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;


}
