<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="catalog_structure")
 * @Entity
 */
class CatalogStructure
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="lft", type="integer", nullable=false)
     */
    private $lft;

    /**
     * @var integer
     *
     * @Column(name="rgt", type="integer", nullable=false)
     */
    private $rgt;

    /**
     * @var integer
     *
     * @Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var integer
     *
     * @Column(name="category_template", type="integer", nullable=false)
     */
    private $categoryTemplate;

    /**
     * @var string
     *
     * @Column(name="img_url", type="string", length=255, nullable=false)
     */
    private $imgUrl;

    /**
     * @var \export\Catalog
     *
     * @ManyToOne(targetEntity="Catalog")
     * @JoinColumns({
     *   @JoinColumn(name="catalog_id", referencedColumnName="id")
     * })
     */
    private $catalog;


}
