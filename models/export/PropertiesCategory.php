<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="properties_category")
 * @Entity
 */
class PropertiesCategory
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\Category
     *
     * @ManyToOne(targetEntity="Category")
     * @JoinColumns({
     *   @JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \export\Properties
     *
     * @ManyToOne(targetEntity="Properties")
     * @JoinColumns({
     *   @JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    private $property;


}
