<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="category_control")
 * @Entity
 */
class CategoryControl
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="label", type="string", length=100, nullable=true)
     */
    private $label;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var integer
     *
     * @Column(name="domain_id", type="integer", nullable=true)
     */
    private $domainId;

    /**
     * @var string
     *
     * @Column(name="variable", type="string", length=100, nullable=false)
     */
    private $variable;

    /**
     * @var integer
     *
     * @Column(name="category_link_id", type="integer", nullable=true)
     */
    private $categoryLinkId;

    /**
     * @var integer
     *
     * @Column(name="catalog_id", type="integer", nullable=true)
     */
    private $catalogId;

    /**
     * @var string
     *
     * @Column(name="composite_label", type="string", length=150, nullable=false)
     */
    private $compositeLabel;

    /**
     * @var integer
     *
     * @Column(name="capacity", type="integer", nullable=false)
     */
    private $capacity;

    /**
     * @var integer
     *
     * @Column(name="only_number", type="integer", nullable=false)
     */
    private $onlyNumber;

    /**
     * @var integer
     *
     * @Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @Column(name="date_range", type="integer", nullable=false)
     */
    private $dateRange;

    /**
     * @var string
     *
     * @Column(name="value_list", type="string", length=500, nullable=false)
     */
    private $valueList;

    /**
     * @var string
     *
     * @Column(name="gallery_thumbnail", type="string", length=50, nullable=false)
     */
    private $galleryThumbnail;

    /**
     * @var string
     *
     * @Column(name="gallery_picture", type="string", length=50, nullable=false)
     */
    private $galleryPicture;

    /**
     * @var integer
     *
     * @Column(name="gallery_links", type="integer", nullable=false)
     */
    private $galleryLinks;

    /**
     * @var string
     *
     * @Column(name="gallery_onclick", type="text", nullable=false)
     */
    private $galleryOnclick;

    /**
     * @var string
     *
     * @Column(name="grouping_label", type="string", length=255, nullable=false)
     */
    private $groupingLabel;

    /**
     * @var string
     *
     * @Column(name="and_or", type="string", length=3, nullable=false)
     */
    private $andOr;

    /**
     * @var Category
     *
     * @ManyToOne(targetEntity="Category")
     * @JoinColumns({
     *   @JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \export\Control
     *
     * @ManyToOne(targetEntity="Control")
     * @JoinColumns({
     *   @JoinColumn(name="control_id", referencedColumnName="id")
     * })
     */
    private $control;

    /**
     * @var \export\Version
     *
     * @ManyToOne(targetEntity="Version")
     * @JoinColumns({
     *   @JoinColumn(name="version_id", referencedColumnName="id")
     * })
     */
    private $version;

    /**
     * @return string
     */
    public function getAndOr()
    {
        return $this->andOr;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return int
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getCategoryLinkId()
    {
        return $this->categoryLinkId;
    }

    /**
     * @return string
     */
    public function getCompositeLabel()
    {
        return $this->compositeLabel;
    }

    /**
     * @return \export\Control
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * @return int
     */
    public function getDateRange()
    {
        return $this->dateRange;
    }

    /**
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * @return int
     */
    public function getGalleryLinks()
    {
        return $this->galleryLinks;
    }

    /**
     * @return string
     */
    public function getGalleryOnclick()
    {
        return $this->galleryOnclick;
    }

    /**
     * @return string
     */
    public function getGalleryPicture()
    {
        return $this->galleryPicture;
    }

    /**
     * @return string
     */
    public function getGalleryThumbnail()
    {
        return $this->galleryThumbnail;
    }

    /**
     * @return string
     */
    public function getGroupingLabel()
    {
        return $this->groupingLabel;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return int
     */
    public function getOnlyNumber()
    {
        return $this->onlyNumber;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getValueList()
    {
        return $this->valueList;
    }

    /**
     * @return string
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * @return \export\Version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }


}
