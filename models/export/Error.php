<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="error")
 * @Entity
 */
class Error
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="message", type="text", nullable=false)
     */
    private $message;


}
