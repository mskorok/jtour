<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="catalog_domains")
 * @Entity
 */
class CatalogDomains
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\Catalog
     *
     * @ManyToOne(targetEntity="Catalog")
     * @JoinColumns({
     *   @JoinColumn(name="catalog_id", referencedColumnName="id")
     * })
     */
    private $catalog;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;


}
