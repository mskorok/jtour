<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="tag_versions")
 * @Entity
 */
class TagVersions
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=500, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \export\Tags
     *
     * @ManyToOne(targetEntity="Tags")
     * @JoinColumns({
     *   @JoinColumn(name="tag_id", referencedColumnName="id")
     * })
     */
    private $tag;

    /**
     * @var \export\VersionCode
     *
     * @ManyToOne(targetEntity="VersionCode")
     * @JoinColumns({
     *   @JoinColumn(name="version_code_id", referencedColumnName="id")
     * })
     */
    private $versionCode;


}
