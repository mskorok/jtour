<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="sf_guard_remember_key")
 * @Entity
 */
class SfGuardRememberKey
{
    /**
     * @var string
     *
     * @Column(name="ip_address", type="string", length=50, nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @Column(name="remember_key", type="string", length=32, nullable=true)
     */
    private $rememberKey;

    /**
     * @var \DateTime
     *
     * @Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \export\SfGuardUser
     *
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @OneToOne(targetEntity="SfGuardUser")
     * @JoinColumns({
     *   @JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}
