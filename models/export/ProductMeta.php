<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="product_meta")
 * @Entity
 */
class ProductMeta
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="meta_title", type="string", length=256, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @Column(name="meta_description", type="text", nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @Column(name="meta_keywords", type="text", nullable=false)
     */
    private $metaKeywords;

    /**
     * @var \export\Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \export\VersionCode
     *
     * @ManyToOne(targetEntity="VersionCode")
     * @JoinColumns({
     *   @JoinColumn(name="version_code_id", referencedColumnName="id")
     * })
     */
    private $versionCode;


}
