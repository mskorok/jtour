<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="catalog_structure_item")
 * @Entity
 */
class CatalogStructureItem
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="catalog_id", type="integer", nullable=false)
     */
    private $catalogId;

    /**
     * @var string
     *
     * @Column(name="meta_title", type="string", length=256, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @Column(name="meta_description", type="text", nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @Column(name="meta_keywords", type="text", nullable=false)
     */
    private $metaKeywords;

    /**
     * @var integer
     *
     * @Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var \export\Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;

    /**
     * @var \export\CatalogStructure
     *
     * @ManyToOne(targetEntity="CatalogStructure")
     * @JoinColumns({
     *   @JoinColumn(name="catalog_structure_id", referencedColumnName="id")
     * })
     */
    private $catalogStructure;


}
