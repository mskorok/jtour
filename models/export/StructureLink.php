<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="structure_link")
 * @Entity
 */
class StructureLink
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;

    /**
     * @var CategoryControl
     *
     * @ManyToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;

    /**
     * @var Structure
     *
     * @ManyToOne(targetEntity="Structure")
     * @JoinColumns({
     *   @JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */
    private $structure;

    /**
     * @return CategoryControl
     */
    public function getCategoryControl()
    {
        return $this->categoryControl;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }


}
