<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="control_groups")
 * @Entity
 */
class ControlGroups
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="variable", type="string", length=100, nullable=false)
     */
    private $variable;

    /**
     * @var \export\Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


}
