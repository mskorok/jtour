<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="product")
 * @Entity
 */
class Product
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\Catalog
     *
     * @ManyToOne(targetEntity="Catalog")
     * @JoinColumns({
     *   @JoinColumn(name="catalog_id", referencedColumnName="id")
     * })
     */
    private $catalog;

    /**
     * @var \export\Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;


}
