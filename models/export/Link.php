<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="link")
 * @Entity
 */
class Link
{
    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var Item
     *
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @OneToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;

    /**
     * @var Item
     *
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @OneToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="linked_item_id", referencedColumnName="id")
     * })
     */
    private $linkedItem;

    /**
     * @var CategoryControl
     *
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @OneToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;

    /**
     * @return CategoryControl
     */
    public function getCategoryControl()
    {
        return $this->categoryControl;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return Item
     */
    public function getLinkedItem()
    {
        return $this->linkedItem;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }


}
