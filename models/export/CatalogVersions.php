<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="catalog_versions")
 * @Entity
 */
class CatalogVersions
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=150, nullable=false)
     */
    private $url;

    /**
     * @var integer
     *
     * @Column(name="version_code_id", type="integer", nullable=false)
     */
    private $versionCodeId;

    /**
     * @var \export\CatalogStructure
     *
     * @ManyToOne(targetEntity="CatalogStructure")
     * @JoinColumns({
     *   @JoinColumn(name="catalog_structure_id", referencedColumnName="id")
     * })
     */
    private $catalogStructure;


}
