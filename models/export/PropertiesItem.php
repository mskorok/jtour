<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="properties_item")
 * @Entity
 */
class PropertiesItem
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var \export\Properties
     *
     * @ManyToOne(targetEntity="Properties")
     * @JoinColumns({
     *   @JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    private $property;

    /**
     * @var \export\Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;

    /**
     * @var \export\PropertiesCategory
     *
     * @ManyToOne(targetEntity="PropertiesCategory")
     * @JoinColumns({
     *   @JoinColumn(name="property_category_id", referencedColumnName="id")
     * })
     */
    private $propertyCategory;

    public function getValue(){
        return $this->value;
    }
}
