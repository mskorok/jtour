<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="menu_item")
 * @Entity
 */
class MenuItem
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="index", type="integer", nullable=false)
     */
    private $index;

    /**
     * @var \export\Menu
     *
     * @ManyToOne(targetEntity="Menu")
     * @JoinColumns({
     *   @JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     */
    private $menu;

    /**
     * @var \export\Structure
     *
     * @ManyToOne(targetEntity="Structure")
     * @JoinColumns({
     *   @JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */
    private $structure;


}
