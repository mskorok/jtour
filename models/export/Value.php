<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="value")
 */
class Value
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var integer
     *
     * @Column(name="version_id", type="integer", nullable=true)
     */
    private $versionId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ManyToMany(targetEntity="Picture", inversedBy="value")
     * @JoinTable(name="picture_value",
     *   joinColumns={
     *     @JoinColumn(name="value_id", referencedColumnName="id")
     *
     *   },
     *   inverseJoinColumns={
     *     @JoinColumn(name="picture_id", referencedColumnName="id")
     *   }
     * )
     */
    private $picture;


    /**
     * @var CategoryControl
     *
     * @ManyToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;

    /**
     * @var Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->picture = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return CategoryControl
     */
    public function getCategoryControl()
    {
        return $this->categoryControl;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getVersionId()
    {
        return $this->versionId;
    }
    
}
