<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="template")
 * @Entity
 */
class Template
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;

    /**
     * @var \export\Structure
     * @OneToMany(targetEntity="\export\Structure", mappedBy="template")
     */
    private $structure;

    #region Getters and Setters
    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    #endregion

    /**
     * @return \export\Structure
     */
    public function getStructure(){
        return $this->structure;
    }
}
