<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="menu")
 * @Entity
 */
class Menu
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="variable", type="string", length=100, nullable=false)
     */
    private $variable;

    /**
     * @var \export\Domain
     *
     * @ManyToOne(targetEntity="Domain")
     * @JoinColumns({
     *   @JoinColumn(name="domain_id", referencedColumnName="id")
     * })
     */
    private $domain;


}
