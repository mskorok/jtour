<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="company")
 * @Entity
 */
class Company
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="phone1", type="string", length=255, nullable=true)
     */
    private $phone1;

    /**
     * @var string
     *
     * @Column(name="phone2", type="string", length=50, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @Column(name="fax1", type="string", length=45, nullable=true)
     */
    private $fax1;

    /**
     * @var string
     *
     * @Column(name="fax2", type="string", length=50, nullable=true)
     */
    private $fax2;

    /**
     * @var string
     *
     * @Column(name="email1", type="string", length=100, nullable=true)
     */
    private $email1;

    /**
     * @var string
     *
     * @Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2;

    /**
     * @var string
     *
     * @Column(name="gsm1", type="string", length=50, nullable=true)
     */
    private $gsm1;

    /**
     * @var string
     *
     * @Column(name="gsm2", type="string", length=50, nullable=true)
     */
    private $gsm2;

    /**
     * @var string
     *
     * @Column(name="website", type="string", length=100, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @Column(name="street_address", type="string", length=100, nullable=true)
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @Column(name="street_suburb", type="string", length=45, nullable=true)
     */
    private $streetSuburb;

    /**
     * @var string
     *
     * @Column(name="street_state", type="string", length=45, nullable=true)
     */
    private $streetState;

    /**
     * @var string
     *
     * @Column(name="street_postcode", type="string", length=45, nullable=true)
     */
    private $streetPostcode;

    /**
     * @var string
     *
     * @Column(name="street_country", type="string", length=45, nullable=true)
     */
    private $streetCountry;

    /**
     * @var string
     *
     * @Column(name="postal_address", type="string", length=100, nullable=true)
     */
    private $postalAddress;

    /**
     * @var string
     *
     * @Column(name="postal_suburb", type="string", length=45, nullable=true)
     */
    private $postalSuburb;

    /**
     * @var string
     *
     * @Column(name="postal_state", type="string", length=45, nullable=true)
     */
    private $postalState;

    /**
     * @var string
     *
     * @Column(name="postal_postcode", type="string", length=45, nullable=true)
     */
    private $postalPostcode;

    /**
     * @var string
     *
     * @Column(name="postal_country", type="string", length=45, nullable=true)
     */
    private $postalCountry;

    /**
     * @var boolean
     *
     * @Column(name="picture_bank_allowed", type="boolean", nullable=true)
     */
    private $pictureBankAllowed;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="full_title", type="string", length=255, nullable=true)
     */
    private $fullTitle;

    /**
     * @var string
     *
     * @Column(name="summary", type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @Column(name="menu_title", type="string", length=255, nullable=true)
     */
    private $menuTitle;

    /**
     * @var integer
     *
     * @Column(name="menu_index", type="integer", nullable=true)
     */
    private $menuIndex;

    /**
     * @var boolean
     *
     * @Column(name="show_in_menu", type="boolean", nullable=true)
     */
    private $showInMenu;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @Column(name="url_alias", type="string", length=255, nullable=true)
     */
    private $urlAlias;

    /**
     * @var boolean
     *
     * @Column(name="enable_stats", type="boolean", nullable=true)
     */
    private $enableStats;

    /**
     * @var boolean
     *
     * @Column(name="published", type="boolean", nullable=true)
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @Column(name="publish_at", type="datetime", nullable=true)
     */
    private $publishAt;

    /**
     * @var \DateTime
     *
     * @Column(name="unpublish_at", type="datetime", nullable=true)
     */
    private $unpublishAt;

    /**
     * @var boolean
     *
     * @Column(name="searchable", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var boolean
     *
     * @Column(name="cacheable", type="boolean", nullable=true)
     */
    private $cacheable;

    /**
     * @var boolean
     *
     * @Column(name="empty_cache", type="boolean", nullable=true)
     */
    private $emptyCache;

    /**
     * @var string
     *
     * @Column(name="content_type", type="string", length=45, nullable=true)
     */
    private $contentType;

    /**
     * @var string
     *
     * @Column(name="content_disposition", type="string", length=45, nullable=true)
     */
    private $contentDisposition;

    /**
     * @var string
     *
     * @Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;


}
