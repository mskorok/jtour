<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="version_code")
 * @Entity
 */
class VersionCode
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="code", type="string", length=3, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @Column(name="icon", type="string", length=100, nullable=true)
     */
    private $icon;


}
