<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="structure_link_tags")
 * @Entity
 */
class StructureLinkTags
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\StructureLink
     *
     * @ManyToOne(targetEntity="StructureLink")
     * @JoinColumns({
     *   @JoinColumn(name="structure_link_id", referencedColumnName="id")
     * })
     */
    private $structureLink;

    /**
     * @var \export\Tags
     *
     * @ManyToOne(targetEntity="Tags")
     * @JoinColumns({
     *   @JoinColumn(name="tag_id", referencedColumnName="id")
     * })
     */
    private $tag;


}
