<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="link_tag")
 * @Entity
 */
class LinkTag
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \export\CategoryControl
     *
     * @ManyToOne(targetEntity="CategoryControl")
     * @JoinColumns({
     *   @JoinColumn(name="category_control_id", referencedColumnName="id")
     * })
     */
    private $categoryControl;

    /**
     * @var \export\Tags
     *
     * @ManyToOne(targetEntity="Tags")
     * @JoinColumns({
     *   @JoinColumn(name="tag_id", referencedColumnName="id")
     * })
     */
    private $tag;

    /**
     * @var \export\Item
     *
     * @ManyToOne(targetEntity="Item")
     * @JoinColumns({
     *   @JoinColumn(name="item_id", referencedColumnName="id")
     * })
     */
    private $item;


}
