<?php
namespace export;


use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="globals")
 * @Entity
 */
class Globals
{
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="variable", type="string", length=100, nullable=false)
     */
    private $variable;

    /**
     * @var string
     *
     * @Column(name="value", type="text", nullable=false)
     */
    private $value;

    /**
     * @var \export\Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


}
