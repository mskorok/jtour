<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 24.04.13
 * Time: 12:06
 * To change this template use File | Settings | File Templates.
 * @description сущность необходимых документов
 *
 * @Entity
 */

namespace application\models\entities\documents;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;

class DocumentsAdapterEntity  extends sysAbstract {

    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description здесь записывается всякий html с классами для декорации
     * @Column
     * @var
     */
    protected $url;
    /**
     * @description а здесь теги закрытия
     * @Column
     * @var
     */
    protected $text="";

    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array(
        'url'=> ''
    );
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 6,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => " v, i, c, s, cc ",
        'RESULT_COLUMN_SQL'         => " value.id as vId, value.value as vaval,
                                        item.id AS itId, item.title AS ittl,
                                        structure.id as sId, structure.url as surl,
                                        category.id AS cId, category.title AS cttl,
                                        category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl ",
        'JOIN'                      => "  i.value  v , i.category c , i.structure s ,    v.categoryControl cc",
        'JOIN_SQL'                  => "  `item` ON item.id=value.item_id, `category_control` ON category_control.id = value.category_control_id, `structure` ON (item.id = structure.item_id  AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE \"%test%\")), `category` ON item.category_id = category.id  ",
        'FROM'                      => "  \export\Item i  ",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "i.id",
        'ORDER_BY_KEY_SQL'          => " item.id ",
        'ORDER_BY_VALUE'            => "DESC",
        'ORDER_BY_VALUE_SQL'        => "DESC",
        'ADDITIONAL_CONDITION'      => "",
        'ADDITIONAL_CONDITION_SQL'  => "",
        'OFFSET'                    => null


    );
    /**
     *
     *SELECT value.id as vId, value.value as vaval,
    item.id AS itId, item.title AS ittl,
    structure.id as sid, structure.url as surl,
    category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl
    FROM `value`
    JOIN `item` ON value.item_id = item.id
    JOIN `category_control` ON  value.category_control_id = category_control.id
    JOIN `category` ON (item.category_id = category.id )
    JOIN `structure` ON
    (item.id = structure.item_id AND structure.url LIKE "url"
    AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE "%test%"))

     */
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $keys=array_keys($criteria);
        if(in_array('config', $keys))
            $conf=$criteria['config'];
        else
            $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(false, $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {
        $findResult = array();

        foreach($results as $item)
        {
            /** @var $item Item  */
            $values=$item->getValue();
            if($values){

                $doc=new  DocumentsAdapterEntity();
                $str=$item->getStructure();
                /** @var $struct Structure */
                $struct=$str[0];
                $url=$struct->getUrl();
                $doc->setUrl($url);

                foreach($values as $value)
                {
                    /** @var $value Value */
                    $catCtl=$value->getCategoryControl();
                    $variable=$catCtl->getVariable();
                    switch($variable)
                    {
                        case "documents":
                            $documents=$value->getValue();
                            $doc->setText($documents);
                            break;
                    }
                }
                if(self::cnt($doc))
                    $findResult[] =$doc;
            } else continue;

        }
        return $findResult;
    }

    protected static function getObjectByValue($results)
    {
        $doc=new DocumentsAdapterEntity();
        $findResult = array();
        $itemId=null;
        foreach($results as $key => $value)
        {
            /** @var $item Item  */
            /** @var $value Value */
            $item=$value->getItem();
            $iId=$item->getId();
            if($itemId != $iId)
            {
                if($key != 0)
                {
                    $findResult[]=$doc;
                    $doc=new DocumentsAdapterEntity();

                }
                $itemId=($itemId != $iId)?$iId:$itemId;
                // сборка сущности
                $str=$item->getStructure();
                /** @var $struct Structure */
                $struct=$str[0];
                $url=$struct->getUrl();
                $doc->setUrl($url);


            }

            $catCtl=$value->getCategoryControl();

            $variable=$catCtl->getVariable();

            switch($variable)
            {
                case "documents":
                    $documents=$value->getValue();
                    $doc->setText($documents);
                    break;
            }
            $findResult[]=$doc;
        }
        unset($value, $catCtl, $key, $variable);

        return $findResult;
    }
    private static function cnt($entity)
    {
        return true;
    }

    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle()
    {
       return "Необходимые документы";
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }



}