<?php
namespace entities\task;
use entities\task\StatusEntity;
use entities\user\UserEntity as userSysEntity,  DateTime;
use entities\_abstract\sysSimpleAbstract as sysSimpleAbstract;
use entities\comments\anyEntity as commentsAnyEntity;

use helpers\JEntityHelper as JEntityHelper;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.01.13
 * Time: 13:06
 * To change this template use File | Settings | File Templates.
 *  Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.11.12
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 *
 *  @description Сущность задачи
 *
 *
 */
class taskAdapterEntity  extends taskEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}
