<?php
namespace entities\company;
use entities\_abstract\sysGeneralAbstract;
use components\JArrayCollection as ArrayCollection;

/**
 *
 *
 * Класс для хранения данных компании
 *
 *
 * @date: 24.01.13 - 18:31
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class CompanyAdapterEntity extends CompanyEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}
