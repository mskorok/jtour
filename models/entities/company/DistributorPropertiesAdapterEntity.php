<?php
namespace entities\company;
use entities\_abstract\sysAbstract;
use entities\sys\MathExpressionEntity;

/**
 *
 *
 *
 *
 * @date: 24.01.13 - 18:37
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class DistributorPropertiesAdapterEntity extends DistributorPropertiesEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}
