<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 07.05.13
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 *  @Entity
 *
 */

namespace application\models\entities\tour;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;
use application\models\entities\media\sysFileImageAdapterEntity as sysFileImageAdapterEntity;

class ToursAdapterEntity  extends sysAbstract {
    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /**
     * Константа, определяющая из будет ли производиться выборка из оптимуса или из массивов
     */
    const OPTIMUS = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description
     * @Column
     * @var
     */
    protected $title;
    /**
     * @description
     * @Column
     * @var
     */
    protected $price;
    /**
     * @description  ссылка на рисунок
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description  стиль ссылки
     * @Column
     * @var
     */
    protected $urlStyle;
    /**
     * @description ссылка
     * @Column
     * @var
     */
    protected  $url;
    /**
     * @description валюта
     * @Column
     * @var
     */
    protected  $currency;
    /**
     * @description урл страницы на которой находится виджет
     * @Column
     * @var
     */
    protected  $ownerUrl;
    /**
     * @description анонс
     * @Column
     * @var
     */
    protected $preview;
    /**
     * @description количество дней
     * @Column
     * @var
     */
    protected  $days;
    /**
     * @description текст
     * @Column
     * @var
     */
    protected $description;
    /**
     * @description фотоальбом
     * @Column
     * @var array
     */
    protected $gallery;
    /**
     * @description совершенно ненужная штука,
     *               используется для сохранения преемственности с оптимусом
     *               например "в Эстонию"
     *
     * @Column
     * @var string
     */
    protected $label;

    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array();
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array();
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        if(self::OPTIMUS){
            $findResult=self::Tours($criteria['url']);
        }else{
            $data=self::getData($criteria['url']);
            $findResult=self::getItems($data);
        }

        return $findResult;
    }
    protected static function getData($url)
    {
        switch($url)
        {
            case "":
                $data=array();
                break;
            default:
                $data=array();
                break;
        }
        return $data;
    }
    protected static function getItems($data)
    {
        $res=array();
        foreach($data as $property)
        {
            $fp=new ToursAdapterEntity();

            $fp->setImg($property['img']);
            $fp->setUrlStyle($property['urlStyle']);
            $fp->setUrl($property['url']);

            $fp->setOwnerUrl($property['ownerUrl']);
            $res[]=$fp;
        }
        unset($property);
        return $res;
    }


    /******************************************************************************************************************
     * New implementation
     ******************************************************************************************************************/

    protected static function Tours($pageUrl)
    {


        // $url=\Yii::app()->request->getScriptUrl();
        // $url=basename($url. ".php")."/";
        $url=$pageUrl;
        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url}";

        $dql="SELECT DISTINCT sl, i, s, cc FROM \export\StructureLink sl
                JOIN sl.item i
                JOIN sl.structure s
                JOIN sl.categoryControl cc
                WHERE cc IN
                (SELECT cc FROM \export\CategoryControl
                WHERE cc.variable='tours') AND i IN ({$partDql})
                ";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);
        $results=$query->getResult();
        unset($dql);
        $findResult=array();





        $dql="SELECT pi FROM \export\PropertiesItem pi
                JOIN pi.item i
                JOIN pi.property p
                WHERE   i IN (SELECT s.item FROM \export\Structure s WHERE s.url='{$pageUrl}')
                     AND
                        p IN (SELECT p FROM \export\Properties p WHERE p.variable='name_kuda')";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);
        $prop=$query->getResult();
        /** @var  $property \export\PropertiesItem*/
        $property=$prop[0];
        $label=$property->getValue();

        if($results){
            foreach($results as $res){
                /** @var  $res \export\StructureLink */
                $entity= new ToursAdapterEntity();

                $entity->setUrl($res->getStructure()->getUrl());
                $entity->setTitle($res->getStructure()->getTitle());
                $entity->setOwnerUrl($url);
                $entity->setLabel($label);

                $item=$res->getItem();
                $dql="SELECT v, cc FROM  \export\Value v
                        JOIN v.item i
                        JOIN v.categoryControl cc
                        WHERE cc.variable IN ('currency', 'start_price_eur', 'start_price_usd', 'start_price_rub', 'pic4list_223x167', 'text_short', 'days') AND i.id=\"{$item}\"";
                $query = JEntityHelper::getEntityManager()->createQuery($dql);

                $values=$query->getResult();
                $currency=null;
                foreach($values as $value){
                    /** @var  $value \export\Value */
                    if($value->getCategoryControl() == "currency") $currency=$value->getValue();
                }
                unset($value);
                $price='';
                foreach($values as $value){
                    /** @var  $value \export\Value */
                    $cc=$value->getCategoryControl();
                    switch($currency)
                    {
                        case "EUR":
                            if($cc == "start_price_eur") $price = $value->getValue();
                            break;
                        case "USD":
                           if($cc == "start_price_usd") $price = $value->getValue();
                            break;
                        case "RUB":
                            if($cc == "start_price_rub") $price = $value->getValue();
                            break;
                    }
                    switch($cc)
                    {
                        case "pic4list_223x167":
                            $id=$value->getValue();
                            $img=sysFileImageAdapterEntity::getImage($id);

                            $entity->setImg($img->getUri());
                            break;
                        case "text_short":
                            $entity->setPreview($value->getValue());
                            break;
                        case "days":
                            $entity->setDays($value->getValue());
                            break;
                    }
                }
                unset($value);
                $entity->setPrice($price);
                $entity->setCurrency($currency);

                $findResult[]=$entity;

            }
            unset($res);
        }
        $findResult=self::checkNullArray($findResult);
        return $findResult;
    }





    private  static function checkNull(array $check)
    {

        $res=false;
        foreach($check as $null)
        {

            $fl1=($null === "")?true:false;
            $fl2=($null === null)?true:false;
            $fl3=((is_array($null) && (count($null) == 0)))?true:false;
            if($fl1 || $fl2 || $fl3) $res=$res || false;
            else $res=$res || true;
        }



        return $res;
    }
    private   static function checkNullArray($check)
    {
        $flag=false;
        foreach($check as $checkedItem)
        {
            if(($checkedItem instanceof ToursAdapterEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                $flag=true;
        }
        $res=$flag?$check:array();
        return $res;
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/


    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $ownerUrl
     */
    public function setOwnerUrl($ownerUrl)
    {
        $this->ownerUrl = $ownerUrl;
    }

    /**
     * @return
     */
    public function getOwnerUrl()
    {
        return $this->ownerUrl;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $urlStyle
     */
    public function setUrlStyle($urlStyle)
    {
        $this->urlStyle = $urlStyle;
    }

    /**
     * @return
     */
    public function getUrlStyle()
    {
        return $this->urlStyle;
    }

    /**
     * @param  $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param  $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * @return
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param  $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
    }

    /**
     * @return
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param  $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param  $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param  $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param array $gallery
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return array
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }


}