<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.05.13
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 *
 *
 * @Entity
 *
 */

namespace application\models\entities\tour;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;

class LifeStyleAdapterEntity  extends sysAbstract {
    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /**
     * Константа, определяющая из будет ли производиться выборка из оптимуса или из массивов
     */
    const OPTIMUS = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description здесь записывается всякий html с классами для декорации
     * @Column
     * @var
     */
    protected $headerDecoration;
    /**
     * @description а здесь теги закрытия
     * @Column
     * @var
     */
    protected $footerDecoration;
    /**
     * @description  ссылка на рисунок
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description  стиль ссылки
     * @Column
     * @var
     */
    protected $urlStyle;
    /**
     * @description ссылка
     * @Column
     * @var
     */
    protected  $url;
    /**
     * @description текст ссылки
     * @Column
     * @var
     */
    protected  $urlText;
    /**
     * @description урл страницы на которой находится виджет
     * @Column
     * @var
     */
    protected  $ownerUrl;
    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array();
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array();
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        if(self::OPTIMUS){
            $findResult=self::LifeStyle($criteria['url']);
        }else{
            $data=self::getData($criteria['url']);
            $findResult=self::getItems($data);
        }

        return $findResult;
    }
    protected static function getData($url)
    {
        switch($url)
        {
            case "":
                $data=array();
                break;
            default:
                $data=array();
                break;
        }
        return $data;
    }
    protected static function getItems($data)
    {
        $res=array();
        foreach($data as $property)
        {
            $fp=new LifeStyleAdapterEntity();
            $fp->setHeaderDecoration($property['headerDecoration']);
            $fp->setFooterDecoration($property['footerDecoration']);
            $fp->setImg($property['img']);
            $fp->setUrlStyle($property['urlStyle']);
            $fp->setUrl($property['url']);
            $fp->setUrlText($property['urlText']);
            $fp->setOwnerUrl($property['ownerUrl']);
            $res[]=$fp;
        }
        unset($property);
        return $res;
    }

    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }
    /******************************************************************************************************************
     * New implementation
     ******************************************************************************************************************/

    protected static function LifeStyle($pageUrl)
    {
        $findResult=array();
        if(!self::checkValue($pageUrl, 'lifestyle_true')){
            if(self::check($pageUrl)){
                if(self::checkValue($pageUrl, 'dop_lf_title'))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra0");
                    $entity->setUrlText(self::value($pageUrl, 'dop_lf_title'));
                    $entity->setUrlStyle(self::value($pageUrl, 'dop_lf_linksize'));
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if(self::checkValue($pageUrl, 'dop_lf_title_2'))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra7");
                    $entity->setUrlText(self::value($pageUrl, 'dop_lf_title_2'));
                    $entity->setUrlStyle(self::value($pageUrl, 'dop_lf_linksize_2'));
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if(self::checkValue($pageUrl, 'dop_lf_title_3'))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra7");
                    $entity->setUrlText(self::value($pageUrl, 'dop_lf_title_3'));
                    $entity->setUrlStyle(self::value($pageUrl, 'dop_lf_linksize_3'));
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'casino_link')) || (self::checkValueLength($pageUrl, 'casino_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra1");
                    $entity->setUrlText("<span>Казино</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'culture_link')) || (self::checkValueLength($pageUrl, 'culture_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra2");
                    $entity->setUrlText("<span>Культура</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'shops_link')) || (self::checkValueLength($pageUrl, 'shops_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra3");
                    $entity->setUrlText("<span>Магазины</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'night_link')) || (self::checkValueLength($pageUrl, 'night_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra4");
                    $entity->setUrlText("<span>Ночная жизнь</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'restruns_link')) || (self::checkValueLength($pageUrl, 'restruns_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra5");
                    $entity->setUrlText("<span>Рестораны</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
                if((self::checkLoop($pageUrl, 'sport_link')) || (self::checkValueLength($pageUrl, 'sport_text')))
                {
                    $entity= new LifeStyleAdapterEntity();
                    $entity->setUrl("{$pageUrl}what_to_do/#fra6");
                    $entity->setUrlText("<span>Спорт</span>");
                    $entity->setOwnerUrl($pageUrl);

                    $findResult[]=$entity;
                }
            }
        }
        $findResult=self::checkNullArray($findResult);
        return $findResult;

    }


    private  static function checkNull(array $check)
    {

        $res=false;
        foreach($check as $null)
        {

            $fl1=($null === "")?true:false;
            $fl2=($null === null)?true:false;
            $fl3=((is_array($null) && (count($null) == 0)))?true:false;
            if($fl1 || $fl2 || $fl3) $res=$res || false;
            else $res=$res || true;
        }



        return $res;
    }
    private   static function checkNullArray($check)
    {
        $flag=false;
        foreach($check as $checkedItem)
        {
            if(($checkedItem instanceof LifeStyleAdapterEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                $flag=true;
        }
        $res=$flag?$check:array();
        return $res;
    }
    private function  check($url)
    {
        $dql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url='".$url."what_to_do/'";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);

        $res=$query->getResult();
        if(is_array($res) && count($res) > 0) $check=true;
        else $check=false;
       return $check;
    }


    private function checkValue($url, $variable){
        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url} OR s.url='{$url}what_to_do/' ";
        $dql="SELECT v FROM  \export\Value v
                        JOIN v.item i
                        JOIN v.categoryControl cc
                        WHERE cc.variable IN ('{$variable}') AND i IN ({$partDql})";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);

        $res=$query->getResult();
        if(is_array($res) && count($res) > 0) $check=true;
        else $check=false;
        return $check;
    }
    private  function checkLoop($url, $variable){

        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url}  OR s.url='{$url}what_to_do/' ";

        $partDql2="SELECT c FROM \export\CategoryControl cc JOIN cc.category c  WHERE   cc.variable={$variable} ";

        $dql="SELECT i FROM \export\Item i JOIN  \export\Category c WHERE c IN ({$partDql2}) AND i IN ({$partDql})) ";


        $query = JEntityHelper::getEntityManager()->createQuery($dql);

        $res=$query->getResult();
        if(is_array($res) && count($res) > 1) $check=true;
        else $check=false;
        return $check;


    }
    private function checkValueLength($url, $variable){
        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url}";
        $dql="SELECT v FROM  \export\Value v
                        JOIN v.item i
                        JOIN v.categoryControl cc
                        WHERE cc.variable IN ('{$variable}') AND i IN ({$partDql})";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);

        $res=$query->getResult();
        $check=false;
        if(is_array($res) && count($res) > 0){
            foreach($res as $value){
                /** @var $value \export\Value */
                if(strlen($value->getValue()) > 0) $check=true;
            }
        }


        return $check;
    }
    private function value($url, $variable){
        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url}";
        $dql="SELECT v FROM  \export\Value v
                        JOIN v.item i
                        JOIN v.categoryControl cc
                        WHERE cc.variable IN ('{$variable}') AND i IN ({$partDql})";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);

        $res=$query->getResult();
        $check=false;
        $cnt=array();
        if(is_array($res) && count($res) > 0){
            foreach($res as $i=>$value){
                /** @var $value \export\Value */
                if(strlen($value->getValue()) > 0){
                    $check=true;
                    $cnt[]=$i;
                }
            }
        }
        if($check){
            /** @var  $data   \export\Value */
            $data=$res[$cnt[0]];
            return $data->getValue();
        } else return "";
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $footerDecoration
     */
    public function setFooterDecoration($footerDecoration)
    {
        $this->footerDecoration = $footerDecoration;
    }

    /**
     * @return
     */
    public function getFooterDecoration()
    {
        return $this->footerDecoration;
    }

    /**
     * @param  $headerDecoration
     */
    public function setHeaderDecoration($headerDecoration)
    {
        $this->headerDecoration = $headerDecoration;
    }

    /**
     * @return
     */
    public function getHeaderDecoration()
    {
        return $this->headerDecoration;
    }

    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $ownerUrl
     */
    public function setOwnerUrl($ownerUrl)
    {
        $this->ownerUrl = $ownerUrl;
    }

    /**
     * @return
     */
    public function getOwnerUrl()
    {
        return $this->ownerUrl;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $urlStyle
     */
    public function setUrlStyle($urlStyle)
    {
        $this->urlStyle = $urlStyle;
    }

    /**
     * @return
     */
    public function getUrlStyle()
    {
        return $this->urlStyle;
    }

    /**
     * @param  $urlText
     */
    public function setUrlText($urlText)
    {
        $this->urlText = $urlText;
    }

    /**
     * @return
     */
    public function getUrlText()
    {
        return $this->urlText;
    }
}