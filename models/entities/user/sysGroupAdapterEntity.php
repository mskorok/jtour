<?php
namespace entities\user;
use entities\user\UserEntity as userSysEntity;
use components\JArrayCollection as ArrayCollection;
use entities\_abstract\sysSimpleAbstract as sysSimpleAbstract;
use entities\notif\sysSubscriberGroupEntity;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.01.13
 * Time: 13:25
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class sysGroupAdapterEntity extends sysGroupEntity
{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
