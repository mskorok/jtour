<?php


namespace application\models\entities\specialOffers;

use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\CategoryControl;
use export\Link;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 19.03.13
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 *  @description сущность спецпредложение
 *
 * @Entity
 * @Table(name="special_offers_entities")
 */
class SpecialOffersAdapterEntity  extends sysAbstract
{
    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 12;
    /**
     * Константа, определяющая из будет ли производиться выборка из оптимуса или из массивов
     */
    const OPTIMUS = false;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description заголовок
     * @var
     */
    protected $title;
    /**
     * @description здесь записывается всякий html с классами для декорации
     * @Column
     * @var
     */
    protected $headerDecoration;
    /**
     * @description а здесь теги закрытия
     * @Column
     * @var
     */
    protected $footerDecoration;
    /**
     * @description  ссылка на рисунок
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description  стиль ссылки
     * @Column
     * @var
     */
    protected $urlStyle;
    /**
     * @description ссылка
     * @Column
     * @var
     */
    protected  $url;
    /**
     * @description текст ссылки   class="price-info"
     * @Column
     * @var
     */
    protected  $urlText;
    /**
     * @description урл страницы на которой находится виджет
     * @Column
     * @var
     */
    protected  $ownerUrl;
    /**
     * @description цена от   class="offer-price"
     * @Column
     * @var
     */
    protected $priceFrom;
    /**
     * @description валюта
     * @Column
     * @var
     */
    protected $currency;
    /**
     * @description продолжительность тура class="offer-text"
     * @Column
     * @var
     */
    protected $duration;
    /**
     * @description страна тура class="country-name"
     * @Column
     * @var
     */
    protected $country;
    /**
     * @description дополнительная информация
     * @Column
     * @var
     */
    protected $additionalInformation;
    /**
     * @description актуально
     * @Column
     * @var boolean
     */
    protected $isActive;
    /**
     * @description категория спецпредложения
     * @Column
     * @var
     */
    protected $label;
    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array();
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array();
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        if(self::OPTIMUS){
            $findResult=self::Offers($criteria);
        }else{
            $data=self::getData($criteria['url']);
            $findResult=self::getItems($data);
        }

        return $findResult;
    }
    protected static function getData($url)
    {

        switch($url)
        {
            case "/fishing/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/?destination=7500&tourTypeIds=1519&dateFrom=04.04.2013&dateTo=18.04.2013&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false",
                        'urlText'=>" Спецпредложения по рыбалке в Норвегии",
                        'ownerUrl'=>"/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/?destination=15161&tourTypeIds=1519&dateFrom=04.04.2013&dateTo=18.04.2013&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false",
                        'urlText'=>"Спецпредложения по рыбалка в Швеции",
                        'ownerUrl'=>"/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/?destination=13924&tourTypeIds=1519&dateFrom=04.04.2013&dateTo=18.04.2013&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false",
                        'urlText'=>"Спецпредложения по рыбалка в Финляндии",
                        'ownerUrl'=>"/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    )
                );
                break;
            case "/index/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/scandinavia/",
                        'urlText'=>" Экскурсионные туры",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 490 €',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Скандинавия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/?destination=15428&dateFrom=27.04.2013&dateTo=29.04.2013&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false&range=false&tourTypeIds=1898#destination=15428&dateFrom=27.04.2013&dateTo=29.04.2013&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false&range=false&tourTypeIds=1898",
                        'urlText'=>"<strong>Туры на майские праздники</strong>",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'2561 $',
                        'duration'=>'Вылеты 27, 28 и 29 апреля а/к British Airways',
                        'additionalInformation'=>'<span class="discount">последние места!</span>',
                        'country'=>'Мексика',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/mexico/",
                        'urlText'=>"Групповые туры с русскоговорящими гидами",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 325 $',
                        'duration'=>'Уникальные экскурсионные туры продолжительностью от 3 до 17 дней!',
                        'additionalInformation'=>'',
                        'country'=>'Мексика',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/usa/new_york/new_york_plus/",
                        'urlText'=>"Туры с посещением Нью-Йорка",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 1345 $',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'США',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/national_parks/usa/",
                        'urlText'=>"Туры по национальным паркам",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 2675 $',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'США',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/iceland_sagas/",
                        'urlText'=>"По следам исландских саг",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 2550 €',
                        'duration'=>'Заезды: 29.04.2013',
                        'additionalInformation'=>'',
                        'country'=>'Исландия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/iceland/may_holiday/",
                        'urlText'=>"Майские праздники в Исландии",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'от 1150 €',
                        'duration'=>'Заезды: 28.04 05.05',
                        'additionalInformation'=>'',
                        'country'=>'Исландия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/?destination=15287&dateFrom=16.04.2013&dateTo=16.04.2013&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false#destination=15287&dateFrom=16.04.2013&dateTo=16.04.2013&range=10-11&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false",
                        'urlText'=>"Туры с прямым перелетом а/к Трансаэро",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от 2755 $',
                        'duration'=>'<strong>Ближайший вылет: 16.04</strong>',
                        'additionalInformation'=>'НОВИНКА!',
                        'country'=>'Ямайка',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/norway/fjords/",
                        'urlText'=>"Фьорды Норвегии",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' 1295 €',
                        'duration'=>'Экскурсионные туры и круизы по фьордам Норвегии',
                        'additionalInformation'=>'',
                        'country'=>'Норвегия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/norway/north/",
                        'urlText'=>"Северная Норвегия",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' 1550 €',
                        'duration'=>'Прямой перелет в северную Норвегию в июле!',
                        'additionalInformation'=>'',
                        'country'=>'Норвегия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/perl_of_scandinavia_capitals_and_fjords/",
                        'urlText'=>"Скандинавские жемчужины: Столицы и Фьорды",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'   от 1575 €',
                        'duration'=>'Заезды: 30 апреля, 28 июня, 12 и 26 июля, 16 августа, 13 сентября',
                        'additionalInformation'=>'',
                        'country'=>'Норвегия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/leto-v-doline-mummi-trollei/",
                        'urlText'=>"Лето в долине мумми троллей",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'  511 €',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Финляндия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/holidays_at_the_holiday_club_caimaa/",
                        'urlText'=>"Отдых в отеле Holiday Club Saimaa",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'    665 €',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Финляндия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/finland/weekend_in_helsinki_by_plane/",
                        'urlText'=>"Выходные в Хельсинки",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от 385 €',
                        'duration'=>' Заезды по пятницам',
                        'additionalInformation'=>'хит продаж!',
                        'country'=>'Финляндия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/shoping_festival/",
                        'urlText'=>"Фестиваль шопинга в Каннах",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от 525 €',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Франция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/ski/france/",
                        'urlText'=>"Горнолыжные туры во Францию",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'   от 545 €',
                        'duration'=>'',
                        'additionalInformation'=>'вкл. перелёт',
                        'country'=>'Франция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/france/",
                        'urlText'=>"Экскурсионные туры во Францию",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'  от 440 € ',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Франция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/stokholm_hockey/",
                        'urlText'=>"Чемпионат Мира по Хоккею 2013",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'  от 400 €',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Швеция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/gotland_regate/",
                        'urlText'=>"Королевская регата 2013",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Швеция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/sweden/weekend_in_stockholm/",
                        'urlText'=>"Выходные в Стокгольме (Шопинг Кулинария История Архитектура)",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от 430 €',
                        'duration'=>'',
                        'additionalInformation'=>'Лучшее предложение!',
                        'country'=>'Швеция',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/brazil/may/",
                        'urlText'=>"Майские праздники в Бразилии",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от  1277 $',
                        'duration'=>'места на гарантии',
                        'additionalInformation'=>'',
                        'country'=>'Бразилия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/brazil/",
                        'urlText'=>"Экскурсионные и пляжные туры в Бразилию",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'  от 1039 $',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'Бразилия',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/argentina/may/",
                        'urlText'=>"Майские праздники в Аргентине",
                        'ownerUrl'=>"index",
                        'priceFrom'=>' от 1385 $',
                        'duration'=>'места на гарантии',
                        'additionalInformation'=>'',
                        'country'=>'Аргентина',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/peru/",
                        'urlText'=>"Групповые туры с русскоговорящими гидами",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'    1688 $',
                        'duration'=>'',
                        'additionalInformation'=>'новинка!',
                        'country'=>'Перу',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/denmark/weekend_in_copenhagen/",
                        'urlText'=>"Выходные в Копенгагене",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'  535 €',
                        'duration'=>'<span style="font-size:12px;"><font color="#CC0000">В подарок - Копенгаген CityCard на 72 часа!!!</font></span>',
                        'additionalInformation'=>'',
                        'country'=>'Дания',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/denmark/holiday_in_legoland/",
                        'urlText'=>"Каникулы в ЛЕГОленде",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'    1090 €',
                        'duration'=>'Заезды: 30.04; 07.05; 23.06; 21.07; 11.08;',
                        'additionalInformation'=>'',
                        'country'=>'Дания',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"http://www.jazztour.ru/tours/sweden/childrens_park_legoland/",
                        'urlText'=>"Детские парки Скандинавии + Леголэнд",
                        'ownerUrl'=>"index",
                        'priceFrom'=>'   1750 €',
                        'duration'=>'  11 августа',
                        'additionalInformation'=>'семейный отдых',
                        'country'=>'Дания',
                        'isActive'=>true
                    )
                );
                break;
            case "/finland/fishing/":
                $data=array(
                    array('headerDecoration'=>'<div class="jazz-layout-wrapper" style="float: none; ">
                                    <div class="wrap">
                                        <div class="jazz_components_container_layout_container ">
                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-container-description" style="width: auto  ">
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <a href="http://www.jazztour.ru/tours/v-chest-dnja-zashitnika-otechestva/" target="_blank" class="jazz-fishing-link">Тур в честь Дня защитника Отечества</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz_components_string_string jazz-fishing-description" style=" ">
                                                                <div class="string">Размещение в рыболовном центре <a href="http://www.jazztour.ru/finland/cottages/naaranlaahti/" target="_blank" style="color:#FFFFFF">Нааранлахти</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-cost" style="width: auto  ">
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz-fishing-currency">
                                                                                <div>
                                                                                    <span>200</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>200</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>262</span>
                                                                                    <span>$</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>8432</span>
                                                                                    <span>p.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz_components_string_string jazz-fishing-places" style=" ">
                                                                                <span class="string"> / за человека</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-float-clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-float-clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>',
                        'footerDecoration'=>"",
                        'img'=>"/images/finland/fishing/fishing-camps/naaranlaahti/91110.jpg",
                        'urlStyle'=>"font-size:12px",
                        'url'=>"",
                        'urlText'=>"",
                        'ownerUrl'=>"/finland/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>'<div class="jazz-layout-wrapper" style="float: none; ">
    <div class="wrap">
        <div class="jazz_components_container_layout_container ">
            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-container-description" style="width: auto  ">
                <div class="jazz-layout-wrapper" style="float: none; ">
                    <div class="wrap">
                        <div class="jazz_components_container_layout_container ">
                            <a href="http://www.jazztour.ru/tours/ribalka-na-reke-torneo/" target="_blank" class="jazz-fishing-link">Финская Лапландия - Рыбалка на реке Торнио</a>
                        </div>
                    </div>
                </div>
                <div class="jazz-layout-wrapper" style="float: none; ">
                    <div class="wrap">
                        <div class="jazz_components_container_layout_container ">
                            <div class="jazz_components_string_string jazz-fishing-description" style=" ">
                                <div class="string">Размещение в рыболовном центре <a href="http://www.jazztour.ru/finland/cottages/lappean-loma/" target="_blank" style="color:#FFFFFF">Лаппеан Лома</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jazz-layout-wrapper" style="float: none; ">
                    <div class="wrap">
                        <div class="jazz_components_container_layout_container ">
                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-cost" style="width: auto  ">
                                <div class="jazz-layout-wrapper" style="float: left; ">
                                    <div class="wrap">
                                        <div class="jazz_components_container_layout_container ">
                                            <div class="jazz-fishing-currency">
                                                <div>
                                                    <span>400</span>
                                                    <span>&amp;euro;</span>
                                                </div>
                                                <div style="display: none;">
                                                    <span>400</span>
                                                    <span>&amp;euro;</span>
                                                </div>
                                                <div style="display: none;">
                                                    <span>524</span>
                                                    <span>$</span>
                                                </div>
                                                <div style="display: none;">
                                                    <span>16864</span>
                                                    <span>p.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jazz-layout-wrapper" style="float: left; ">
                                    <div class="wrap">
                                        <div class="jazz_components_container_layout_container ">
                                            <div class="jazz_components_string_string jazz-fishing-places" style=" ">
                                                <span class="string"> / за неделю</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jazz-float-clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jazz-float-clear"></div>
            </div>
        </div>
    </div>
</div>',
                        'footerDecoration'=>"",
                        'img'=>"/images/finland/fishing/fishing-camps/lappean-loma/134606.jpg",
                        'urlStyle'=>"font-size:12px",
                        'url'=>"",
                        'urlText'=>"",
                        'ownerUrl'=>"/finland/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    )
                );
                break;
            case "/sweden/fishing/":
                $data=array(
                    array('headerDecoration'=>'<div class="jazz-layout-wrapper" style="float: none; ">
                                    <div class="wrap">
                                        <div class="jazz_components_container_layout_container ">
                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-container-description" style="width: auto  ">
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <a href="http://www.jazztour.ru/tours/fishing_weekend_in_sweden/" target="_blank" class="jazz-fishing-link">Рыболовный уикенд в Швеции</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz_components_string_string jazz-fishing-description" style=" ">
                                                                <div class="string">Рыбалка на озерах Юксинген или Боркен. Предполагаемый улов щука, окунь, кумжа, арктический голец и раки. </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-cost" style="width: auto  ">
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz-fishing-currency">
                                                                                <div>
                                                                                    <span>720</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>720</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>943.2</span>
                                                                                    <span>$</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>30355.2</span>
                                                                                    <span>p.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz_components_string_string jazz-fishing-places" style=" ">
                                                                                <span class="string"> / за человека</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-float-clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-float-clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>',
                        'footerDecoration'=>"",
                        'img'=>"/images/norway/fishing/temp/119967.jpg",
                        'urlStyle'=>"font-size:12px",
                        'url'=>"",
                        'urlText'=>"",
                        'ownerUrl'=>"/sweden/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    ),
                    array('headerDecoration'=>'<div class="jazz-layout-wrapper" style="float: none; ">
                                    <div class="wrap">
                                        <div class="jazz_components_container_layout_container ">
                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-container-description" style="width: auto  ">
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <a href="http://www.jazztour.ru/tours/fishing-in-south-sweden/" target="_blank" class="jazz-fishing-link">Рыбалка в южной Швеции </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz_components_string_string jazz-fishing-description" style=" ">
                                                                <div class="string">Рыбалка на озерах южных провинций Швеции или в шхерах Балтийского моря.</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-layout-wrapper" style="float: none; ">
                                                    <div class="wrap">
                                                        <div class="jazz_components_container_layout_container ">
                                                            <div class="jazz-layout-container-layout-wrapper jazz_components_layout_layout jazz-fishing-cost" style="width: auto  ">
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz-fishing-currency">
                                                                                <div>
                                                                                    <span>400</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>400</span>
                                                                                    <span>&amp;euro;</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>524</span>
                                                                                    <span>$</span>
                                                                                </div>
                                                                                <div style="display: none;">
                                                                                    <span>16864</span>
                                                                                    <span>p.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-layout-wrapper" style="float: left; ">
                                                                    <div class="wrap">
                                                                        <div class="jazz_components_container_layout_container ">
                                                                            <div class="jazz_components_string_string jazz-fishing-places" style=" ">
                                                                                <span class="string"> / за неделю</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="jazz-float-clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jazz-float-clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>',
                        'footerDecoration'=>"",
                        'img'=>"/images/sweden/fishing/fishing-centers/yxnerum/119156.jpg",
                        'urlStyle'=>"font-size:12px",
                        'url'=>"",
                        'urlText'=>"",
                        'ownerUrl'=>"/sweden/fishing/",
                        'priceFrom'=>'',
                        'duration'=>'',
                        'additionalInformation'=>'',
                        'country'=>'',
                        'isActive'=>true
                    )
                );
                break;
            case "/norway/fishing/center/":
                $data=array();

                break;
            case "/norway/fishing/north/":
                $data=array();
                break;
            case "/norway/fishing/south/":
                $data=array();
                break;
            case "/norway/fishing/west/":
                $data=array();
                break;
            case "/norway/fishing/south_west/":
                $data=array();
                break;
            case "/usa/fishing/arizona/":
                $data=array();
                break;
            case "/usa/fishing/north_carolina/":
                $data=array();
                break;
            case "/usa/fishing/alaska/":
                $data=array();
                break;
            case "/usa/fishing/florida/":
                $data=array();
                break;
            case "/fishing/komanda-dzhaz-po-robalke/":
                $data=array();
                break;
            case "/norway/fishing/fishing-rivers/":
                $data=array();
                break;
            default:
                $data=array();
                break;
        }
        return $data;
    }
    protected static function getItems($data)
    {
        $res=array();
        $i=0;
        foreach($data as $property)
        {
            $i++;
            $fp=new SpecialOffersAdapterEntity();
            $fp->setHeaderDecoration($property['headerDecoration']);
            $fp->setFooterDecoration($property['footerDecoration']);
            $fp->setAdditionalInformation($property['additionalInformation']);
            $fp->setImg($property['img']);
            $fp->setUrlStyle($property['urlStyle']);
            $fp->setUrl($property['url']);
            $fp->setUrlText($property['urlText']);
            $fp->setOwnerUrl($property['ownerUrl']);
            $fp->setPriceFrom($property['priceFrom']);
            $fp->setDuration($property['duration']);
            $fp->setCountry($property['country']);
            $fp->setIsActive($property['isActive']);
            $fp->setId($i);
            $res[]=$fp;
            unset($fp);
        }
        unset($property);
        return $res;
    }
    private static function Offers($criteria)
    {
        $url=$criteria['url'];
        $type=$criteria['label'];

        switch($type){
            case 'cruise':
                return self::getCruise($url);
                break;
            case 'news':
                return self::getNews($url);
                break;
            case 'tours':
                return self::getTours($url);
                break;
            case 'country':
                return self::getCountry($url);
                break;
            case 'city':
                return self::getCity($url);
                break;
            case 'toppage':
                return self::getToppage($url);
                break;
            case 'fishing':
                return self::getFishing($url);
                break;
            case 'ski':
                return self::getSki($url);
                break;
            case 'index':
                return self::getIndexpage($url);
                break;
        }

        return array();

    }
    private function getCruise($url)
    {
        if(JOptimusHelper::checkLoop($url, 'spo_list')){
            $findResult=array();
            $limit=self::QUERY_LIMIT;

        }

        /**
         *
         *
         *SELECT item.id, item.title, value.id, value.value, variable
        FROM `structure`, `item`, `value`, `category_control`, `link``
        WHERE link.item_id=structure.item_id AND structure.url='/usa/'
        AND item.category_id=527 AND value.item_id=item.id AND value.category_control_id=category_control.id AND item.id=link.linked_item_id
         *
         *
         *!!!!!!success!!!!!!!!!!!!!!!!!!!!!!!!!!
         *
         *
         *
         * loop control.id=4
         *
         *
         *SELECT link.item_id, value, variable  FROM `link`, `value`, `category_control`
         WHERE link.linked_item_id IN (SELECT DISTINCT item_id FROM `structure` WHERE url='/usa/') AND link.item_id=value.item_id
         AND value.category_control_id=category_control.id
         *
         *
         *
         *
         * SELECT link.item_id, value, variable
         FROM `link`, `value`, `category_control`
         WHERE link.linked_item_id=15211 AND link.item_id=value.item_id
         AND value.category_control_id=category_control.id
         *
         *
         * SELECT link.item_id, value, variable FROM  `value` JOIN `link` JOIN `category_control` WHERE link.linked_item_id=15211 AND link.item_id=value.item_id
        AND value.category_control_id=category_control.id
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         * SELECT item.id, item.title, value.id, value.value, variable
         FROM `structure`, `item`, `value`, `category_control`
         WHERE item.id=structure.item_id AND structure.url='/usa/'
         AND item.category_id=527 AND value.item_id=item.id AND value.category_control_id=category_control.id
         */


        $i = 0;
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<table width=100% border=1>'."\r\n";
            $html.= '	<tr>'."\r\n";

            $i = 0;

            $html.= '	<cms:loop.spo_list sort="priority">'."\r\n";
            if($i<12)
            {
                $html.= '		<td valign="top" style="padding-right:45px;width:190px;">'."\r\n";
                $html.= '			<ul class="spo-list">'."\r\n";
                $html.= '				<a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/<cms:value.promo_pic_170x129>" alt="" /></a><br/>'."\r\n";
                $html.= '				<br/>'."\r\n";
                $html.= '				<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                $html.= '				<table border=1 width="95%">'."\r\n";
                $html.= '					<tr>'."\r\n";
                $html.= '						<td align="left"><span class="font16 duration"><cms:value.days> '.getDay('<cms:value.days>').'</span></td>'."\r\n";
                $html.= '						<td align="right" style="padding-right:20pxpx;"><strong class="font18 red">'.(strlen('<cms:value.price>')>0? '<cms:value.price> <cms:value.currency>':'').'</strong></td>'."\r\n";
                $html.= '					</tr>'."\r\n";
                $html.= '				</table>'."\r\n";
                $html.= '			</ul>'."\r\n";
                $html.= '		</td>'."\r\n";

                $array = array(2,5,8,11,14,17,20,23,26,29,32,35,38,41);
                if(in_array($i, $array))
                {
                    $html.= '	<tr>'."\r\n";
                    $html.= '		<td class="mb20">&nbsp;<br/><br/></td>'."\r\n";
                    $html.= '	</tr>'."\r\n";
                }
            }
            $i++;

            $html.= '	</cms:loop.spo_list>'."\r\n";
            $html.= '	</tr>'."\r\n";
            $html.= '	</table>'."\r\n";
            $html.= '	<br/>'."\r\n";
            $html.= '</div>'."\r\n";
        }
        return true;
    }
    private function getTours($url){
        $i = 0;
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $html = '<br/><br/>&nbsp;<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html.= '	<table width=100% border=1 align="center">'."\r\n";
            $html.= '	<tr>'."\r\n";

            $i = 0;

            $html.= '	<cms:loop.spo_list>'."\r\n";
            if($i<12)
            {
                $html.= '		<td  valign="top" width="200" style="">'."\r\n";
                $html.= '			<ul class="spo-list">'."\r\n";
                $html.= '			<li>'."\r\n";
                $html.= '				<a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/<cms:value.promo_pic_170x129>" alt="" /></a><br/>'."\r\n";
                $html.= '				<br/>'."\r\n";
                $html.= '				<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                $html.= '				<table border=1 width="95%">'."\r\n";
                $html.= '					<tr>'."\r\n";
                $html.= '						<td align="left" ><span class="font16 duration"><cms:value.days> '.getDay('<cms:value.days>').'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="font18 red">'.(strlen('<cms:value.price>')>0? '<cms:value.price> <cms:value.currency>':'').'</strong></td>'."\r\n";
                //$html.= '						<td align="right"><strong class="font18 red">'.(strlen('<cms:value.price>')>0? '<cms:value.price> <cms:value.currency>':'').'</strong></td>'."\r\n";
                $html.= '					</tr>'."\r\n";
                $html.= '				</table>'."\r\n";
                $html.= '			</li>'."\r\n";
                $html.= '			</ul>'."\r\n";
                $html.= '		</td>'."\r\n";

                if(in_array($i,array(2,5,8)))
                {
                    $html.= '	</tr>'."\r\n";
                    $html.= '	<tr>'."\r\n";
                    $html.= '		<td class="mb20"><br/><br/></td>'."\r\n";
                    $html.= '	</tr>'."\r\n";
                    $html.= '	<tr>'."\r\n";
                }
            }
            $i++;

            $html.= '	</cms:loop.spo_list>'."\r\n";
            $html.= '	</tr>'."\r\n";
            $html.= '	</table>'."\r\n";
            $html.= '	<br/>'."\r\n";
            $html.= '</div>'."\r\n";
        }
        return true;
    }
    private function getToppage($url){
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $i = 0;
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html.= '		<cms:loop.spo_list sort="priority">'."\r\n";
            if($i<3)
            {
                $html.= '<div class="specialbox mb20">'."\r\n";
                $html.= '    <a font16 href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><b><cms:title></b></a><br/><br/>'."\r\n";
                $html.= '    <a font16  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/<cms:value.promo_pic_182x128>"  alt="" /></a>'."\r\n";
                $html.= '   <table width="100%">'."\r\n";
                $html.= '       <tr>'."\r\n";
                $html.= '            <td>'."\r\n";
                if(strlen('<cms:value.days>')>0)
                    $html.= '               <cms:value.days> '.getDay('<cms:value.days>').'<br />'."\r\n";
                else
                    $html.= '               <i>нет данных</i><br />'."\r\n";
                $html.= '            </td>'."\r\n";
                $html.= '            <td><b class="font13 black"><cms:value.price>'.(strlen('<cms:value.price>')>0?'€':'').'</b><br/></td>'."\r\n";
                $html.= '        </tr>'."\r\n";
                $html.= '    </table>'."\r\n";
                $html.= '</div>	'."\r\n";
            }
            $i ++;
            $html.= '		</cms:loop.spo_list><br/><br/>'."\r\n";
        }
        return true;
    }
    private function getSki($url){
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $i = 0;
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html.= '		<cms:loop.spo_list sort="priority">'."\r\n";
            if($i<3)
            {
                $html.= '<div class="specialbox mb20">'."\r\n";
                $html.= '    <a font16 href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><b><cms:title></b></a><br/><br/>'."\r\n";
                $html.= '    <a font16  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/<cms:value.promo_pic_182x128>"  alt="" /></a>'."\r\n";
                $html.= '   <table width="100%">'."\r\n";
                $html.= '       <tr>'."\r\n";
                $html.= '            <td>'."\r\n";
                if(strlen('<cms:value.days>')>0)
                    $html.= '               <cms:value.days> '.getDay('<cms:value.days>').'<br />'."\r\n";
                else
                    $html.= '               <i>нет данных</i><br />'."\r\n";
                $html.= '            </td>'."\r\n";
                $html.= '            <td><b class="font13 black"><cms:value.price>'.(strlen('<cms:value.price>')>0?'€':'').'</b><br/></td>'."\r\n";
                $html.= '        </tr>'."\r\n";
                $html.= '    </table>'."\r\n";
                $html.= '</div>	'."\r\n";
            }
            $i ++;
            $html.= '		</cms:loop.spo_list><br/><br/>'."\r\n";
        }
        return true;
    }
    private function getCountry($url){
        $i = 0;
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html .= '<ul class="special-offers-list">'."\r\n";
            $html.= '		<cms:loop.spo_list sort="priority">'."\r\n";
            if($i<6)
            {
                $img = (strlen("<cms:value.promo_pic_manual>")>0) ?"<cms:value.promo_pic_manual>":"<cms:value.promo_pic_182x128>";
                $html.= '	<li>'."\r\n";
                if(strlen("<cms:value.nolink>")>0)
                {
                    $html.= '		<img src="/'.$img.'" alt="" class="jazz-gallery-photo-thumbnails" /><br/>'."\r\n";
                    $html.= '		<span class="title"><cms:value.name_mask></span>'."\r\n";
                }
                else
                {



                    if(strlen("<cms:value.promo_pic_url>")>0)
                    {
                        $html.= '		<a  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="<cms:value.promo_pic_url>" alt="" class="jazz-gallery-photo-thumbnails"/></a><br/>'."\r\n";
                        $html.= '		<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";


                    }
                    else
                    {


                        $html.= '		<a  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/'.$img.'" alt="" class="jazz-gallery-photo-thumbnails"/></a><br/>'."\r\n";
                        $html.= '		<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                    }
                }
                $html.= '		<div class="info">'."\r\n";
                $html.= '			<div class="duration"><cms:value.days> '.getDay('<cms:value.days>').'</div>'."\r\n";
                $html.= '			<strong class="price"><cms:value.price><cms:value.currency></strong>'."\r\n";
                $html.= '		</div><br/>'."\r\n";
                $html.= '	</li>'."\r\n";
            }
            $i ++;
            $html.= '		</cms:loop.spo_list>'."\r\n";
            $html.= '</ul>'."\r\n";
            $html.= '</div>	'."\r\n";
        }
        return true;
    }
    private function getCity($url){
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html .= '<ul class="spo-list">'."\r\n";
            $html.= '		<cms:loop.spo_list sort="priority">'."\r\n";
            if($i<3)
            {
                $img = (strlen("<cms:value.promo_pic_manual>")>0) ?"<cms:value.promo_pic_manual>":"<cms:value.promo_pic_170x129>";

                $html.= '	<li>'."\r\n";

                if(strlen("<cms:value.promo_pic_url>")>0)
                {
                    $html.= '		<a  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="<cms:value.promo_pic_url>" alt="" class="jazz-gallery-photo-thumbnails"/></a><br/>'."\r\n";
                    $html.= '		<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";


                }
                else
                {


                    $html.= '		<a  href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/'.$img.'" alt="" class="jazz-gallery-photo-thumbnails"/></a><br/>'."\r\n";
                    $html.= '		<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                }


//						$html.= '		<div class="photoborder"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/'.$img.'" alt="" /></a></div><br/>'."\r\n";
//						$html.= '		<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                $html.= '		<div class="info">'."\r\n";
                $html.= '			<span class="duration"><cms:value.days> '.getDay('<cms:value.days>').'</span>'."\r\n";
                $html.= '			<strong class="price"><cms:value.price><cms:value.currency></strong>'."\r\n";
                $html.= '		</div><br/>'."\r\n";
                $html.= '	</li>'."\r\n";
            }
            $i ++;
            $html.= '		</cms:loop.spo_list>'."\r\n";
            $html.= '</ul>'."\r\n";
            //$html.= '<a href="http://'.$_SERVER["SERVER_NAME"].'/special/">Все спецпредложения</a>'."\r\n";
            $html.= '</div>	'."\r\n";
        }
        return true;
    }
    private function getFishing($url){
        if(strlen('<cms:ifloop.tour_list>')>0)
        {
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html .= '<ul class="spo-list">'."\r\n";
            $html.= '		<cms:loop.tour_list sort="priority">'."\r\n";
            if($i<2)
            {
                $html.= '	<li>'."\r\n";
                $html.= '		<a href="<cms:url>" ><img src="/<cms:value.pic4list>" alt="" /></a><br/>'."\r\n";
                $html.= '		<span class="title"><a font16 href="<cms:url>"><cms:title></a></span>'."\r\n";
                $html.= '		<div class="info">'."\r\n";
                $html.= '			<span class="duration"><cms:value.days> '.getDay('<cms:value.days>').'</span>'."\r\n";
                $html.= '			<strong class="price">от <cms:value.start_price_eur>€</strong>'."\r\n";
                $html.= '		</div><br/>'."\r\n";
                $html.= '	</li>'."\r\n";
            }
            $i ++;
            $html.= '		</cms:loop.tour_list>'."\r\n";

            $html.= '</ul>'."\r\n";

            if(strlen("<cms:value.manual_title_1>")>0)
                $html.= '<a href="<cms:value.manual_link_1>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false"><cms:value.manual_title_1></a><br/>'."\r\n";
            if(strlen("<cms:value.manual_title_2>")>0)
                $html.= '<a href="<cms:value.manual_link_2>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false"><cms:value.manual_title_2></a><br/>'."\r\n";
            if(strlen("<cms:value.manual_title_3>")>0)
                $html.= '<a href="<cms:value.manual_link_3>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false""><cms:value.manual_title_3></a><br/>'."\r\n";
            if(strlen("<cms:value.manual_title_4>")>0)
                $html.= '<a href="<cms:value.manual_link_4>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false""><cms:value.manual_title_4></a><br/>'."\r\n";
            if(strlen("<cms:value.manual_title_5>")>0)
                $html.= '<a href="<cms:value.manual_link_5>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false""><cms:value.manual_title_5></a><br/>'."\r\n";
            if(strlen("<cms:value.manual_title_6>")>0)
                $html.= '<a href="<cms:value.manual_link_6>&dateFrom='.date("d.m.Y").'&dateTo='.date("d.m.Y",strtotime("+14 days")).'&range=false&occupancy%5Badult%5D=2&occupancy%5Bchild%5D=false""><cms:value.manual_title_6></a><br/>'."\r\n";

            $html.= '</div>	'."\r\n";
        }
        return true;
    }
    private function getNews($url){
        $i = 0;
        if(strlen('<cms:ifloop.spo_list>')>0)
        {
            $html = '<div class="special-offers  block">'."\r\n";
            $html.= '	<div class="headline">'."\r\n";
            $html.= '		Спецпредложения'."\r\n";
            $html.= '	</div>'."\r\n";
            $html.= '	<table width=100% border=1>'."\r\n";

            $i = 0;

            $html.= '	<cms:loop.spo_list>'."\r\n";
            if($i<12)
            {
                $html.= '	<tr>'."\r\n";
                $html.= '		<td valign="top" style="width:190px;">'."\r\n";
                $html.= '			<ul class="spo-list">'."\r\n";
                $html.= '				<a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><img src="/<cms:value.promo_pic_170x129>" alt="" /></a><br/>'."\r\n";
                $html.= '				<br/>'."\r\n";
                $html.= '				<span class="title"><a href="'.((strlen('<cms:value.a_href_manual>')>0) ?  '<cms:value.a_href_manual>' : '<cms:loop.a_href><cms:url></cms:loop.a_href>').'"><cms:value.name_mask></a></span>'."\r\n";
                $html.= '				<table border=1 width="95%">'."\r\n";
                $html.= '					<tr>'."\r\n";
                $html.= '						<td align="left"><span class="font16 duration"><cms:value.days> '.getDay('<cms:value.days>').'</span></td>'."\r\n";
                $html.= '						<td align="right" style="padding-right:35px;"><strong class="font18 red">'.(strlen('<cms:value.price>')>0? '<cms:value.price> <cms:value.currency>':'').'</strong></td>'."\r\n";
                $html.= '					</tr>'."\r\n";
                $html.= '				</table>'."\r\n";
                $html.= '			</ul>'."\r\n";
                $html.= '		</td>'."\r\n";
                $html.= '	</tr>'."\r\n";

                $html.= '	<tr>'."\r\n";
                $html.= '		<td class="mb20">&nbsp;<br/><br/></td>'."\r\n";
                $html.= '	</tr>'."\r\n";
            }
            $i++;

            $html.= '	</cms:loop.spo_list>'."\r\n";
            $html.= '	</table>'."\r\n";
            $html.= '	<br/>'."\r\n";
            $html.= '</div>'."\r\n";
        }
        return true;
    }
    private function getIndexpage($url){
        return true;
    }

    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $footerDecoration
     */
    public function setFooterDecoration($footerDecoration)
    {
        $this->footerDecoration = $footerDecoration;
    }

    /**
     * @return
     */
    public function getFooterDecoration()
    {
        return $this->footerDecoration;
    }

    /**
     * @param  $headerDecoration
     */
    public function setHeaderDecoration($headerDecoration)
    {
        $this->headerDecoration = $headerDecoration;
    }

    /**
     * @return
     */
    public function getHeaderDecoration()
    {
        return $this->headerDecoration;
    }

    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $ownerUrl
     */
    public function setOwnerUrl($ownerUrl)
    {
        $this->ownerUrl = $ownerUrl;
    }

    /**
     * @return
     */
    public function getOwnerUrl()
    {
        return $this->ownerUrl;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $urlStyle
     */
    public function setUrlStyle($urlStyle)
    {
        $this->urlStyle = $urlStyle;
    }

    /**
     * @return
     */
    public function getUrlStyle()
    {
        return $this->urlStyle;
    }

    /**
     * @param  $urlText
     */
    public function setUrlText($urlText)
    {
        $this->urlText = $urlText;
    }

    /**
     * @return
     */
    public function getUrlText()
    {
        return $this->urlText;
    }

    /**
     * @param  $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param  $priceFrom
     */
    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;
    }

    /**
     * @return
     */
    public function getPriceFrom()
    {
        return $this->priceFrom;
    }

    /**
     * @param  $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param  $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

}
?>

