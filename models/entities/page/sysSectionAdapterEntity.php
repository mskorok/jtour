<?php
namespace entities\page;
use components\JArrayCollection as ArrayCollection;
use entities\page\sysEntity as pageSysEntity;
use entities\_abstract\sysAbstract as sysAbstract;
use entities\widget\anyEntity as widgetAnyEntity;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.01.13
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 *
 *
 */
class sysSectionAdapterEntity  extends sysSectionEntity
{


    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
