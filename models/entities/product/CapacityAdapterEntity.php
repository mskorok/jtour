<?php
namespace entities\product;
use entities\_abstract\sysAbstract;

/**
 *
 * @date: 24.01.13 - 14:50
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class CapacityAdapterEntity extends CapacityEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
