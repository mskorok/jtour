<?php
namespace application\models\entities\common;

use entities\common\routeMenuItemEntity;
use entities\_abstract\sysGeneralAbstract;
use entities\user\UserEntity as userSysEntity, DateTime;
use \JArrayCollection as ArrayCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use export\Structure;
/**
 *
 * @class ROUTE_MenuItem
 *
 * @date: 12.01.13 - 20:39
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
/**
 * @Entity
 */
class routeMenuItemAdapterEntity extends routeMenuItemEntity{

    const USE_ADAPTER = true;
    const USE_PROTOTYPE= false;


    const USE_DQL= false;
    const BY_ITEM= false;
    const QUERY_LIMIT = 20;
    const RESULT_COLUMN_DQL="  s ";
    const RESULT_COLUMN_SQL="
            id, url, grouping_label, title
             ";
    const JOIN = false;
    /** by Item */
    //const JOIN_SQL = " `value` ON item.id=value.item_id, `category` ON item.category_id = category.id,   `category_control` ON category_control.id = value.category_control_id,   `structure`  ON structure.item_id = item.id";
    /** by Value      */
    const JOIN_SQL =false;
    const FROM = "  \export\Structure s  ";
    /** by Item */
    //const FROM_SQL = "  `item`   ";
    /** by Value      */
    const FROM_SQL = "  `structure`   ";
    const ORDER_BY_KEY="i.id";
    /** by Item */
    //const ORDER_BY_KEY_SQL=" item.id ";
    /** by Value      */
    const ORDER_BY_KEY_SQL=" id ";
    const ORDER_BY_VALUE="DESC";
    const ORDER_BY_VALUE_SQL="DESC";
    const ADDITIONAL_CONDITION = " c.parent IN  " ;
    const ADDITIONAL_CONDITION_SQL = " `parent_id` IN   " ;



    protected  static $responding=array('url' => 'url', 'name' => 'grouping_label');

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return null|array
     */
    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        if(!self::USE_PROTOTYPE){
            $variable = \application\components\JOptimusHelper::getOptimusVariable($criteria['url']);
            foreach($variable as $varName => $value){
                if(strpos($varName,'menu')!==false){
                    $result[$varName] = $value;
                }
            }
            return $result;
        } else {

            if(static::USE_DQL){
                $orderBy=(static::ORDER_BY_KEY )?array(static::ORDER_BY_KEY => static::ORDER_BY_VALUE):array();
                $results=self::getOptimus($criteria,  $orderBy, $limit, $offset);
            } else
            {
                $orderBy=(static::ORDER_BY_KEY_SQL )?array(static::ORDER_BY_KEY_SQL => static::ORDER_BY_VALUE_SQL):array();
                $results=self::getNativeSqlOptimus($criteria,  $orderBy, $limit, $offset);
            }
            $findResult=(static::BY_ITEM)?self::getObjectByItem($results):static::getObjectByValue($results);

            return $findResult;
        }
    }

    protected  static function _aFindAll(){
        $limit=static::QUERY_LIMIT;
        $criteria= array();

        return self::_aFindBy( $criteria,  $orderBy = null, $limit) ;
    }

    protected  static function _aFind($id){

        $criteria = array('id' => $id);

        return self::_aFindBy( $criteria,  $orderBy = null, $limit = 1) ;
    }



    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return self::_aFindBy( $criteria,  $orderBy , $limit = 1) ;
    }
    public static function getOptimus(array $criteria, array $orderBy , $limit , $offset = null)
    {
        $responding=self::$responding;
        $resultColumnsDql=static::RESULT_COLUMN_DQL;
        $additionalCondition=static::ADDITIONAL_CONDITION;
        $queryLine="";
        $order="";
        $and="";
        $join="";
        /*
         *
         * $criteria = array("property" => "value");
         * $responding =array("property" => "column")
         * $queryLine= array("column" => "value")
         * $resultColumns = array("property" => "column")
         * $criteria= array("property1" => "value1", "property1" => "value1");
         * $responding =array("property1" => "column1", "property2" => "column2")
         *
         *
         */
        foreach($criteria as $property => $value)
        {
            $keys=array_keys($responding);
            if(in_array($property, $keys ))
            {
                if($queryLine == "")
                    $queryLine .= " {$responding[$property]}={$value} ";
                else
                    $queryLine .= " AND {$responding[$property]}={$value} ";
            }
        }
        unset($property, $value);
        if($orderBy){
            $key=array_keys($orderBy);
            $value=array_values($orderBy);
            $order=" ORDER BY {$key[0]}  {$value[0]}  ";
            unset($key, $value);
        }

        if((isset($additionalCondition) && !empty($additionalCondition)) && (isset($queryLine) && !empty($queryLine)))    $and=" AND ";


        if(static::JOIN){
            $jn=explode(", ", static::JOIN);
            $ja=array();
            foreach($jn as $j)
            {
                $ja[]=" JOIN ".$j;
            }
            $join=implode(" ", $ja);
        }
        $from= static::FROM;
        $where= (((isset($additionalCondition) && !empty($additionalCondition))) || ((isset($queryLine) && !empty($queryLine))))? "WHERE" : "";
        $dql="SELECT {$resultColumnsDql} FROM {$from}  {$join}
        {$where}   {$additionalCondition} (SELECT  s.id FROM \export\Structure` s WHERE url={$queryLine}) {$order} ";



        $data= JEntityHelper::getEntityManager()->createQuery($dql)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();

        return $data;
    }
    public static function getNativeSqlOptimus(array $criteria, array $orderBy , $limit , $offset = null)
    {
        $responding=self::$responding;
        $resultColumnsSql=static::RESULT_COLUMN_SQL;
        $additionalCondition=static::ADDITIONAL_CONDITION_SQL;
        $queryLine="";
        $limitLine=static::QUERY_LIMIT;
        $limitLine=" LIMIT ".$limitLine;
        $order="";
        $and="";
        /*
         *
         * $criteria = array("property" => "value");
         * $responding =array("property" => "column")
         * $queryLine= array("column" => "value")
         * $resultColumns = array("property" => "column")
         * $criteria= array("property1" => "value1", "property1" => "value1");
         * $responding =array("property1" => "column1", "property2" => "column2")
         *
         *
         */
        foreach($criteria as $property => $value)
        {
            $keys=array_keys($responding);
            if(in_array($property, $keys ))
            {
                if($queryLine == "")
                    $queryLine .= " {$responding[$property]}={$value} ";
                else
                    $queryLine .= " AND {$responding[$property]}={$value} ";
            }
        }
        unset($property, $value);
        if($orderBy){
            $key=array_keys($orderBy);
            $value=array_values($orderBy);
            $order=" ORDER BY {$key[0]}  {$value[0]}  ";
            unset($key, $value);
        }

        if((isset($additionalCondition) && !empty($additionalCondition)) && (isset($queryLine) && !empty($queryLine)))    $and=" AND ";



        $jn=explode(", ", static::JOIN_SQL);
        $ja=array();
        foreach($jn as $j)
        {
            $ja[]=" JOIN ".$j;
        }
        $join=implode(" ", $ja);
        $from= static::FROM_SQL;
        $where= (((isset($additionalCondition) && !empty($additionalCondition))) || ((isset($queryLine) && !empty($queryLine))))? "WHERE" : "";



        $sql="SELECT
            {$resultColumnsSql}
        FROM  {$from} {$join}
        {$where}   {$additionalCondition} (SELECT `id` FROM `structure` WHERE url={$queryLine}) {$order} {$limitLine} ";



            $rsm= new ResultSetMapping;
            $rsm->addEntityResult('export\Structure', 's');
            $rsm->addFieldResult('s', 'id', 'id');
            $rsm->addFieldResult('s', 'grouping_label', 'groupingLabel');
            $rsm->addFieldResult('s', 'url', 'url');
            $rsm->addFieldResult('s', 'title', 'title');







        $query = JEntityHelper::getEntityManager()->createNativeQuery($sql, $rsm);
        $data=$query->getResult();
        return $data;
    }
    protected static function getObjectByItem($results)
    {

        return false;
    }
    protected static function getObjectByValue($results)
    {
        $findResult = array();

        $entity=new routeMenuItemEntity;
        foreach($results as  $value)
        {

            /** @var $value  Structure  */
            $name=$value->getGroupingLabel();
            $text=$value->getTitle();
            $url=$value->getUrl();

            $entity->setName($name);
            $entity->setText($text);
            $entity->setHref($url);


            $findResult[]=$entity;
        }
        return $findResult;

    }
    /**
     *
     *
     * SELECT * FROM `structure` WHERE `parent_id` IN (SELECT `id` FROM `structure` WHERE url="/countries/")
     *
     *
     *
     */
}
