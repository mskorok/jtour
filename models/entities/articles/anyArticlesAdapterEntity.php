<?php
namespace application\models\entities\articles;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;
use application\models\entities\media\sysFileImageAdapterEntity as sysFileImageAdapterEntity;

/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 18.03.13
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 *  @description сущность статья
 *
 * @Entity
 *
 */


class anyArticlesAdapterEntity extends sysAbstract {
    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description заголовок   value.variable='header'
     * @Column
     * @var
     */
    protected $header;
    /**
     * @description краткое описание статьи        value.variable='short_text'
     * @Column
     * @var
     */
    protected $preview;
    /**
     * @description текст  статьи        value.variable='fishing_description'
     * @Column
     * @var
     */
    protected $description;
    /**
     * @description урл по которому статья располагается        structure.url
     * @Column
     * @var
     */
    protected $url;
    /**
     * @description категория к которой принадлежит статья   fishing
     * @Column
     * @var
     */
    protected  $category;
    /**
     * @description категория к которой принадлежит статья   fishing
     * @Column
     * @var
     */
    protected  $categoryId;
    /**
     * @description приоритет статьи, зависит от категории  value.variable= 'priority'
     * @Column
     * @var
     */
    protected $priority;
    /**
     * @description картинка к статье      ссылка на сущность с изображениями, по которой получаем url value.variable='pic4list'
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description размер текста ссылки        value.variable= 'a_href_size'
     * @Column
     * @var
     */
    protected $aSize;
    /**
     * @description фотоальбом к статье(url)   value.variable=array('fishing_gallery')
     * @Column
     * @var array
     */
    protected $gallery;
    /**
     * @description фотоальбом к статье(url)   value.variable=array('fotoalbom')
     * @Column
     * @var array
     */
    protected $fotoalbum;
    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *   $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array('categoryId'=> 'category.id',
                                      'category'  => 'structure.url', // 'category'  => array('value' => "_%fishing/", 'condition' => "LIKE")
                                      'id'=> '',
                                      'header'=> '',
                                      'preview'=> '',
                                      'description'=> '',
                                      'url'=> '',
                                      'priority'=> '',
                                      'img'=> '',
                                      'aSize'=> '',
                                      'gallery'=> '',
                                      'fotoalbum'=> ''
                                      );
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 6,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => " v, i, c, s, cc ",
        'RESULT_COLUMN_SQL'         => " value.id as vId, value.value as vaval,
                                        item.id AS itId, item.title AS ittl,
                                        structure.id as sId, structure.url as surl,
                                        category.id AS cId, category.title AS cttl,
                                        category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl ",
        'JOIN'                      => "  i.value  v , i.category c , i.structure s ,    v.categoryControl cc",
        'JOIN_SQL'                  => "  `item` ON item.id=value.item_id, `category_control` ON category_control.id = value.category_control_id, `structure` ON (item.id = structure.item_id  AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE \"%test%\")), `category` ON item.category_id = category.id  ",
        'FROM'                      => "  \export\Item i  ",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "i.id",
        'ORDER_BY_KEY_SQL'          => " item.id ",
        'ORDER_BY_VALUE'            => "DESC",
        'ORDER_BY_VALUE_SQL'        => "DESC",
        'ADDITIONAL_CONDITION'      => "",
        'ADDITIONAL_CONDITION_SQL'  => "",
        'OFFSET'                    => null


    );
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->gallery=new ArrayCollection();
        $this->fotoalbum=new ArrayCollection();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $keys=array_keys($criteria);
        if(in_array('config', $keys))
            $conf=$criteria['config'];
        else
            $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(false, $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {
        $findResult = array();

        foreach($results as $item)
        {
            /** @var $item Item  */
            $values=$item->getValue();
            if($values){

                $article=new anyArticlesAdapterEntity;
                $str=$item->getStructure();
                /** @var $struct Structure */
                $struct=$str[0];
                $url=$struct->getUrl();
                $article->setUrl($url);
                $desc=$item->getTitle();
                foreach($values as $value)
                {
                    /** @var $value Value */
                    $catCtl=$value->getCategoryControl();
                    $variable=$catCtl->getVariable();
                    switch($variable)
                    {
                        case "header":
                            $header=$value->getValue();
                            $article->setHeader($header);
                            break;
                        case "fishing_description":
                            $description=$value->getValue();
                            $article->setDescription($description);
                            break;
                        case "short_text":
                            $preview=$value->getValue();
                            $article->setPreview($preview);
                            break;
                        case "priority":
                            $priority=$value->getValue();
                            $article->setPriority($priority);
                            break;
                        case "fishing_gallery":
                            $gallery=$value->getValue();
                            $galleryThumbnail=$catCtl->getGalleryThumbnail()?$catCtl->getGalleryThumbnail():119;
                            $galleryPicture=$catCtl->getGalleryPicture()?$catCtl->getGalleryPicture():119;
                            $gallery=sysFileImageAdapterEntity::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );
                            $article->setGallery($gallery);
                            break;
                        case "fotoalbom":
                            $gallery=$value->getValue();
                            $galleryThumbnail=$catCtl->getGalleryThumbnail()?$catCtl->getGalleryThumbnail():119;
                            $galleryPicture=$catCtl->getGalleryPicture()?$catCtl->getGalleryPicture():119;
                            $gallery=sysFileImageAdapterEntity::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );
                            $article->setFotoalbum($gallery);
                            break;
                        case "pic4list":
                            $id=$value->getValue();
                            $img=sysFileImageAdapterEntity::getImage($id);
                            $article->setImg($img);
                            break;
                        case "a_href_size":
                            $asz=$value->getValue();
                            $article->setASize($asz);
                            break;
                    }
                }
                if(self::cnt($article))
                $findResult[] =$article;
            } else continue;

        }
        return $findResult;
    }

    protected static function getObjectByValue($results)
    {
        $article=new anyArticlesAdapterEntity;
        $findResult = array();
        $itemId=null;
        foreach($results as $key => $value)
        {
            /** @var $item Item  */
            /** @var $value Value */
            $item=$value->getItem();
            $desc=$item->getTitle();
            $iId=$item->getId();
            if($itemId != $iId)
            {
                if($key != 0)
                {
                    $findResult[]=$article;
                    $article=new anyArticlesAdapterEntity;

                }
                $itemId=($itemId != $iId)?$iId:$itemId;
                // сборка сущности
                $str=$item->getStructure();
                /** @var $struct Structure */
                $struct=$str[0];
                $url=$struct->getUrl();
                $article->setUrl($url);


            }

            $catCtl=$value->getCategoryControl();

            $variable=$catCtl->getVariable();

            switch($variable)
            {
                case "header":
                    $header=$value->getValue();
                    $article->setHeader($header);
                    break;
                case "fishing_description":
                    $description=$value->getValue();
                    $article->setDescription($description);
                    break;
                case "short_text":
                    $preview=$value->getValue();
                    $article->setPreview($preview);
                    break;
                case "priority":
                    $priority=$value->getValue();
                    $article->setPriority($priority);
                    break;
                case "fishing_gallery":
                    $gallery=$value->getValue();
                    $galleryThumbnail=$catCtl->getGalleryThumbnail()?$catCtl->getGalleryThumbnail():119;
                    $galleryPicture=$catCtl->getGalleryPicture()?$catCtl->getGalleryPicture():119;
                    $gallery=sysFileImageAdapterEntity::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );
                    $article->setGallery($gallery);
                    break;
                case "fotoalbom":
                    $gallery=$value->getValue();
                    $galleryThumbnail=$catCtl->getGalleryThumbnail()?$catCtl->getGalleryThumbnail():119;
                    $galleryPicture=$catCtl->getGalleryPicture()?$catCtl->getGalleryPicture():119;
                    $gallery=sysFileImageAdapterEntity::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );
                    $article->setFotoalbum($gallery);
                    break;
                case "pic4list":
                    $id=$value->getValue();
                    $img=sysFileImageAdapterEntity::getImage($id);
                    $article->setImg($img);
                    break;
                case "a_href_size":
                    $asz=$value->getValue();
                    $article->setASize($asz);
                    break;
            }
            $findResult[]=$article;
        }
        unset($value, $catCtl, $key, $variable);

        return $findResult;
    }
    private static function cnt($entity)
    {
        return true;
    }



    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle() {
        // TODO: Implement getTitle() method.
    }


    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param  $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
    }

    /**
     * @return
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param  $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param  $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $fotoalbum
     */
    public function setFotoalbum($fotoalbum)
    {
        $this->fotoalbum = $fotoalbum;
    }

    /**
     * @return array|ArrayCollection
     */
    public function getFotoalbum()
    {
        return $this->fotoalbum;
    }

    /**
     * @param  $aSize
     */
    public function setASize($aSize)
    {
        $this->aSize = $aSize;
    }

    /**
     * @return
     */
    public function getASize()
    {
        return $this->aSize;
    }


    /**
     * @param  $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param $gallery
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return array|ArrayCollection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

/**
 *
 *SELECT value.id as vId, value.value as vaval,
item.id AS itId, item.title AS ittl,
structure.id as sid, structure.url as surl,
category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl
FROM `value`
JOIN `item` ON value.item_id = item.id
JOIN `category_control` ON  value.category_control_id = category_control.id
JOIN `category` ON (item.category_id = category.id AND category.id  IN (606))
JOIN `structure` ON
(item.id = structure.item_id AND structure.url LIKE "%fishing%"
AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE "%test%"))

 *
 * SELECT value.id as vId, value.value as vaval,
item.id AS itId, item.title AS ittl,
structure.id as sid, structure.url as surl,
category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl
FROM `value`
JOIN `item` ON value.item_id = item.id
JOIN `category_control` ON  value.category_control_id = category_control.id
JOIN `structure` ON
(item.id = structure.item_id AND structure.url LIKE "%fishing%"
AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE "%test%"))


ORDER BY value.id ASC
 *
 *
 *
 *
 *value.id as vId, value.value as vaval,
item.id AS itId, item.title AS ittl,
structure.id as sid, structure.url as surl,
category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,

 *
 *
 *
 *
 *
 *
 *
 *value.id,  picture_value.order, value.value, category_control.variable, category_control.gallery_thumbnail, structure.url, picture.id, item.id, picture_size.resolution
 *
 *
 *SELECT value.id,  picture_value.order, value.value, category_control.variable, category_control.label,
 structure.url, picture.id, item.id, picture_size.resolution
FROM `value`
JOIN `picture_value` ON value.id =picture_value.value_id
JOIN `picture` ON picture.id =picture_value.picture_id
JOIN `item` ON value.item_id = item.id
JOIN `category_control` ON  value.category_control_id = category_control.id
JOIN `structure` ON
(item.id = structure.item_id AND structure.url LIKE "%fishing%"
AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE "%test%"))
JOIN `folder` ON folder.id=picture.folder_id
JOIN `picture_size` ON category_control.gallery_thumbnail=picture_size.id
WHERE  picture.id  IN (SELECT picture.id FROM `value`, `item`,  `category`,   `structure`, `picture_value`,  `picture`
WHERE category.id =606
AND item.category_id = category.id
AND item.id = structure.item_id
AND structure.url LIKE "%fishing%"
AND value.item_id = item.id
AND  picture.id =picture_value.picture_id
AND value.id =picture_value.value_id
)
ORDER BY value.id, picture_value.order ASC
 *
 *
 *
 *
 *
 *
 *
 *
 *SELECT value.id,  picture_value.order, value.value, category_control.variable, category_control.label, structure.url, picture.id, item.id
FROM `picture`
JOIN `picture_value` ON picture.id =picture_value.picture_id
JOIN `value` ON (picture.id =picture_value.picture_id AND value.id =picture_value.value_id)
JOIN `item` ON value.item_id = item.id
JOIN `category_control` ON  value.category_control_id = category_control.id
JOIN `structure` ON
(item.id = structure.item_id AND structure.url LIKE "%fishing%"
AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE "%test%"))
WHERE  picture.id  IN (SELECT picture.id FROM `value`, `item`,  `category`,   `structure`, `picture_value`,  `picture`
WHERE category.id =606
AND item.category_id = category.id
AND item.id = structure.item_id
AND structure.url LIKE "%fishing%"
AND value.item_id = item.id
AND  picture.id =picture_value.picture_id
AND value.id =picture_value.value_id
)
ORDER BY value.id, picture_value.order ASC
 *
 *
 */
}