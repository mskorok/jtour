<?php
namespace entities\contract;
use entities\_abstract\sysAbstract;
use components\JArrayCollection as ArrayCollection;

/**
 * @date: 26.01.13 - 15:26
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class ContractChildAgeAdapterEntity extends ContractChildAgeEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}
