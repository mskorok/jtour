<?php
namespace entities\contract;
use \entities\contract\ContractEntity,
    \entities\contract\ContractServiceEntity,
    \entities\contract\ContractOptionEntity,
    \entities\contract\ContractSeasonGroupEntity,
    \entities\policy\ChargingPolicyEntity,
    \entities\contract\PriceElementEntity;
/**
 *
 * @date: 29.01.13 - 14:35
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class ContractElementAdapterEntity extends ContractElementEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
