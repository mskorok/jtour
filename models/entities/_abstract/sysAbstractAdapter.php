<?php
namespace entities\_abstract;

use services\JCacheService as CacheService;
use Doctrine\Common\Util\ClassUtils;
use services\JEventService as JEventService;
use services\cacheCallback\JMessageCacheCallback as MessageCacheCallback;
use services\cacheCallback\JTranslateMessageCacheCallback as TranslateMessageCacheCallback;
use components\JArrayCollection as ArrayCollection;
use Doctrine\Common\Persistence\Proxy;
use helpers\JEntityHelper as JEntityHelper;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 *
 * @class sysAbstract
 * @date: 12.01.13 - 20:39
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
abstract class sysAbstractAdapter extends sysAbstract{

    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
