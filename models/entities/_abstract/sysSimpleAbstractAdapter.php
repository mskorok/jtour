<?php
namespace entities\_abstract;

/**
 *
 * @description От данного класса должны наследоваться все простые сущности - конфиги, списки и т.д.
 */
abstract class sysSimpleAbstractAdapter extends sysSimpleAbstract{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}
