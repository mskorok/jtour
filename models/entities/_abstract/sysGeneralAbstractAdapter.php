<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 29.08.12
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */
namespace entities\_abstract;
use services\JDefaultService as DefService;
use entities\page\siteUserEntity;
use entities\page\sysClassEntity as sysClassEntity;
use entities\sys\sysTemplateEntity as sysTemplateEntity;

/**
 * @description От данного класса должны наследоваться все сущности которым нужна страница
 */
abstract class sysGeneralAbstractAdapter extends sysGeneralAbstract{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
