<?php


namespace application\models\entities\fishing;

use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 04.04.13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 *
 *
 *   @description сущность места рыбалки
 *
 * @Entity
 */

class FishingPlacesAdapterEntity extends sysAbstract {

    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /**
     * Константа, определяющая из будет ли производиться выборка из оптимуса или из массивов
     */
    const OPTIMUS = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description здесь записывается всякий html с классами для декорации
     * @Column
     * @var
     */
    protected $headerDecoration;
    /**
     * @description а здесь теги закрытия
     * @Column
     * @var
     */
    protected $footerDecoration;
    /**
     * @description  ссылка на рисунок
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description  стиль ссылки
     * @Column
     * @var
     */
    protected $urlStyle;
    /**
     * @description ссылка
     * @Column
     * @var
     */
    protected  $url;
    /**
     * @description текст ссылки
     * @Column
     * @var
     */
    protected  $urlText;
    /**
     * @description урл страницы на которой находится виджет
     * @Column
     * @var
     */
    protected  $ownerUrl;
    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array();
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array();
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        if(self::OPTIMUS){
            $findResult=self::Where_fishing($criteria['url']);
        }else{
            $data=self::getData($criteria['url']);
            $findResult=self::getItems($data);
        }

        return $findResult;
    }
    protected static function getData($url)
    {
        switch($url)
        {
            case "/fishing/":
                $data=array(
                    array('headerDecoration'=>"",
                          'footerDecoration'=>"",
                          'img'=>null,
                          'urlStyle'=>"font-size:20px",
                          'url'=>"/norway/fishing/",
                          'urlText'=>"Рыбалка в Норвегии",
                          'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:18px",
                        'url'=>"/finland/fishing/",
                        'urlText'=>"Рыбалка в Финляндии",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:18px",
                        'url'=>"/sweden/fishing/",
                        'urlText'=>"Рыбалка в Швеции",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:18px",
                        'url'=>"/iceland/fishing/",
                        'urlText'=>"Рыбалка в Исландии",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/denmark/fishing/",
                        'urlText'=>"Рыбалка в Дании",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/usa/fishing/",
                        'urlText'=>"Рыбалка в США",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/brazil/fishing/",
                        'urlText'=>"Рыбалка в Бразилии",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/argentina/fishing/",
                        'urlText'=>"Рыбалка в Аргентине",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/chile/fishing/",
                        'urlText'=>"Рыбалка в Чили",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/venezuela/fishing/",
                        'urlText'=>"Рыбалка в Венесуэле",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/costa_rica/fishing/",
                        'urlText'=>"Рыбалка в Коста Рике",
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>"Рыбалка на Кубе",
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/cuba/fishing/",
                        'urlText'=>null,
                        'ownerUrl'=>"/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/mexico/fishing/",
                        'urlText'=>"Рыбалка в Мексике",
                        'ownerUrl'=>"/fishing/")
                );
                break;
            case "/usa/fishing/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/usa/fishing/arizona/",
                        'urlText'=>"Аризона",
                        'ownerUrl'=>"/usa/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/usa/fishing/alaska/",
                        'urlText'=>"Аляска: речная и морская рыбалка",
                        'ownerUrl'=>"/usa/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/usa/fishing/north_carolina/",
                        'urlText'=>"Северная Королина",
                        'ownerUrl'=>"/usa/fishing/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:18px",
                        'url'=>"/usa/fishing/florida/",
                        'urlText'=>"Флорида",
                        'ownerUrl'=>"/usa/fishing/")
                );
                break;
            case "/finland/fishing/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/finland/fishing/fishing_center/",
                        'urlText'=>"Рыбалка в Финляндии",
                        'ownerUrl'=>"/finland/fishing/")
                );
                break;
            case "/sweden/fishing/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/sweden/fishing/fishing_centres/",
                        'urlText'=>"Рыболовные Центры в Швеции",
                        'ownerUrl'=>"/sweden/fishing/")
                );
                break;
            case "/norway/fishing/center/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:12px",
                        'url'=>"/norway/fishing/center/fishing-in-trondelag/",
                        'urlText'=>"Рыболовные центры в Trondelag",
                        'ownerUrl'=>"/norway/fishing/center/")
                );
                break;
            case "/norway/fishing/north/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:18px",
                        'url'=>"/norway/fishing/north/fishing-in-finnmark/",
                        'urlText'=>"Рыболовные центры Finnmark",
                        'ownerUrl'=>"/norway/fishing/north/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/norway/fishing/north/fishing-in-troms/",
                        'urlText'=>"Рыболовные центры в Troms",
                        'ownerUrl'=>"/norway/fishing/north/"),
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/norway/fishing/north/fishing-in-lofoten-vesteralen-nordland/",
                        'urlText'=>"Рыболовные центры Lofoten, Vesteralen, Nordland",
                        'ownerUrl'=>"/norway/fishing/north/")
                );
                break;
            case "/norway/fishing/south/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/norway/fishing/south/fishing-in-rogaland-agder-buskerud/",
                        'urlText'=>"Рыболовные центры в Rogaland, Agder, Buskerud",
                        'ownerUrl'=>"/norway/fishing/south/")
                );
                break;
            case "/norway/fishing/west/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/norway/fishing/fishing-in-more-og-romsdal/",
                        'urlText'=>"Рыболовные центры в More Og Romsdal",
                        'ownerUrl'=>"/norway/fishing/west/")
                );
                break;
            case "/norway/fishing/south_west/":
                $data=array(
                    array('headerDecoration'=>"",
                        'footerDecoration'=>"",
                        'img'=>null,
                        'urlStyle'=>"font-size:14px",
                        'url'=>"/norway/fishing/fishing-in-sogn-og-fjordane-hordaland/",
                        'urlText'=>"Рыболовные центры в Sogn Og Fjordane, Hordaland",
                        'ownerUrl'=>"/norway/fishing/south_west/")
                );
                break;
            case "/usa/fishing/arizona/":
                $data=array();
                break;
            case "/usa/fishing/north_carolina/":
                $data=array();
                break;
            case "/usa/fishing/alaska/":
                $data=array();
                break;
            case "/usa/fishing/florida/":
                $data=array();
                break;
            case "/fishing/komanda-dzhaz-po-robalke/":
                $data=array();
                break;
            case "/norway/fishing/fishing-rivers/":
                $data=array();
                break;
            default:
                $data=array();
                break;
        }
        return $data;
    }
    protected static function getItems($data)
    {
        $res=array();
        foreach($data as $property)
        {
            $fp=new FishingPlacesAdapterEntity();
            $fp->setHeaderDecoration($property['headerDecoration']);
            $fp->setFooterDecoration($property['footerDecoration']);
            $fp->setImg($property['img']);
            $fp->setUrlStyle($property['urlStyle']);
            $fp->setUrl($property['url']);
            $fp->setUrlText($property['urlText']);
            $fp->setOwnerUrl($property['ownerUrl']);
            $res[]=$fp;
        }
        unset($property);
        return $res;
    }

    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }
    /******************************************************************************************************************
     * New implementation
     ******************************************************************************************************************/

    protected static function Where_fishing($pageUrl)
    {


        // $url=\Yii::app()->request->getScriptUrl();
        // $url=basename($url. ".php")."/";
        $url=$pageUrl;
        $partDql="SELECT i FROM \export\Structure s JOIN s.item i WHERE   s.url={$url}";

        $dql="SELECT DISTINCT sl, i, s, cc FROM \export\StructureLink sl
                JOIN sl.item i
                JOIN sl.structure s
                JOIN sl.categoryControl cc
                WHERE cc IN
                (SELECT cc FROM \export\CategoryControl
                WHERE cc.variable='where') AND i IN ({$partDql})
                ";
        $query = JEntityHelper::getEntityManager()->createQuery($dql);
        $also=$query->getResult();
        unset($dql);
        $findResult=array();

        if($also){
            foreach($also as $res){
                /** @var  $res \export\StructureLink */
                $entity= new FishingPlacesAdapterEntity();

                $entity->setUrl($res->getStructure()->getUrl());
                $entity->setUrlText($res->getStructure()->getTitle());
                $entity->setOwnerUrl($url);

                $item=$res->getItem();
                $dql="SELECT v, cc FROM  \export\Value v
                        JOIN v.item i
                        JOIN v.categoryControl cc
                        WHERE cc.variable IN ('a_href_size') AND i.id=\"{$item}\"";
                $query = JEntityHelper::getEntityManager()->createQuery($dql);

                $values=$query->getResult();
                if($values){
                    /** @var  $value \export\Value */
                    $value=$values[0];
                    $size=$value->getValue()."px";
                    $entity->setUrlStyle($size);
                }

                $findResult[]=$entity;

            }
            unset($res);
        }
        $findResult=self::checkNullArray($findResult);
        return $findResult;
    }


    private  static function checkNull(array $check)
    {

        $res=false;
        foreach($check as $null)
        {

            $fl1=($null === "")?true:false;
            $fl2=($null === null)?true:false;
            $fl3=((is_array($null) && (count($null) == 0)))?true:false;
            if($fl1 || $fl2 || $fl3) $res=$res || false;
            else $res=$res || true;
        }



        return $res;
    }
    private   static function checkNullArray($check)
    {
        $flag=false;
        foreach($check as $checkedItem)
        {
            if(($checkedItem instanceof FishingPlacesAdapterEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                $flag=true;
        }
        $res=$flag?$check:array();
        return $res;
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $footerDecoration
     */
    public function setFooterDecoration($footerDecoration)
    {
        $this->footerDecoration = $footerDecoration;
    }

    /**
     * @return
     */
    public function getFooterDecoration()
    {
        return $this->footerDecoration;
    }

    /**
     * @param  $headerDecoration
     */
    public function setHeaderDecoration($headerDecoration)
    {
        $this->headerDecoration = $headerDecoration;
    }

    /**
     * @return
     */
    public function getHeaderDecoration()
    {
        return $this->headerDecoration;
    }

    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $ownerUrl
     */
    public function setOwnerUrl($ownerUrl)
    {
        $this->ownerUrl = $ownerUrl;
    }

    /**
     * @return
     */
    public function getOwnerUrl()
    {
        return $this->ownerUrl;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  $urlStyle
     */
    public function setUrlStyle($urlStyle)
    {
        $this->urlStyle = $urlStyle;
    }

    /**
     * @return
     */
    public function getUrlStyle()
    {
        return $this->urlStyle;
    }

    /**
     * @param  $urlText
     */
    public function setUrlText($urlText)
    {
        $this->urlText = $urlText;
    }

    /**
     * @return
     */
    public function getUrlText()
    {
        return $this->urlText;
    }

}