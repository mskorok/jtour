<?php
namespace entities\media;
use entities\sys\sysTemplateEntity;
use entities\geo\anyTypeEntity;
use entities\_abstract\sysSimpleAbstract;

/**
 *
 * @description типы форматов файлов
 */
class sysFileFormatAdapterEntity extends FileFormatEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}