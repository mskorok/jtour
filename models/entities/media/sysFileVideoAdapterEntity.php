<?php
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена
 */
namespace application\models\entities\media;

use helpers\JEntityHelper;
use entities\media\VideoFileEntity;
/**
 * @description Таблица видео-файлов
 *
 */
class sysFileVideoAdapterEntity extends VideoFileEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

    protected static function _aFindByDQL($dql){
        return true;
    }
}
