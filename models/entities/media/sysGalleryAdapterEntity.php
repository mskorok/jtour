<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.04.13
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 * @description сущность рыбалка
 *
 * @Entity
 * @Table("gallery_entity")
 */

namespace application\models\entities\media;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;

class sysGalleryAdapterEntity extends sysAbstract{

    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса !!!внимание при выборке более 25 сущностей апач получает контрольный в голову!!!!
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description title
     * @Column
     * @var
     */
    protected $title;
    /**
     * @description описание
     * @Column
     * @var
     */
    protected $description;
    /**
     * @description изображения к галерее
     * @Column
     * @var
     */
    protected $images;
    /**
     * @description изображения для предпросмотра
     * @Column
     * @var
     */
    protected $thumbnail;
    /**
     * @description флаг, определяющий активна ли галерея в данный момент
     * @Column
     * @var
     */
    protected $isActive;
    /**
     * @description урл
     * @Column
     * @var
     */
    protected $url;
    /**
     * @description тип галереи(в зависимости от скрипта используемого галереей)
     * @Column
     * @var
     */
    protected $type;
    /**
     * @description внешний адрес js скрипта галереи
     * @Column
     * @var
     */
    protected $extUrl;
    /**
     * @description  адрес js скрипта галереи на сервере
     * @Column
     * @var
     */
    protected $scriptUri;
    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array();
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 8,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => " v, i, c, s, cc, t ",
        'RESULT_COLUMN_SQL'         => " value.id as vid, value.value as vaval, value.item_id as valit, value.version_id as valver,
                                         value.category_control_id as valccl,
                                         item.id as itid, item.title as ittl,
                                         structure.id as sid, structure.url as surl,
                                         category_control.variable as cclvr, category_control.id as cclid ",
        'JOIN'                      => " i.value  v , i.category c , i.structure s ,  s.template t ,  v.categoryControl cc",
        'JOIN_SQL'                  => "  `item` ON item.id=value.item_id,
                                           `category_control` ON category_control.id = value.category_control_id,
                                           `structure`  ON structure.item_id = item.id,
                                           `category` ON item.category_id = category.id ",
        'FROM'                      => "  \export\Item i  ",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "i.id",
        'ORDER_BY_KEY_SQL'          => " item.id ",
        'ORDER_BY_VALUE'            => "DESC",
        'ORDER_BY_VALUE_SQL'        => "DESC",
        'ADDITIONAL_CONDITION'      => "",
        'ADDITIONAL_CONDITION_SQL'  => "",
        'OFFSET'                    => null


    );
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();

    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(false, $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {
        return true;
    }
    protected static function getObjectByValue($results)
    {
        return true;
    }

    /**
     * $description возвращает массив, первый элемент которого - thumbnail, второй - изображение
     */
    public function getImagesWithThumbnail()
    {

    }
    public static function  renderImagesForGallery(array $images)
    {

        foreach($images as $image)
        {
            if($image instanceof sysFileImageAdapterEntity)
            {
                echo "<li><img src='".$image->getUri()."'></li>";

            } else continue;
        }

    }


    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle() {
        // TODO: Implement getTitle() method.
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param  $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param  $extUrl
     */
    public function setExtUrl($extUrl)
    {
        $this->extUrl = $extUrl;
    }

    /**
     * @return
     */
    public function getExtUrl()
    {
        return $this->extUrl;
    }

    /**
     * @param  $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param  $scriptUri
     */
    public function setScriptUri($scriptUri)
    {
        $this->scriptUri = $scriptUri;
    }

    /**
     * @return
     */
    public function getScriptUri()
    {
        return $this->scriptUri;
    }

    /**
     * @param  $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param  $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }




}