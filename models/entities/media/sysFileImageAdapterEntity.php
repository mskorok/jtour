<?php

namespace application\models\entities\media;
use enums\PositionEnum;
use entities\media\ImageFileEntity as ImageFileEntity;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\CategoryControl;
use export\Link;
use entities\_abstract\sysAbstract;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use \export\Folder as Folder;
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена

 *
 * @Entity
 * @Table(name="media_file_image_entities")
 *
 * @description сущность изображение
 */

  class sysFileImageAdapterEntity extends ImageFileEntity{

    /******************************************************************************************************************
    * БЛОК КОНСТАНТ АДАПТЕРА
    ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/



    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     * $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array(
        'id'=> '',
        'title'=> '',
        'height'=> '',
        'width'=> '',
        'waterMarkPosition'=> '',
        'folder'=> '',
        'typeMedia'=> '',
        'format'=> '',
        'description'=> '',
        'size'=> '',
        'fileNameExtensions'=> '',
        'label'=> '',
        'uri'=> '',
        'optimusPicId'=>'picture.id',
        'optimusSizeId'=> '',
        'optimusSize'=> ''

    );
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 5,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => " v,  i, s, cc, p ",
        'RESULT_COLUMN_SQL'         => " value.id as vId, value.value as vaval,
                                         item.id AS itId, item.title AS ittl,
                                         structure.id as sId, structure.url as surl,
                                         category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,
                                         picture.id AS pcId, picture.format AS format, picture.url AS ext, picture.title AS label

                                         ",
        'JOIN'                      => " i.value  v , v.categoryControl cc ,  v.picture p , i.structure s ",
        'JOIN_SQL'                  => "   `item` ON value.item_id = item.id, `category` ON item.category_id = category.id, `category_control` ON (value.category_control_id = category_control.id AND category_control.variable NOT IN ('')), `structure`  ON (item.id = structure.item_id AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE '%test%')), `picture_value` ON value.id =picture_value.value_id, `picture` ON picture.id =picture_value.picture_id",
        'FROM'                      => "\export\Item i",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "v, p",
        'ORDER_BY_KEY_SQL'          => " value.id, picture_value.order ",
        'ORDER_BY_VALUE'            => "ASC",
        'ORDER_BY_VALUE_SQL'        => "ASC",
        'ADDITIONAL_CONDITION'      => " s.url NOT IN (SELECT s.url FROM s WHERE s.url LIKE '%test%') AND cc.variable NOT IN ('') ",
        'ADDITIONAL_CONDITION_SQL'  => "",
        'OFFSET'                    => null


    );
    /**
     *
     * @description значение переменной variable для отдельных изображений
     *
     * @var
     */
    protected static $image=array("pic4list", "foto", "pic1", "rivers_map", "maps", "deck1_foto",  "deck2_foto",
        "deck3_foto", "deck4_foto", "deck5_foto", "deck6_foto", "deck7_foto", "deck8_foto", "deck9_foto", "map_right",
        "dop_pic", "hotel_facilities", "cruisesmap");
    /**
     *
     * @description  значение переменной variable для галерей
     *
     *
     * @var
     */
    protected static $gallary=array( "gallery", "gallery_preview", "flash_gallery_all", "fotoalbom", "flash_gallery",
        "promo_gallery", "photos", "fishing_gallery", "gallery_preview_winter", "gallery_winter", "preview_photos",
        "new_year_gallery", "beauty_photos", "active_1_photos", "active_2_photos", "active_3_photos", "active_4_photos",
        "active_5_photos", "active_6_photos", "active_8_photos", "active_9_photos", "child_1_photo", "child_2_photo",
        "child_3_photo", "child_4_photo", "child_5_photo", "child_6_photo", "south_north_photos", "north_south_photos",
        "booklet_photos_1", "booklet_photos_2", "snowman_photos_1", "snowman_photos_2",  "snowboard_photos_1",
        "snowboard_photos_2", "snowboard_photos_3", "snowboard_photos_4", "snowboard_photos_5", "skiing_photos_1",
        "skiing_photos_2	", "skiing_photos_2", "skiing_photos_3", "driving_skiing_photos", "active_photos",
        "htg_photos_1", "conference_photo1", "gallery_vertical", "additional1_gallery", "additional2_gallery",
        "restruns_gallery", "flash", "infro_gallery");

    /**
     **** SQL ****
     *
     *
     * SELECT value.id AS vId, value.value AS vaval,
    picture_value.value_id AS pvvId, picture_value.picture_id AS pvpId,
    item.id AS itId, item.title AS ittl,
    folder.short_url AS dir, folder.name AS dirname, folder.id AS dirId,
    structure.id as sid, structure.url as surl,
    category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,
    picture.id AS pcId, picture.format AS format, picture.url AS ext,
    picture.title AS label,picture.folder_id AS pcdir,

    FROM `value`
    JOIN  `item` ON value.item_id = item.id
    JOIN  `category` ON item.category_id = category.id
    JOIN  `category_control` ON value.category_control_id = category_control.id
    JOIN  `structure`  ON item.id = structure.item_id
    JOIN  `picture_value` ON value.id =picture_value.value_id
    JOIN  `picture` ON picture.id =picture_value.picture_id

    WHERE category.id =606
    AND structure.url LIKE "%fishing%"
   AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE '%test%')
    AND category_control.variable NOT IN ("")
    ORDER BY value.id, picture_value.order ASC
     *
     *
     *
     *
     **** dql ****
     *
     *SELECT v,  i, s, cc, p FROM \export\Item i JOIN i.value  v JOIN  v.categoryControl cc JOIN v.picture p JOIN i.structure s
     *
     * WHERE s.url NOT IN (SELECT s.url FROM s WHERE s.url LIKE '%test%') AND cc.variable NOT IN ("") AND s.url LIKE "%fishing%"
     * AND category.id =606 ORDER BY v.id ASC
     *
     *
     */
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();

    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindByDQL($dql){
        $data = JOptimusHelper::findByDQL($dql);
        foreach($data as $k => $val){
            $data[$val['id']]=$val['resolution'];
            unset($data[$k]);
        }
        return $data;
    }


    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $keys=array_keys($criteria);
        if(in_array('config', $keys))
            $conf=$criteria['config'];
        else
            $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        unset($keys);
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            if(isset($criteria['optimusPicId']) && is_numeric($criteria['optimusPicId']) && ($criteria['optimusPicId'] > 0))
            {
                $conf['ADDITIONAL_CONDITION']="v.value LIKE \"%{$criteria['optimusPicId']}%\" AND ( s.url NOT IN (SELECT s.url FROM s WHERE s.url LIKE '%test%') AND cc.variable NOT IN ('') )";
            }
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            if(isset($criteria['optimusPicId']) && is_numeric($criteria['optimusPicId']) && ($criteria['optimusPicId'] > 0))
            {
                $_SERVER['optimusPicId']=$criteria['optimusPicId'];
                $conf['ORDER_BY_KEY_SQL']=" value.id ";
                $conf['JOIN_SQL']="   `item` ON value.item_id = item.id, `category` ON item.category_id = category.id, `category_control` ON (value.category_control_id = category_control.id AND category_control.variable NOT IN ('')), `structure`  ON (item.id = structure.item_id AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE '%test%')), `picture`";
                $placeholder=$criteria['optimusPicId'];
                $conf['ADDITIONAL_CONDITION_SQL']="value.value LIKE '%". $placeholder ."%'";
            }
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(self::getRSM(), $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {

        $findResult=array();
        foreach($results as $item)
        {
            /** @var $item Item  */
            $values=$item->getValue();
            $desc=$item->getTitle();

            foreach($values as $value)
            {
                /** @var $value Value */
                $img=null;
                /** @var $value Value */
                $variable=$value->getCategoryControl()->getVariable();
                if(in_array($variable, self::$image))
                {
                    $pid= $value->getValue();
                    if(!is_numeric($pid) && (isset($_SERVER['optimusPicId']) && is_numeric($_SERVER['optimusPicId']))){
                        $pid=$_SERVER['optimusPicId'];
                    }
                    unset($_SERVER['optimusPicId']);
                    if(!is_numeric($pid)) continue;
                    $img=self::getImage($pid);
                    $img->setDescription($desc);
                } elseif(in_array($variable, self::$gallary))
                {


                    $gallery=$value->getValue();
                    $galleryThumbnail=$value->getCategoryControl()->getGalleryThumbnail()?$value->getCategoryControl()->getGalleryThumbnail():119;
                    $galleryPicture=$value->getCategoryControl()->getGalleryPicture()?$value->getCategoryControl()->getGalleryPicture():119;
                    $img=self::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );


                } else continue;
                $findResult[]=$img;
            }
            unset($value);
        }
        unset($item);
        $findResult=self::checkNullArray($findResult);
        return $findResult;
    }
    protected static function getObjectByValue($results)
    {
        $findResult=array();
        foreach($results as $value)
        {
            $img=null;
            /** @var $value Value */
            $variable=$value->getCategoryControl()->getVariable();
            $desc=$value->getItem()->getTitle();

            $flagImg=in_array($variable, self::$image);
            $flagGallary=in_array($variable, self::$gallary);
            if($flagImg)
            {
              $pid= $value->getValue();
                if(!is_numeric($pid) && (isset($_SERVER['optimusPicId']) && is_numeric($_SERVER['optimusPicId']))){
                    $pid=$_SERVER['optimusPicId'];
                }
                unset($_SERVER['optimusPicId']);
                if(!is_numeric($pid)) continue;
              $img=self::getImage($pid);
              $img->setDescription($desc);
            } elseif($flagGallary)
            {


                $gallery=$value->getValue();
                $galleryThumbnail=$value->getCategoryControl()->getGalleryThumbnail()?$value->getCategoryControl()->getGalleryThumbnail():119;
                $galleryPicture=$value->getCategoryControl()->getGalleryPicture()?$value->getCategoryControl()->getGalleryPicture():119;
                $img=self::getNewGallery($desc, $gallery, $galleryThumbnail, $galleryPicture );


            } else continue;
            $findResult[]=$img;
        }
        unset($value);
        $findResult=self::checkNullArray($findResult);
        return $findResult;
    }

    /**
     *value.id as vId, value.value as vaval,
    item.id AS itId, item.title AS ittl,
    structure.id as sid, structure.url as surl,
    category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,
    picture.id AS pcId, picture.format AS format, picture.url AS ext, picture.title AS label
     *
     * @return ResultSetMapping
     */
    protected static function getRSM()
    {
        $rsm= new ResultSetMapping;
        $rsm->addEntityResult('export\Value', 'v');
        $rsm->addFieldResult('v', 'vId', 'id');
        $rsm->addFieldResult('v', 'vaval', 'value');


        $rsm->addMetaResult('v', 'valccl', 'categoryControl');
        $rsm->addJoinedEntityResult('export\CategoryControl' , 'ccl', 'v', 'categoryControl');
        $rsm->addFieldResult('ccl', 'cclId', 'id');
        $rsm->addFieldResult('ccl', 'cclvr', 'variable');
        $rsm->addFieldResult('ccl', 'cclGllThnl', 'galleryThumbnail');

        $rsm->addMetaResult('v', 'valit', 'item');
        $rsm->addJoinedEntityResult('export\Item' , 'i', 'v', 'item');
        $rsm->addFieldResult('i', 'itId', 'id');
        $rsm->addFieldResult('i', 'ittl', 'title');

        $rsm->addMetaResult('i', 'sId', 'strusture');
        $rsm->addJoinedEntityResult('export\Structure' , 's', 'i', 'structure');
        $rsm->addFieldResult('s', 'sId', 'id');
        $rsm->addFieldResult('s', 'surl', 'url');
/*
        $rsm->addMetaResult('v', 'pvpId', 'picture');
        $rsm->addJoinedEntityResult('export\Picture' , 'p', 'v', 'picture');
        $rsm->addFieldResult('p', 'pcId', 'id');
        $rsm->addFieldResult('p', 'format', 'format');
        $rsm->addFieldResult('p', 'ext', 'url');
        $rsm->addFieldResult('p', 'label', 'title');

        $rsm->addMetaResult('p', 'pcdir', 'folderId');
        $rsm->addJoinedEntityResult('export\Folder' , 'f', 'p', 'folderId');
        $rsm->addFieldResult('f', 'dirId', 'id');
        $rsm->addFieldResult('f', 'dir', 'shortUrl');

        $rsm->addMetaResult('p', 'cclGllThnl', 'size');
        $rsm->addJoinedEntityResult('export\PictureSize' , 'psz', 'p', 'size');
        $rsm->addFieldResult('psz', 'pszId', 'id');
        $rsm->addFieldResult('psz', 'size', 'resolution');
*/
        return $rsm;
    }

    public  static function positionGenerator($wpos)
    {
        switch($wpos)
        {
            case "East":
                $position=3;
                break;
            case "SouthEast":
                $position=11;
                break;
            default:
                $position=11;
                break;
        }
        return $position;
    }
    public  static function mediaTypesGenerator($fm)
    {
        switch($fm)
        {
            case "image/jpeg":
                $position=3;
                break;
            case "image/gif":
                $position=4;
                break;
            case "image/png":
                $position=1;
                break;
            default:
                $position=1;
                break;
        }
        return $position;
    }
    public  static function getImage($id, $sizeId=119)
    {
        $sizeId=$sizeId?$sizeId:119;
        $id=$id?$id:false;
        $img=new ImageFileEntity();
        if($sizeId && $id)
        {
            $dql = "SELECT pic FROM \export\PictureSize pic WHERE pic.id={$sizeId} ";
            $Size = JEntityHelper::getEntityManager()->createQuery($dql)->getResult();
            $dql = "SELECT pic FROM \export\Picture pic WHERE pic.id={$id} ";
            $Pict = JEntityHelper::getEntityManager()->createQuery($dql)->getResult();

            if(count($Size) > 0)
            $Size=$Size[0];
            if(count($Pict) > 0)
            $Pict=$Pict[0];


        } else return array();

        if(isset($Size) && !empty($Size) && ($Size instanceof PictureSize) && isset($Pict) && !empty($Pict) && ($Pict instanceof Picture))
        {

            /** @var $Pict  Picture */
            $pid=$Pict->getId();
            $fm=$Pict->getFormat();
            $ext=$Pict->getUrl();
            $ttl=$Pict->getTitle();
            $fldId=$Pict->getFolderId();


            /** @var $Size  PictureSize */
            $sid=$Size->getId();
            $he=$Size->getHeight();
            $wi=$Size->getWidth();
            $re=$Size->getResolution();
            $wpos=$Size->getWatermarkPosition();

            if($fldId){
                $dql = "SELECT pic FROM \export\Folder pic WHERE pic.id={$fldId} ";
                /** @var $Folder \export\Folder */
                $fld = JEntityHelper::getEntityManager()->createQuery($dql)->getResult();
                $Folder=$fld[0];
                $uri=$Folder->getShortUrl();
                $uri=$uri.$pid.$ext;
            } else    $uri="/images/sizes/{$re}/{$pid}{$ext}";



            $img->setOptimusPicId($pid);
            $img->setOptimusSizeId($sid);
            $img->setHeight($he);
            $img->setWidth($wi);
            $img->setOptimusSize($re);
            $img->setWaterMarkPosition(self::positionGenerator($wpos));
            $img->setFormat(self::mediaTypesGenerator($fm));
            $img->setFileNameExtensions($ext);
            $img->setLabel($ttl);
            $img->setUri($uri);
           $nullArray=array($pid, $sid, $he, $wi, $re, $wpos, $fm, $ext, $ttl, $uri);

            if(!self::checkNull($nullArray))       return array();
        } else  return array();
        return $img;
    }
    public   static function getNewGallery($desc, $gallery, $galleryThunbnail, $galleryPicture ){
        $gallery=explode(",", $gallery);
        $gall=new ArrayCollection;
        foreach($gallery as $gal)
        {

            $thumb=sysFileImageAdapterEntity::getImage($gal,$galleryThunbnail);

            if(isset($thumb) && !empty($thumb) && ($thumb instanceof ImageFileEntity)) {
                /** @var $thumb ImageFileEntity */
                $thumb->setDescription($desc);
                $picture=sysFileImageAdapterEntity::getImage($gal,$galleryPicture);
                /** @var $picture ImageFileEntity */
                $picture->setDescription($desc);

                $img=array($thumb, $picture);

                $gall->add($img);
            }
            unset($thumb);

        }

        return $gall;
    }
      private  static function checkNull(array $check)
      {

              $res=false;
              foreach($check as $null)
              {

                  $fl1=($null === "")?true:false;
                  $fl2=($null === null)?true:false;
                  $fl3=((is_array($null) && (count($null) == 0)))?true:false;
                  if($fl1 || $fl2 || $fl3) $res=$res || false;
                  else $res=$res || true;
              }



          return $res;
      }
      private   static function checkNullArray($check)
      {
          $flag=false;
          foreach($check as $checkedItem)
          {
              if(($checkedItem instanceof ImageFileEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                  $flag=true;
          }
          $res=$flag?$check:array();
          return $res;
      }
    /******************************************************************************************************************
     * Override methods / Перекрытые свойства
     ******************************************************************************************************************/



}
