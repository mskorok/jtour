<?php
namespace entities\media;

use entities\_abstract\sysSimpleAbstract;
use application\types\media\MediaFileTypes;
use DateTime;

/**
 * Медиа библиотека
 *
 * @author Наумова Елена
 *
 * @modified Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @date: 21.01.13 - 17:51
 *
 *
 */
class sysLibraryAdapterEntity extends LibraryEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}