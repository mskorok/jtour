<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 24.04.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 *
 * @Entity
 *
 */

namespace application\models\entities\weddings;


use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;
use application\models\entities\media\sysFileImageAdapterEntity as sysFileImageAdapterEntity;

class WeddingsPackagesAdapterEntity    extends sysAbstract {
    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description здесь записывается всякий html с классами для декорации
     * @Column
     * @var
     */
    protected $headerDecoration;
    /**
     * @description а здесь теги закрытия
     * @Column
     * @var
     */
    protected $footerDecoration;
    /**
     * @description  ссылка на рисунок
     * @Column
     * @var
     */
    protected $img;
    /**
     * @description галерея
     * @Column
     * @var
     */
    protected  $gallery;
    /**
     * @description  стиль ссылки
     * @Column
     * @var
     */
    protected $title;

    /**
     * @description текст ссылки
     * @Column
     * @var
     */
    protected  $text;
    /**
     * @description урл страницы на которой находится виджет
     * @Column
     * @var
     */
    protected  $ownerUrl;
    /**
     *
     * @description значение переменной variable для отдельных изображений
     *
     * @var
     */
    private  static $images=array("pic4list", "foto", "pic1", "rivers_map", "maps", "deck1_foto",  "deck2_foto",
        "deck3_foto", "deck4_foto", "deck5_foto", "deck6_foto", "deck7_foto", "deck8_foto", "deck9_foto", "map_right",
        "dop_pic", "hotel_facilities", "cruisesmap");
    /**
     *
     * @description  значение переменной variable для галерей
     *
     *
     * @var
     */
    private  static $galleries=array( "gallery", "gallery_preview", "flash_gallery_all", "fotoalbom", "flash_gallery",
        "promo_gallery", "photos", "fishing_gallery", "gallery_preview_winter", "gallery_winter", "preview_photos",
        "new_year_gallery", "beauty_photos", "active_1_photos", "active_2_photos", "active_3_photos", "active_4_photos",
        "active_5_photos", "active_6_photos", "active_8_photos", "active_9_photos", "child_1_photo", "child_2_photo",
        "child_3_photo", "child_4_photo", "child_5_photo", "child_6_photo", "south_north_photos", "north_south_photos",
        "booklet_photos_1", "booklet_photos_2", "snowman_photos_1", "snowman_photos_2",  "snowboard_photos_1",
        "snowboard_photos_2", "snowboard_photos_3", "snowboard_photos_4", "snowboard_photos_5", "skiing_photos_1",
        "skiing_photos_2	", "skiing_photos_2", "skiing_photos_3", "driving_skiing_photos", "active_photos",
        "htg_photos_1", "conference_photo1", "gallery_vertical", "additional1_gallery", "additional2_gallery",
        "restruns_gallery", "flash", "infro_gallery");

    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *
     *  $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *  !!!ATTENTION!!!!! $criteria = array("ownerUrl" => array("value" => "'URL'", "condition" => "LIKE"));       !!!ATTENTION!!!!!
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array(
        'headerDecoration'=> '',
        'footerDecoration'=> '',
        'title'=> 'item.title',
        'ownerUrl'=> 'structure.url',
        'img'=>'',
        'gallery'=> '',
        'text'=> ''

    );
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 5,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => "  ",
        'RESULT_COLUMN_SQL'         => " value.id as vId, value.value as vaval,
                                         item.id AS itId, item.title AS ittl,
                                         structure.id as sId, structure.url as surl,
                                         category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl


                                         ",
        'JOIN'                      => "",
        'JOIN_SQL'                  => "   `link` ON (link.category_control_id IN  (SELECT id FROM `category_control` WHERE category_control.variable='packages')),  `structure` ON link.item_id=structure.item_id, `item` ON (link.linked_item_id=item.id), `category_control` ON category_control.id=value.category_control_id ",
        'FROM'                      => "",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "",
        'ORDER_BY_KEY_SQL'          => "structure.url",
        'ORDER_BY_VALUE'            => "ASC",
        'ORDER_BY_VALUE_SQL'        => "ASC",
        'ADDITIONAL_CONDITION'      => "",
        'ADDITIONAL_CONDITION_SQL'  => "item.id=value.item_id ",
        'OFFSET'                    => null


    );
    /******************************************************************************************************************
     *  SQL / Запрос
     ******************************************************************************************************************/
    /**
     *SELECT item.title, value.value,  structure.url, category_control.variable
    FROM value
    JOIN link ON (link.category_control_id IN
    (SELECT id FROM category_control WHERE category_control.variable="packages"))
    JOIN structure ON (link.item_id=structure.item_id )
    JOIN item ON (link.linked_item_id=item.id)
    JOIN category_control ON category_control.id=value.category_control_id
    WHERE item.id=value.item_id AND structure.url LIKE "%cuba%"
     *
     *
   SELECT value.*, structure.url, item.title, link.category_control_id, category_control.id, category_control.variable FROM  value, item, structure, category_control, link WHERE link.category_control_id IN
  (SELECT id FROM category_control WHERE category_control.variable="packages")
 AND link.item_id=structure.item_id AND structure.url LIKE "%cuba%" AND link.linked_item_id=item.id AND item.id=value.item_id
    AND category_control.id=value.category_control_id ORDER BY value.value IN (SELECT value.value FROM value, category_control WHERE category_control.variable="priority") ASC
     *
     */
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $keys=array_keys($criteria);
        if(in_array('config', $keys))
            $conf=$criteria['config'];
        else
            $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        unset($keys);
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(self::getRSM(), $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {


        return array();
    }
    protected static function getObjectByValue($results)
    {
        $article=new WeddingsPackagesAdapterEntity;
        $findResult = array();
        $itemId=null;
        foreach($results as $key => $value)
        {
            /** @var $item Item  */
            /** @var $value Value */
            $item=$value->getItem();
            $desc=$item->getTitle();
            $iId=$item->getId();
            if($itemId != $iId)
            {
                if($key != 0)
                {
                    $findResult[]=$article;
                    $article=new WeddingsPackagesAdapterEntity;

                }
                $itemId=($itemId != $iId)?$iId:$itemId;
                // сборка сущности
                $str=$item->getStructure();
                /** @var $struct Structure */
                $struct=$str[0];
                $url=$struct->getUrl();
                $article->setOwnerUrl($url);


            }

            $catCtl=$value->getCategoryControl();

            $variable=$catCtl->getVariable();

            switch($variable)
            {
                case "short_text":
                    $header=$value->getValue();
                    $article->setHeaderDecoration($header);
                    break;
                case "default_text":
                    $description=$value->getValue();
                    $article->setText($description);
                    break;

                case "pic4list":
                    $id=$value->getValue();
                    $img=sysFileImageAdapterEntity::getImage($id);

                    $article->setImg($img->getUri());
                    break;

            }
            $findResult[]=$article;
        }
        unset($value, $catCtl, $key, $variable);
        $findResult=self::checkNullArray($findResult);
        return $findResult;
    }

    /**
     *value.id as vId, value.value as vaval,
    item.id AS itId, item.title AS ittl,
    structure.id as sid, structure.url as surl,
    category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,
    picture.id AS pcId, picture.format AS format, picture.url AS ext, picture.title AS label
     *
     * @return ResultSetMapping
     */
    protected static function getRSM()
    {
        $rsm= new ResultSetMapping;
        $rsm->addEntityResult('export\Value', 'v');
        $rsm->addFieldResult('v', 'vId', 'id');
        $rsm->addFieldResult('v', 'vaval', 'value');


        $rsm->addMetaResult('v', 'valccl', 'categoryControl');
        $rsm->addJoinedEntityResult('export\CategoryControl' , 'ccl', 'v', 'categoryControl');
        $rsm->addFieldResult('ccl', 'cclId', 'id');
        $rsm->addFieldResult('ccl', 'cclvr', 'variable');
        $rsm->addFieldResult('ccl', 'cclGllThnl', 'galleryThumbnail');

        $rsm->addMetaResult('v', 'valit', 'item');
        $rsm->addJoinedEntityResult('export\Item' , 'i', 'v', 'item');
        $rsm->addFieldResult('i', 'itId', 'id');
        $rsm->addFieldResult('i', 'ittl', 'title');

        $rsm->addMetaResult('i', 'sId', 'strusture');
        $rsm->addJoinedEntityResult('export\Structure' , 's', 'i', 'structure');
        $rsm->addFieldResult('s', 'sId', 'id');
        $rsm->addFieldResult('s', 'surl', 'url');
        /*
                $rsm->addMetaResult('v', 'pvpId', 'picture');
                $rsm->addJoinedEntityResult('export\Picture' , 'p', 'v', 'picture');
                $rsm->addFieldResult('p', 'pcId', 'id');
                $rsm->addFieldResult('p', 'format', 'format');
                $rsm->addFieldResult('p', 'ext', 'url');
                $rsm->addFieldResult('p', 'label', 'title');

                $rsm->addMetaResult('p', 'pcdir', 'folderId');
                $rsm->addJoinedEntityResult('export\Folder' , 'f', 'p', 'folderId');
                $rsm->addFieldResult('f', 'dirId', 'id');
                $rsm->addFieldResult('f', 'dir', 'shortUrl');

                $rsm->addMetaResult('p', 'cclGllThnl', 'size');
                $rsm->addJoinedEntityResult('export\PictureSize' , 'psz', 'p', 'size');
                $rsm->addFieldResult('psz', 'pszId', 'id');
                $rsm->addFieldResult('psz', 'size', 'resolution');
        */
        return $rsm;
    }

    public  static function positionGenerator($wpos)
    {
        switch($wpos)
        {
            case "East":
                $position=3;
                break;
            case "SouthEast":
                $position=11;
                break;
            default:
                $position=11;
                break;
        }
        return $position;
    }
    public  static function mediaTypesGenerator($fm)
    {
        switch($fm)
        {
            case "image/jpeg":
                $position=3;
                break;
            case "image/gif":
                $position=4;
                break;
            case "image/png":
                $position=1;
                break;
            default:
                $position=1;
                break;
        }
        return $position;
    }

    private  static function checkNull(array $check)
    {

        $res=false;
        foreach($check as $null)
        {

            $fl1=($null === "")?true:false;
            $fl2=($null === null)?true:false;
            $fl3=((is_array($null) && (count($null) == 0)))?true:false;
            if($fl1 || $fl2 || $fl3) $res=$res || false;
            else $res=$res || true;
        }



        return $res;
    }
    private   static function checkNullArray($check)
    {
        $flag=false;
        foreach($check as $checkedItem)
        {
            if(($checkedItem instanceof WeddingsPackagesAdapterEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                $flag=true;
        }
        $res=$flag?$check:array();
        return $res;
    }



    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $footerDecoration
     */
    public function setFooterDecoration($footerDecoration)
    {
        $this->footerDecoration = $footerDecoration;
    }

    /**
     * @return
     */
    public function getFooterDecoration()
    {
        return $this->footerDecoration;
    }

    /**
     * @param  $headerDecoration
     */
    public function setHeaderDecoration($headerDecoration)
    {
        $this->headerDecoration = $headerDecoration;
    }

    /**
     * @return
     */
    public function getHeaderDecoration()
    {
        return $this->headerDecoration;
    }

    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $ownerUrl
     */
    public function setOwnerUrl($ownerUrl)
    {
        $this->ownerUrl = $ownerUrl;
    }

    /**
     * @return
     */
    public function getOwnerUrl()
    {
        return $this->ownerUrl;
    }

    /**
     * @param  $gallery
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param  $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param  $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }



}