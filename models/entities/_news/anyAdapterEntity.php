<?php
namespace application\models\entities\_news;
use entities\_news\anyEntity;
use entities\_abstract\sysGeneralAbstract;
use entities\user\UserEntity as userSysEntity;
use \JArrayCollection as ArrayCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\CategoryControl;
use export\Link;

/**
 * @Entity
 */

class anyAdapterEntity extends anyEntity
{

    const USE_ADAPTER = true;
    const USE_DQL= false;
    const BY_ITEM= false;
    const QUERY_LIMIT = 6;
    const RESULT_COLUMN_DQL=" v, i, c, s, cc, t ";
    const RESULT_COLUMN_SQL=" value.id as vid, value.value as vaval, value.item_id as valit, value.version_id as valver, value.category_control_id as valccl,
            item.id as itid, item.title as ittl,
            structure.id as sid, structure.url as surl,
            category_control.variable as cclvr, category_control.id as cclid ";
    const JOIN = " i.value  v , i.category c , i.structure s ,  s.template t ,  v.categoryControl cc";
    /** by Item */
    //const JOIN_SQL = " `value` ON item.id=value.item_id, `category` ON item.category_id = category.id,   `category_control` ON category_control.id = value.category_control_id,   `structure`  ON structure.item_id = item.id";
    /** by Value      */
    const JOIN_SQL = "  `item` ON item.id=value.item_id, `category_control` ON category_control.id = value.category_control_id, `structure`  ON structure.item_id = item.id, `category` ON item.category_id = category.id ";
    const FROM = "  \export\Item i  ";
    /** by Item */
    //const FROM_SQL = "  `item`   ";
    /** by Value      */
    const FROM_SQL = "  `value`   ";
    const ORDER_BY_KEY="i.id";
    /** by Item */
    //const ORDER_BY_KEY_SQL=" item.id ";
    /** by Value      */
    const ORDER_BY_KEY_SQL=" item.id ";
    const ORDER_BY_VALUE="DESC";
    const ORDER_BY_VALUE_SQL="DESC";
    const ADDITIONAL_CONDITION = " c.id =610  " ;
    const ADDITIONAL_CONDITION_SQL = " category.id =610  " ;



    protected  static $responding=array(
        "url"=>"structure.url",
        "id" => "item.id"
    );

    /*
     *
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     */





    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){

        if(static::USE_DQL){
            $orderBy=(static::ORDER_BY_KEY )?array(static::ORDER_BY_KEY => static::ORDER_BY_VALUE):array();
            $results=self::getOptimus($criteria,  $orderBy, $limit, $offset);
        } else
        {
            $orderBy=(static::ORDER_BY_KEY_SQL )?array(static::ORDER_BY_KEY_SQL => static::ORDER_BY_VALUE_SQL):array();
            $results=self::getNativeSqlOptimus($criteria,  $orderBy, $limit, $offset);
        }
        $findResult=(static::BY_ITEM)?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected  static function _aFindAll(){
        $limit=static::QUERY_LIMIT;
        $criteria= array();

        return self::_aFindBy( $criteria,  $orderBy = null, $limit) ;
    }

    protected  static function _aFind($id){

        $criteria = array('id' => $id);

        return self::_aFindBy( $criteria,  $orderBy = null, $limit = 1) ;
    }



    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return self::_aFindBy( $criteria,  $orderBy , $limit = 1) ;
    }
    public static function getOptimus(array $criteria, array $orderBy , $limit , $offset = null)
    {
        $responding=self::$responding;
        $resultColumnsDql=static::RESULT_COLUMN_DQL;
        $additionalCondition=static::ADDITIONAL_CONDITION;
        $queryLine="";
        $order="";
        $and="";
        /*
         *
         * $criteria = array("property" => "value");
         * $responding =array("property" => "column")
         * $queryLine= array("column" => "value")
         * $resultColumns = array("property" => "column")
         * $criteria= array("property1" => "value1", "property1" => "value1");
         * $responding =array("property1" => "column1", "property2" => "column2")
         *
         *
         */
        foreach($criteria as $property => $value)
        {
            $keys=array_keys($responding);
            if(in_array($property, $keys ))
            {
                if($queryLine == "")
                    $queryLine .= " {$responding[$property]}={$value} ";
                else
                    $queryLine .= " AND {$responding[$property]}={$value} ";
            }
        }
        unset($property, $value);
        if($orderBy){
            $key=array_keys($orderBy);
            $value=array_values($orderBy);
            $order=" ORDER BY {$key[0]}  {$value[0]}  ";
            unset($key, $value);
        }

        if((isset($additionalCondition) && !empty($additionalCondition)) && (isset($queryLine) && !empty($queryLine)))    $and=" AND ";



        $jn=explode(", ", static::JOIN);
        $ja=array();
        foreach($jn as $j)
        {
            $ja[]=" JOIN ".$j;
        }
        $join=implode(" ", $ja);
        $from= static::FROM;
        $where= (((isset($additionalCondition) && !empty($additionalCondition))) || ((isset($queryLine) && !empty($queryLine))))? "WHERE" : "";
        $dql="SELECT {$resultColumnsDql} FROM {$from}  {$join}
        {$where}   {$additionalCondition} {$and} {$queryLine} {$order} ";



        $data= JEntityHelper::getEntityManager()->createQuery($dql)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();

        return $data;
    }
    public static function getNativeSqlOptimus(array $criteria, array $orderBy , $limit , $offset = null)
    {
        $responding=self::$responding;
        $resultColumnsSql=static::RESULT_COLUMN_SQL;
        $additionalCondition=static::ADDITIONAL_CONDITION_SQL;
        $queryLine="";
        $limitLine=static::QUERY_LIMIT;
        $limitLine=" LIMIT ".$limitLine;
        $order="";
        $and="";
        /*
         *
         * $criteria = array("property" => "value");
         * $responding =array("property" => "column")
         * $queryLine= array("column" => "value")
         * $resultColumns = array("property" => "column")
         * $criteria= array("property1" => "value1", "property1" => "value1");
         * $responding =array("property1" => "column1", "property2" => "column2")
         *
         *
         */
        foreach($criteria as $property => $value)
        {
            $keys=array_keys($responding);
            if(in_array($property, $keys ))
            {
                if($queryLine == ""){
                    if(!is_array($value))
                        $queryLine .= " {$responding[$property]}={$value} ";
                    else
                        $queryLine .= " {$responding[$property]} {$value['condition']} {$value['value']} ";
                } else {
                    if(!is_array($value))
                        $queryLine .= "  AND   {$responding[$property]}={$value} ";
                    else
                        $queryLine .= "  AND   {$responding[$property]} {$value['condition']} {$value['value']} ";
                }

            }
        }
        unset($property, $value);
        if($orderBy){
            $key=array_keys($orderBy);
            $value=array_values($orderBy);
            $order=" ORDER BY {$key[0]}  {$value[0]}  ";
            unset($key, $value);
        }

        if((isset($additionalCondition) && !empty($additionalCondition)) && (isset($queryLine) && !empty($queryLine)))    $and=" AND ";



        $jn=explode(", ", static::JOIN_SQL);
        $ja=array();
        foreach($jn as $j)
        {
            $ja[]=" JOIN ".$j;
        }
        $join=implode(" ", $ja);
        $from= static::FROM_SQL;
        $where= (((isset($additionalCondition) && !empty($additionalCondition))) || ((isset($queryLine) && !empty($queryLine))))? "WHERE" : "";

        /**
         *
         *
         *
         * @description вывод с головным элементом value
         *
         *
         *
         *
         * $sql="SELECT
         * value.id as vid, value.value as vaval, value.item_id as valit, value.version_id as valver, value.category_control_id as valccl,
         * item.id as itid, item.title as ittl,
         * structure.id as sid, structure.url as surl,
         * category_control.variable as cclvr, category_control.id as cclid
         * FROM `value`
         * JOIN `item` ON item.id=value.item_id JOIN `category` ON item.category_id = category.id
         * JOIN `category_control` ON category_control.id = value.category_control_id
         * JOIN `structure`  ON structure.item_id = item.id
         * WHERE category.id =610
         * ORDER BY value.id DESC   LIMIT 5";
         *
         * @description вывод с головным элементом item
         *
         *
         *
         *
         *
         * $sql="SELECT  value.id as vid, value.value as vaval, value.item_id as valit, value.version_id as valver, value.category_control_id as valccl,
         *  item.id as itid, item.title as ittl,
         *  structure.id as sid, structure.url as surl,
         *  category_control.variable as cclvr, category_control.id as cclid
         *  FROM `item`
         *  JOIN `value` ON item.id=value.item_id
         *  JOIN `category` ON item.category_id = category.id
         *  JOIN `category_control` ON category_control.id = value.category_control_id
         *  JOIN `structure`  ON structure.item_id = item.id
         *  WHERE category.id =610
         *  ORDER BY item.id DESC
         *  LIMIT 5";
         *
         *
         *
         */

        $sql="SELECT
            {$resultColumnsSql}
        FROM  {$from} {$join}
        {$where}   {$additionalCondition} {$and} {$queryLine} {$order} {$limitLine} ";

        if(static::BY_ITEM){
            $rsm= new ResultSetMapping;
            $rsm->addEntityResult('export\Item', 'i');
            $rsm->addFieldResult('i', 'itid', 'id');
            $rsm->addFieldResult('i', 'ittl', 'title');
            $rsm->addMetaResult('i', 'sid', 'strusture');
            $rsm->addJoinedEntityResult('export\Structure' , 's', 'i', 'structure');
            $rsm->addFieldResult('s', 'sid', 'id');
            $rsm->addFieldResult('s', 'surl', 'url');

            $rsm->addMetaResult('i', 'vid', 'value');
            $rsm->addJoinedEntityResult('export\Value', 'v', 'i', 'value');
            $rsm->addFieldResult('v', 'vid', 'id');
            $rsm->addFieldResult('v', 'vaval', 'value');
            $rsm->addFieldResult('v', 'valver', 'versionId');

            $rsm->addMetaResult('v', 'valccl', 'categoryControl');
            $rsm->addJoinedEntityResult('export\CategoryControl' , 'ccl', 'v', 'categoryControl');
            $rsm->addFieldResult('ccl', 'cclid', 'id');
            $rsm->addFieldResult('ccl', 'cclvr', 'variable');
        } else {

            $rsm= new ResultSetMapping;
            $rsm->addEntityResult('export\Value', 'v');
            $rsm->addFieldResult('v', 'vid', 'id');
            $rsm->addFieldResult('v', 'vaval', 'value');
            $rsm->addFieldResult('v', 'valver', 'versionId');

            $rsm->addMetaResult('v', 'valccl', 'categoryControl');
            $rsm->addJoinedEntityResult('export\CategoryControl' , 'ccl', 'v', 'categoryControl');
            $rsm->addFieldResult('ccl', 'cclid', 'id');
            $rsm->addFieldResult('ccl', 'cclvr', 'variable');

            $rsm->addMetaResult('v', 'valit', 'item');
            $rsm->addJoinedEntityResult('export\Item' , 'i', 'v', 'item');
            $rsm->addFieldResult('i', 'itid', 'id');
            $rsm->addFieldResult('i', 'ittl', 'title');

            $rsm->addMetaResult('i', 'sid', 'strusture');
            $rsm->addJoinedEntityResult('export\Structure' , 's', 'i', 'structure');
            $rsm->addFieldResult('s', 'sid', 'id');
            $rsm->addFieldResult('s', 'surl', 'url');



        }





        $query = JEntityHelper::getEntityManager()->createNativeQuery($sql, $rsm);
        $data=$query->getResult();
        return $data;
    }
    protected static function getObjectByItem($results)
    {

        $findResult = array();
        $author=new userSysEntity;
        foreach($results as $item)
        {
            /** @var $item Item  */
            $values=$item->getValue();
            if($values){

                $news=new anyEntity;
                $creationData='';
                $preview=$item->getTitle();
                $picture='';
                $textNews='';
                $extUrl='';
                /** @var $struct Structure */
                $struc=$item->getStructure();
                $struct=$struc[0];
                $extUrl=$struct->getUrl();

                foreach($values as $value)
                {
                    /** @var $value Value */
                    /** @var $catcon CategoryControl */
                    $catcon=$value->getCategoryControl();
                    $variable=$catcon->getVariable();
                    switch($variable)
                    {
                        case "text":
                            $textNews=$value->getValue();
                            break;
                        case "date":
                            $creationData=$value->getValue();
                            break;
                        case "pic1":
                            $picture=$value->getValue();
                            break;
                        case "announce":
                            $preview=$value->getValue();
                            break;
                    }


                }
                unset($value, $catcon, $variable);
                if((($news->getCreationDate()) || ($news->getTextNews()) || ($news->getPreview()) || ($news->getExtUrl())) && !(($news->getCreationDate() =='') && ($news->getTextNews() =='') && ($news->getPreview() =='') && ($news->getExtUrl() ==''))){
                    $news->setAuthor($author);
                    $news->setCreationDate($creationData);
                    $news->setExtSource(true);
                    $news->setExtText($textNews);
                    $news->setPreview($preview);
                    $news->setExtUrl($extUrl);
                    $news->setTextNews($textNews);

                    $findResult[] =$news;
                }
            } else continue;

        }
        return $findResult;
    }
    protected static function getObjectByValue($results)
    {
        $notEmpty=null;
        $findResult = array();
        $author=new userSysEntity;
        $itemId=null;
        $news=new anyEntity;
        foreach($results as $key => $value)
        {
            /** @var $item Item  */
            /** @var $value Value */
            $item=$value->getItem();
            $iId=$item->getId();
            if($itemId != $iId)
            {
                if($key != 0)
                {
                    if($notEmpty)
                    $findResult[]=$news;
                    $notEmpty=null;
                    $news=new anyEntity;

                }
                $itemId=($itemId != $iId)?$iId:$itemId;
                // сборка сущности
                $creationData='';
                $preview=$item->getTitle();
                $picture='';
                $textNews='';
                $extUrl='';
                $struc=$item->getStructure();
                /** @var $struct Structure */
                $struct=$struc[0];
                $extUrl=$struct->getUrl();
                $news->setAuthor($author);
                $news->setExtSource(true);
                $news->setExtText($textNews);
                $news->setPreview($preview);
                $news->setExtUrl($extUrl);
                $news->setTextNews($textNews);

            }

            $catcon=$value->getCategoryControl();
            $variable=$catcon->getVariable();

            switch($variable)
            {
                case "text":
                    $textNews=$value->getValue();
                    $news->setExtText($textNews);
                    $news->setTextNews($textNews);
                    break;
                case "date":
                    $creationData=$value->getValue();
                    $news->setCreationDate($creationData);
                    break;
                case "announce":
                    $preview=$value->getValue();
                    $news->setPreview($preview);
                    break;
                case "pic1":
                    $picture=$value->getValue();
                    break;
            }
            if((($news->getCreationDate()) || ($news->getTextNews()) || ($news->getPreview()) || ($news->getExtUrl())) && !(($news->getCreationDate() =='') && ($news->getTextNews() =='') && ($news->getPreview() =='') && ($news->getExtUrl() ==''))) $notEmpty=true;
        }
        unset($value, $catcon, $key, $variable);
        if($notEmpty)
        $findResult[]=$news;
        return $findResult;

    }
}
