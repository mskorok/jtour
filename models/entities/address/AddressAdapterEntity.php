<?php
namespace entities\address;
use entities\_abstract\sysAbstract;

/**
 *
 *
 * Класс для хранения адресов
 *
 *
 * @date: 24.01.13 - 19:49
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 */
class AddressAdapterEntity extends AddressEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
