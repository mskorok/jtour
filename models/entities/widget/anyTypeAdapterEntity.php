<?php
namespace entities\widget;
use components\JArrayCollection as ArrayCollection;
use entities\_abstract\sysSimpleAbstract as sysSimpleAbstract;
use entities\widget\anyEntity as widgetAnyEntity;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.01.13
 * Time: 13:39
 * To change this template use File | Settings | File Templates.
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 08.11.12
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 * Данный класс используется для типизации виджетов разных типов, именно из этого списка пользователь будет выбирать
 * какой именно тип виджета он хочет добавить. В свойстве type хранится имя класса сущности которым обрабатываются все
 * виджеты данного типа. Именно по этому своству в коде можно будет читать и отпределять что за виджет у нас в руках
 * и какой тип виджета создавать.
 *
 * Вообще у данного класса сущносте довлльно большой спектр применения.
 * P.S. Я так думаю :)
 *
 *
 */
class anyTypeAdapterEntity extends anyTypeEntity
{


    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
