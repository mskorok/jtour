<?php
namespace application\models\entities\geo;

use entities\geo\RegionEntity;
use entities\common\routeMenuItemEntity;
use helpers\JEntityHelper;
 /**
  *
  */
class anyRegionAdapterEntity extends RegionEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

    protected static function _aFindByDQL($dql){
        $result = array();
        $data = \application\components\JOptimusHelper::findByDQL($dql);
        // Поиск регионов
        $var = null;
        $link = array();
        for($i=0;$i<count($data);$i++){
            /** @var $data[] \export\StructureLink */
            $structID = $data[$i]->getStructure()->getId();
            $dql = "SELECT s FROM \export\Structure s WHERE s.id={$structID}";
            $sLink = JEntityHelper::getEntityManager()->createQuery($dql)->getArrayResult();
            if($data[$i+1] instanceof \export\CategoryControl){
                /** @var $data[] \export\CategoryControl */
                $var = $data[++$i]->getVariable();
                $link[$var] = array();
            }
            array_push($link[$var],$sLink[0]);
        }
        if(isset($link['regions'])){
            foreach($link['regions'] as $index => $item){
                $menu = new routeMenuItemEntity();
                $menu->setHref($item['url']);
                $menu->setTitle($item['title']);
                array_push($link['regions'],$menu);
                unset($link['regions'][$index]);
            }
        }
        return $link;
    }

}