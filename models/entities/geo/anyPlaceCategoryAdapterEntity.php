<?php
namespace entities\geo;
use components\JArrayCollection as ArrayCollection;
use entities\_abstract\sysSimpleAbstract;

 /**
  * Категории доступных гео мест
  *
  */
class anyPlaceCategoryAdapterEntity extends anyPlaceCategoryEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}