<?php
namespace entities\geo;
use entities\sys\sysTemplateEntity;
use entities\_abstract\sysSimpleAbstract;

 /**
  *
  * @description Сущность anyTypeEntity описывает типы GEO-объектов.
  *              Содержит свойства: city-город, region-регион, country-страна, marketingRegion-маркетинговый регион.
  */
class anyTypeAdapterEntity extends anyTypeEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}