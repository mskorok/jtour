<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 29.04.13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 *
 * @Entity
 *
 *
 */

namespace application\models\entities\right;
use entities\_abstract\sysAbstract;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Query as Query;
use helpers\JEntityHelper as JEntityHelper;
use application\components\JOptimusHelper;
use \JActiveRecord as JActiveRecord;
use export\StructureLink;
use export\Value;
use export\Item;
use export\Category;
use export\Structure;
use export\Picture as Picture;
use export\PictureSize as PictureSize;
use export\CategoryControl;
use export\Link;
use \export\Folder as Folder;
use application\models\entities\media\sysFileImageAdapterEntity as sysFileImageAdapterEntity;


class MapAdapterEntity extends sysAbstract {
//TODO СДЕЛАТЬ АДАПТЕР

    /******************************************************************************************************************
     * БЛОК КОНСТАНТ АДАПТЕРА
     ******************************************************************************************************************/
    /**
     * Константа, определяющая из какой базы будет производится выборка
     */
    const USE_ADAPTER = true;
    /**
     *
     * @description лимит для коррекции выборки из оптимуса
     */
    const QUERY_LIMIT = 4;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/

    /**
     * @description урл по которому статья располагается        structure.url
     * @Column
     * @var
     */
    protected $url;


    /**
     * @description картинка к статье      ссылка на сущность с изображениями, по которой получаем url value.variable='pic4list'
     * @Column
     * @var
     */
    protected $img;



    /******************************************************************************************************************
     * Adapter Property / Свойства конфигурации адаптера
     ******************************************************************************************************************/
    /**
     *
     *   $criteria  может иметь два варианта
     * $criteria = array("property" => array("value" => "value", "condition" => "condition"));
     * $criteria = array("property" => "value");
     * $responding =array("property" => "column")
     * $queryLine= array("column" => "value")
     * $resultColumns = array("property" => "column")
     * $criteria= array("property1" => "value1", "property1" => "value1");
     * $responding =array("property1" => "column1", "property2" => "column2")
     *
     *
     *
     *
     * кроме того в $criteria['config'] может передоваться кастомный конфигуратор
     *
     *   @description  Конфигуратор соответствия столбцов в Оптимусе и свойств сущности
     *
     * @var array
     */
    public   static $responding=array(
        'url'=> ''
    );
    /**
     *  @description  Конфигуратор выборки из Оптимуса
     * @var array
     */
    public   static $conf=array(
        'QUERY_LIMIT'               => 6,
        'USE_DQL'                   => false,
        'BY_ITEM'                   => false,
        'RESULT_COLUMN_DQL'         => "",
        'RESULT_COLUMN_SQL'         => " value.id as vId, value.value as vaval,
                                        item.id AS itId, item.title AS ittl,
                                        structure.id as sId, structure.url as surl,

                                        category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl ",
        'JOIN'                      => "",
        'JOIN_SQL'                  => "  `item` ON item.id=value.item_id, `category_control` ON category_control.id = value.category_control_id, `structure` ON (item.id = structure.item_id  AND  structure.url NOT IN (SELECT structure.url FROM `structure` WHERE structure.url LIKE \"%test%\"))  ",
        'FROM'                      => "",
        'FROM_SQL'                  => "  `value`   ",
        'ORDER_BY_KEY'              => "",
        'ORDER_BY_KEY_SQL'          => "",
        'ORDER_BY_VALUE'            => "DESC",
        'ORDER_BY_VALUE_SQL'        => "DESC",
        'ADDITIONAL_CONDITION'      => "",
        'ADDITIONAL_CONDITION_SQL'  => "category_control.id IN  (13723, 14318 )",
        'OFFSET'                    => null


    );
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();


    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    protected static function _aFindBy( array $criteria, array $orderBy = null, $limit  , $offset = null){
        $responding=self::$responding?self::$responding:JOptimusHelper::$responding;
        $keys=array_keys($criteria);
        if(in_array('config', $keys))
            $conf=$criteria['config'];
        else
            $conf=self::$conf?self::$conf:JOptimusHelper::$conf;
        $conf['QUERY_LIMIT']=$limit?$limit:$conf['QUERY_LIMIT'];
        $conf['OFFSET']=$offset?$offset:$conf['OFFSET'];

        if($conf['USE_DQL']){
            $conf['ORDER_BY_KEY']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY'];
            $conf['ORDER_BY_VALUE']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE'];
            $results=JOptimusHelper::getOptimus($criteria, $conf,  $responding);
        } else
        {
            $conf['ORDER_BY_KEY_SQL']=$orderBy?array_keys($orderBy):$conf['ORDER_BY_KEY_SQL'];
            $conf['ORDER_BY_VALUE_SQL']=$orderBy?array_values($orderBy):$conf['ORDER_BY_VALUE_SQL'];
            $results=JOptimusHelper::getNativeSqlOptimus(self::getRSM(), $criteria, $conf,  $responding);
        }
        $findResult=($conf['BY_ITEM'])?self::getObjectByItem($results):static::getObjectByValue($results);

        return $findResult;
    }
    protected static function getObjectByItem($results)
    {

        return array();
    }

    protected static function getObjectByValue($results)
    {
        $map=new MapAdapterEntity;
        $findResult = array();
        $itemId=null;
        foreach($results as $key => $value)
        {
            /** @var $value Value */
            $id=$value->getValue();
            $img=sysFileImageAdapterEntity::getImage($id);
            $map->setImg($img);
            $item=$value->getItem();
            $str=$item->getStructure();
            /** @var $struct Structure */
            $struct=$str[0];
            $url=$struct->getUrl();
            $map->setUrl($url);


            $findResult[]=$map;
        }



        unset($value);
        $findResult=self::checkNullArray($findResult);
        return $findResult;


    }

    /**
     *value.id as vId, value.value as vaval,
    item.id AS itId, item.title AS ittl,
    structure.id as sid, structure.url as surl,
    category_control.variable as cclvr, category_control.id as cclId, category_control.gallery_thumbnail AS cclGllThnl,
    picture.id AS pcId, picture.format AS format, picture.url AS ext, picture.title AS label
     *
     * @return ResultSetMapping
     */
    protected static function getRSM()
    {
        $rsm= new ResultSetMapping;
        $rsm->addEntityResult('export\Value', 'v');
        $rsm->addFieldResult('v', 'vId', 'id');
        $rsm->addFieldResult('v', 'vaval', 'value');


        $rsm->addMetaResult('v', 'valccl', 'categoryControl');
        $rsm->addJoinedEntityResult('export\CategoryControl' , 'ccl', 'v', 'categoryControl');
        $rsm->addFieldResult('ccl', 'cclId', 'id');
        $rsm->addFieldResult('ccl', 'cclvr', 'variable');
        $rsm->addFieldResult('ccl', 'cclGllThnl', 'galleryThumbnail');

        $rsm->addMetaResult('v', 'valit', 'item');
        $rsm->addJoinedEntityResult('export\Item' , 'i', 'v', 'item');
        $rsm->addFieldResult('i', 'itId', 'id');
        $rsm->addFieldResult('i', 'ittl', 'title');

        $rsm->addMetaResult('i', 'sId', 'strusture');
        $rsm->addJoinedEntityResult('export\Structure' , 's', 'i', 'structure');
        $rsm->addFieldResult('s', 'sId', 'id');
        $rsm->addFieldResult('s', 'surl', 'url');
        /*
                $rsm->addMetaResult('v', 'pvpId', 'picture');
                $rsm->addJoinedEntityResult('export\Picture' , 'p', 'v', 'picture');
                $rsm->addFieldResult('p', 'pcId', 'id');
                $rsm->addFieldResult('p', 'format', 'format');
                $rsm->addFieldResult('p', 'ext', 'url');
                $rsm->addFieldResult('p', 'label', 'title');

                $rsm->addMetaResult('p', 'pcdir', 'folderId');
                $rsm->addJoinedEntityResult('export\Folder' , 'f', 'p', 'folderId');
                $rsm->addFieldResult('f', 'dirId', 'id');
                $rsm->addFieldResult('f', 'dir', 'shortUrl');

                $rsm->addMetaResult('p', 'cclGllThnl', 'size');
                $rsm->addJoinedEntityResult('export\PictureSize' , 'psz', 'p', 'size');
                $rsm->addFieldResult('psz', 'pszId', 'id');
                $rsm->addFieldResult('psz', 'size', 'resolution');
        */
        return $rsm;
    }
    private  static function checkNull(array $check)
    {

        $res=false;
        foreach($check as $null)
        {

            $fl1=($null === "")?true:false;
            $fl2=($null === null)?true:false;
            $fl3=((is_array($null) && (count($null) == 0)))?true:false;
            if($fl1 || $fl2 || $fl3) $res=$res || false;
            else $res=$res || true;
        }



        return $res;
    }
    private   static function checkNullArray($check)
    {
        $flag=false;
        foreach($check as $checkedItem)
        {
            if(($checkedItem instanceof MapAdapterEntity) || (($checkedItem instanceof ArrayCollection) && ($checkedItem->count() > 0)) || (is_array($checkedItem) && (count($checkedItem) > 0) && self::checkNull($checkedItem)))
                $flag=true;
        }
        $res=$flag?$check:array();
        return $res;
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param  $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }
}