<?php
namespace entities\policy;
use entities\sys\sysIntRangeEntity;
use entities\_abstract\sysAbstract as sysAbstract;
use components\JArrayCollection as ArrayCollection;
/**
 *
 *
 * Класс хранения возрастов детской политики
 *
 *
 * @date: 06.01.13 - 18:10
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 *
 * @modified mike
 * @date 15.01.13 - 12:43
 * @description Перенос из проекта Jazz.Application
 *
 * @modified Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @date 24.01.13 - 15:13
 * @description Переименование anyChildPolicyEntity -> ChildPolicyEntity, anyChildAgesEntity -> ChildAgesEntity
 */
class ChildAgesAdapterEntity extends ChildAgesEntity{


    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
