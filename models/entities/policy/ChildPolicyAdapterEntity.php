<?php
namespace entities\policy;
use components\JArrayCollection as ArrayCollection;
use entities\_abstract\sysSimpleAbstract as sysSimpleAbstract;
/**
 *
 * Класс хранения детской политики
 *
 *
 * @date: 06.01.13 - 18:23
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 *
 * @modified mike
 * @date 15.01.13 - 12:44
 * @description Перенос из проекта Jazz.Application
 *
 * @modified Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @date 24.01.13 - 15:19
 * @description Переименование anyChildPolicyEntity -> ChildPolicyEntity, anyChildAgesEntity -> ChildAgesEntity
 *
 */
class ChildPolicyAdapterEntity extends ChildPolicyEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
