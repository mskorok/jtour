<?php
namespace entities\menu;
use components\JArrayCollection as ArrayCollection;
use entities\_abstract\sysAbstract;
use entities\sys\sysPackageEntity;
use entities\page\sysEntity as pageSysEntity;

/**
 *
 */
class sysItemAdapterEntity extends sysItemEntity{

    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }

}