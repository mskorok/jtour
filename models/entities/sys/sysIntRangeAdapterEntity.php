<?php
namespace entities\sys;
use entities\_abstract\sysAbstract as sysAbstract;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 15.01.13
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 *
 * @date: 06.01.13 - 18:05
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 *
 *
 */
class sysIntRangeAdapterEntity  extends sysIntRangeEntity
{


    const USE_ADAPTER = true;


    protected  static function _aFindAll(){
        return true;
    }

    protected  static function _aFind($id){
        return true;
    }

    protected static function _aFindBy(array $criteria, array $orderBy = null, $limit = null, $offset = null){
        return true;
    }

    protected static function _aFindOneBy(array $criteria, array $orderBy = null)
    {
        return true;
    }
}
