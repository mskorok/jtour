<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * price
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\priceRepository")
 */
class price extends entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="float")
     */
    protected $price;

    /**
     * @var float
     *
     * @ORM\Column(name="foreigners", type="float",  nullable=true)
     */
    protected $foreigners;

    /**
     * @var float
     *
     * @ORM\Column(name="children", type="float",  nullable=true)
     */
    protected $children;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float",  nullable=true)
     */
    protected $discount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime",  nullable=true)
     */
    protected $expires;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    protected $currency;


    public function __toString(){
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return price
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set foreigners
     *
     * @param float $foreigners
     * @return price
     */
    public function setForeigners($foreigners)
    {
        $this->foreigners = $foreigners;

        return $this;
    }

    /**
     * Get foreigners
     *
     * @return float 
     */
    public function getForeigners()
    {
        return $this->foreigners;
    }

    /**
     * Set children
     *
     * @param float $children
     * @return price
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return float 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return price
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set expires
     *
     * @param \DateTime $expires
     * @return price
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return \DateTime 
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}
