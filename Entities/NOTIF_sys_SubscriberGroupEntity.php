<?php
namespace Jazz\Entities;

/**
 * @Entity
 */
class NOTIF_sys_SubscriberGroupEntity extends \Jazz\Entities\NOTIF_sys_SubscriberEntity{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="USER_sys_GroupEntity", inversedBy="subscribers", cascade={"persist"})
     * @var \Jazz\Entities\USER_sys_GroupEntity
     */
    protected $group;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              По сути он используется в EntityGridRowBuilder для отображения Заголовка сущности
     * @return string
     */
    public function getTitle() {
        return $this->getNotificationTitle() . $this->getGroupTitle();
    }

    /**
     * @param \Jazz\Entities\USER_sys_GroupEntity $group
     * @return NOTIF_sys_SubscriberGroupEntity
     */
    public function setGroup($group) {
        $this->group = $group;
        return $this;
    }

    /**
     * @return \Jazz\Entities\USER_sys_GroupEntity
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getGroupTitle() {
        return $this->getGroup()->getTitle();
    }


}