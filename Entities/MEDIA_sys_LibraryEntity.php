<?php
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена
 */
namespace Jazz\Entities;
use DateTime;
/**
 * @Entity
 * @InheritanceType("JOINED")
 * @Table(name="media_library_entities")
 * @DiscriminatorColumn(name="discrim", type="string")
 * @DiscriminatorMap({"mediaLibraryEntity" = "MEDIA_sys_LibraryEntity", "mediaFileAudioEntity" = "MEDIA_sys_FileAudioEntity", "mediaFileVideoEntity" = "MEDIA_sys_FileVideoEntity", "mediaFileImageEntity" = "MEDIA_sys_FileImageEntity", "mediaFileDocumentEntity" = "MEDIA_sys_FileDocumentEntity"})
 */
class MEDIA_sys_LibraryEntity extends \Jazz\Entities\ENTITY_sys_GeneralAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description Тип медиа-файла(аудио==1; документ==2; изображение==3; видео==4)
     * @var int
     * @Column(type="integer")
     */
    protected $typeMedia;
    /**
     * @ManyToOne(targetEntity="MEDIA_sys_FileFormatEntity") связь многие к одному односторонняя
     * @var MEDIA_sys_FileFormatEntity
     */
    protected $format;
    /**
     * @description Описание медиа-файла
     * @Column(type="string")
     * @var string
     */
    protected $description;
    /**
     * @size Размер медиа-файла
     * @Column(type="integer")
     * @var int
     */
    protected $size;
    /**
     * @description Дата создания
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $dateCreation;
    /**
     * @description Кто создал
     * @Column(type="string", length=32)
     * @var  string
     */
    protected $whoCreated;
    /**
     * @description Дата изменения
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $dateChange;
    /**
     * @description Кто изменил
     * @Column(type="string", length=32)
     * @var  string
     */
    protected $whoChanged;
    /**
     * @description Внутренняя ссылка
     * @Column(type="text", length=128)
     * @var string
     */
    protected $internalUrl;
    /**
     * Метод отвечает за генерацию Url для пользовательской страницы.
     * По умолчанию возвращает пустую строку поэтому именно сущность потомка должна отвечать за то как именно
     * генерировать url.
     * @return string Url для пользовательской страницы
     */
    protected function getUrlForUserPage()
    {
        // TODO: Implement getUrlForUserPage() method.
    }

    /**
     * @param \DateTime $dateCreation
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateChange
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setDateChange($dateChange)
    {
        $this->dateChange = $dateChange;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateChange()
    {
        return $this->dateChange;
    }

    /**
     * @param string $description
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param \Jazz\Entities\MEDIA_sys_FileFormatEntity $format
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return \Jazz\Entities\MEDIA_sys_FileFormatEntity
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $internalUrl
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setInternalUrl($internalUrl)
    {
        $this->internalUrl = $internalUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getInternalUrl()
    {
        return $this->internalUrl;
    }

    /**
     * @param int $typeMedia
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setTypeMedia($typeMedia)
    {
        $this->typeMedia = $typeMedia;
        return $this;
    }

    /**
     * @return int
     */
    public function getTypeMedia()
    {
        return $this->typeMedia;
    }

    /**
     * @param int $size
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $whoChanged
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setWhoChanged($whoChanged)
    {
        $this->whoChanged = $whoChanged;
        return $this;
    }

    /**
     * @return string
     */
    public function getWhoChanged()
    {
        return $this->whoChanged;
    }

    /**
     * @param string $whoCreated
     * @return MEDIA_sys_LibraryEntity ${HINT}
     */
    public function setWhoCreated($whoCreated)
    {
        $this->whoCreated = $whoCreated;
        return $this;
    }

    /**
     * @return string
     */
    public function getWhoCreated()
    {
        return $this->whoCreated;
    }


}