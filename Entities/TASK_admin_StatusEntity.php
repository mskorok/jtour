<?php
namespace Jazz\Entities;
use Jazz\Entities\USER_sys_Entity, DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.11.12
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 *
 *  @description Статус выполнения задачи в менеджере задач
 *
 * @Entity
 * @Table(name="task_status_entities")
 */
class TASK_admin_StatusEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description Описание(название) статуса
     * @Column(type="string")
     * @var
     */
    protected $statusDescription;

    /**
     * @ManyToMany(targetEntity="TASK_admin_StatusEntity", inversedBy="nextStatuses")
     * @JoinTable(name="task_new_status",
     * joinColumns={@JoinColumn(name="status_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="nextStatuses_id", referencedColumnName="id")}
     * )
     * @var
     */
    protected $status;
    /**
     * @ManyToMany(targetEntity="TASK_admin_StatusEntity", mappedBy="status")
     * @var
     */
    protected $nextStatuses;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct()
    {
        parent::__construct();

        $this->status = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nextStatuses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/


    /**
     * @param  $statusDescription
     */
    public function setStatusDescription($statusDescription)
    {
        $this->statusDescription = $statusDescription;
    }

    /**
     * @return
     */
    public function getStatusDescription()
    {
        return $this->statusDescription;
    }

    /**
     * @param  $nextStatuses
     */
    public function setNextStatuses($nextStatuses)
    {
        $this->nextStatuses = $nextStatuses;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getNextStatuses()
    {
        return $this->nextStatuses;
    }

    /**
     * @param  $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getStatus()
    {
        return $this->status;
    }


}
