<?php
/**
 * Created by IntelliJ IDEA.
 * User:Еlena Naumova
 * Date: 27.11.12
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
use DateTime;
use DatePeriod;
use DateInterval;

/**
 * @Entity
 * @Table(name="policy_charging_policy_entities")
 *  таблица для определения сущности, определяющий параметры расчета стоимости опции.
 *  Имеет набор свойств:
 *  name- название.
 *	chargingDuration: продолжительность – стоимость за ночь, за час , за неделю и тп.
 *  сapacity- Вместимость.  Максимально допустимое количество использующих выбранную опцию лиц
 *  Day Overlap- от  данного свойства зависит считается ли стоимость за ночь или за день
 *  Unit based - если галка активна стоимость считается за услугу, не зависимо от кол-ва человек,
 *   если галка не активна то наоборот,  стоимость считается (умножается) на кол-во человек
*/
class POLICY_booking_ChargingPolicyEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description  продолжительность – данное свойство необходимо, что бы определить как часто брать с клиента плату.
     * Вот несколько простых примеров.
     * Два дня - P2D.
     * Две секунды - PT2S.
     * Шесть лет и пять минут - P6YT5M.
     *
     *  Y	года
     *  M	месяцы
     *  D	дни
     *  W	недели. Преобразуется в дни, поэтому не может быть использован совместно с D.
     *  H	часы
     *  M	минуты
     *  S	секунды
     *
     * @Column(type="string", length=30)
     * @var  string
     */
    protected $duration;
    /**
     * Максимально допустимое количество использующих выбранную опцию лиц
     *
     * @Column(type="integer")
     * @var integer
     */
    protected $capacity;
    /**
     * от данного свойства зависит считается ли стоимость за ночь или за день
     *
     * @Column(type="boolean")
     * @var bool
    */
    protected $dayOverlap;
    /**
     * @description  если галка активна стоимость считается за услугу, не зависимо от кол-ва человек,
     *   если галка не активна то наоборот,  стоимость считается (умножается) на кол-во человек
     *
     * @Column(type="boolean")
     * @var bool
     */
    protected $perUnit;

    /******************************************************************************************************************
     * Additional methods
     *****************************************************************************************************************/
    /**
     * Считает стоимость исходя из следующей логики.
     *  1) Определяет за что цена - за человека испольющего опцию или же за опцию
     *      1.1) Если цена за человека то берет price и умножает на кол-во персон использующих опцию
     *      1.2) Если цена за опцию то берет price и умножает на максимальную вместимости опции
     *  2) Далее умножаем на кол-во повторении во времени длительности опции.
     *
     * @param int $price Цена которою нужно пересчитать
     * @param int $persons Кол-во человек использующих опцию
     * @param \DateTime $dateStart Дата начала использования опции
     * @param \DateTime $dateEnd Дата окончания использования опции
     * @return int
     */
    public function calculate($price, $persons, $dateStart, $dateEnd){
        $repetitions = $this->_getCountOfRepetitions($dateStart, $dateEnd);

        return $this->getPerUnit()
            ? $this->_calculatePerUnit($price, $persons) * $repetitions
            : $this->_calculatePerPerson($price, $persons) * $repetitions;
    }

    /**
     * Возвращает цену за единицу сервиса с учетом максимальной вместимости сервиса
     * @param int $price Цена которою нужно пересчитать
     * @param int $persons Кол-во человек использующих опцию
     * @return int Цена за
     */
    private function _calculatePerUnit($price, $persons){
        $capacity = $this->getCapacity();
        $countUnit = $capacity ? ceil($persons / $capacity) : 1;

        return $price * $countUnit;
    }

    /**
     * Возвращает цену за кол-во человек использующих сервис
     * @param int $price Цена которою нужно пересчитать
     * @param int $persons Кол-во человек использующих опцию
     * @return int Цена за кол-во человек
     */
    private function _calculatePerPerson($price, $persons){
        return $price * $persons;
    }

    /**
     * Возвращает кол-во повторений в зависимости от времени продолжительности действия политики
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return int
     */
    private function _getCountOfRepetitions($dateStart, $dateEnd){

        if( $this->getDuration() == "" ) return 1;

        if( $this->getDayOverlap() ) $dateStart = $dateStart->sub(new DateInterval('P1D') );

        $repetitions = 0;
        $interval = new DateInterval($this->getDuration());
        $period = new DatePeriod($dateStart, $interval, $dateEnd);

        // При переборе экземпляра DatePeriod в цикле будут отображены все отобранные даты периода.
        foreach ($period as $date) {
            $repetitions++;
        }

        return $repetitions ? $repetitions : 1;
    }

    /******************************************************************************************************************
     * Getter / Setter
     *****************************************************************************************************************/
    /**
     * @param int $capacity
     * @return PRICE_booking_ChargingPolicyEntity
     */
    public function setCapacity($capacity) {
        $this->capacity = $capacity;
        return $this;
    }

    /**
     * @return int
     */
    public function getCapacity() {
        return $this->capacity;
    }

    /**
     * @param boolean $dayOverlap
     * @return PRICE_booking_ChargingPolicyEntity
     */
    public function setDayOverlap($dayOverlap) {
        $this->dayOverlap = $dayOverlap;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDayOverlap() {
        return $this->dayOverlap;
    }

    /**
     * @param string $duration
     * @return PRICE_booking_ChargingPolicyEntity
     */
    public function setDuration($duration) {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration() {
        return $this->duration;
    }

    /**
     * @param boolean $perUnit
     * @return PRICE_booking_ChargingPolicyEntity
     */
    public function setPerUnit($perUnit) {
        $this->perUnit = $perUnit;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getPerUnit() {
        return $this->perUnit;
    }
}