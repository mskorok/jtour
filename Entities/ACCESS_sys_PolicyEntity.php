<?php
namespace Jazz\Entities;
use   \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 23.10.12
 * Time: 10:59
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс создающий таблицу политик доступа
 *
 *
 * @Entity
 * @Table(name="access_policy_entities")
 *
 */
class ACCESS_sys_PolicyEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /**
     *
     * @description Политики доступа - видимости, редактирования, создания, удаления
     *
     *
     * @Column
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $policy;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {
        parent::__construct();
        $this->policy = new \Doctrine\Common\Collections\ArrayCollection();


    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $policy
     * @return ACCESS_sys_PolicyEntity ${HINT}
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPolicy()
    {
        return $this->policy;
    }

}
