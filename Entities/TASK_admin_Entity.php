<?php
namespace Jazz\Entities;
use Jazz\Other\Utils\DefaultValueService As DefService;
use Jazz\Entities\USER_sys_Entity, Jazz\Entities\TASK_admin_StatusEntity, DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.11.12
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 *
 *  @description Менеджер задач
 *
 * @Entity
 * @Table(name="task_entities")
 */
class TASK_admin_Entity  extends ENTITY_sys_SimpleAbstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description тип сущности, к которой принадлежим
     * @var string
     */
    protected $linkedEntityType;
    /**
     * @description id сущности, к которой принадлежим
     * @var int
     */
    protected $linkedEntityId;
    /**
     * @description Описание задачи
     * @Column(type="text")
     * @var
     */
    protected $description;

    /**
     *  @description ссылка на пользователя, который является автором
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     *
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $author;
    /**
     * @description ссылка на пользователя, который является исполнителем
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinColumn(name="performer_id", referencedColumnName="id")
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $performer;
    /**
     * @description ссылка на статус, который в данный момент имеет задача
     * @ManyToOne(targetEntity="TASK_admin_StatusEntity")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     * @var
     */
    protected $status;
    /**
     * @ManyToMany(targetEntity="COMMENTS_any_Entity")
     * @JoinTable(name="task_comments",
     * joinColumns={@JoinColumn(name="task_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="comments_id", referencedColumnName="id", unique=true)}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $comments;
    /**
     * @description Пока непонятно что это
     * @ManyToMany(targetEntity="TAGS_any_Entity")
     * @JoinTable(name="tags_for_tasks",
     * joinColumns={@JoinColumn(name="task_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $tags;
    /**
     * @description Дата выполнения задачи
     * @Column(type="datetime" )
     * @var  \DateTime
     */
    protected $performanceDate;
    /**
     * @description Дата создания задачи
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $created;
    /**
     * @description Дата обновления задачи
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $updated;
    /**
     * @description Флаг устанавливающий завершенность выполнения задачи
     * @Column(type="boolean")
     * @var boolean
     */
    protected $isClosed;  //можно и без него, но это позволяет не использовать лишний раз выборку из базы
    /**
     * @description Флаг определяющий, удалена ли задача
     * @Column(type="boolean")
     * @var boolean
     */
    protected $isRemoved;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct()
    {
        parent::__construct();
        $this->isClosed=false;
        $this->isRemoved=false;
        $this->description = "";
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
        $this->tags    = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments= new \Doctrine\Common\Collections\ArrayCollection();
        $this->status= \Jazz\Application::getInstance()->getEntityFindOneBy("TASK_admin_StatusEntity", array("title" => "открыто"));//Здесь потом надо будет поставить  id, но пока так

    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/

    protected  function setStatus(\Jazz\Entities\TASK_admin_StatusEntity $newStatus)
    {
      if(($this->author->getId() != $this->performer->getId()) && ($newStatus->getId() == 4 || $newStatus->getId() == 5 ))
      {
        return false;
      }
        $this->setNewStatus($newStatus);
        return true;
    }
    public function getTaskForDataView()
    {
        $query="SELECT n,  b, c, d  FROM Jazz\Entities\TASK_admin_Entity n JOIN n.status b JOIN n.comments c JOIN n.tags d WHERE n.id IN ({$this->getId()})";
        $query=\Jazz\Application::getInstance()->getEntityManager()->createQuery($query);
        $task=$query->getResult();
        return $task[0];
    }
    public   function getStatusList()
    {

        $id=$this->getStatus()->getId();
        $query="SELECT n,  b  FROM Jazz\Entities\TASK_admin_StatusEntity n JOIN n.status b WHERE n.id  IN ({$id})";
        $query=\Jazz\Application::getInstance()->getEntityManager()->createQuery($query);
        $res=$query->getResult();
        return $res[0];
    }
    public  function addTag($tag)
    {
        $this->save(array($tag));

    }
    public  function addComment(\Jazz\Entities\COMMENTS_any_Entity $comment)
    {
        $comment->setLinkedEntityType($this->getEntityName());
        $comment->setLinkedEntityId($this->getId());
        $this->save(array($comment));

    }



    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return USER_sys_Entity
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * @param $performanceDate
     */
    public function setPerformanceDate($performanceDate)
    {
        $this->performanceDate = $performanceDate;
    }

    /**
     * @return \DateTime
     */
    public function getPerformanceDate()
    {
        return $this->performanceDate;
    }

    /**
     * @param TASK_admin_StatusEntity $status
     */
    public function setNewStatus($status)
    {
        $this->status = $status;
        if($status->getTitle() == "готово")
        {
            $this->isClosed=true;
        }
    }

    /**
     * @return object
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param $performer
     * @return void
     */
    public function setPerformer($performer)
    {
        $this->performer = $performer;
    }

    /**
     * @return USER_sys_Entity
     */
    public function getPerformer()
    {
        return $this->performer;
    }



    public function getCreated()
    {
        return $this->created;
    }

    public function setUpdated()
    {
        $this->updated = new \DateTime("now");
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param boolean $isClosed
     */
    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;
    }

    /**
     * @return boolean
     */
    public function getIsClosed()
    {
        return $this->isClosed;
    }

    /**
     * @param  $isRemoved
     */
    public function setIsRemoved($isRemoved)
    {
        $this->isRemoved = $isRemoved;
    }

    /**
     * @return bool
     */
    public function getIsRemoved()
    {
        return $this->isRemoved;
    }

    /**
     * @param int $linkedEntityId
     */
    public function setLinkedEntityId($linkedEntityId)
    {
        $this->linkedEntityId = $linkedEntityId;
    }

    /**
     * @return int
     */
    public function getLinkedEntityId()
    {
        return $this->linkedEntityId;
    }

    /**
     * @param string $linkedEntityType
     */
    public function setLinkedEntityType($linkedEntityType)
    {
        $this->linkedEntityType = $linkedEntityType;
    }

    /**
     * @return string
     */
    public function getLinkedEntityType()
    {
        return $this->linkedEntityType;
    }


}
