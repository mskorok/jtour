<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="admin_pages_entities")
 */
class PAGE_admin_AdminEntity extends \Jazz\Entities\PAGE_sys_Entity{

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function getPageItemName(){
        return array("name-text-general");
    }

    public function getPageTemplate(){
        $names = $this->getPageItemName();
        return "<div name=\"{$names['0']}\"></div>";
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMetaKeywords() {
        return new \Doctrine\Common\Collections\ArrayCollection(array());
    }

    /**
     * @return string
     */
    public function getMetaTitle() {
        return $this->getTitle();
    }

    /**
     * @return string
     */
    public function getMetaDescription() {
        return $this->getTitle();
    }

}