<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 07.09.12
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
/**
 * @Entity
 * @Table(name="translates_entities")
 */
class TRANSLATE_any_Entity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="integer")
     * @var integer
     */
    protected $messageId;
    /**
     * @ManyToOne(targetEntity="TRANSLATE_sys_LanguageEntity")
     * @var \Jazz\Entities\TRANSLATE_sys_LanguageEntity
     */
    protected $language;
    /**
     * @Column(type="string", length=2048)
     * @var string
     */
    protected $translate;

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getTitle() {
        return $this->getTranslate();
    }

    /**
     * @param \Jazz\Entities\TRANSLATE_sys_LanguageEntity $language
     * @return TRANSLATE_any_Entity
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return \Jazz\Entities\TRANSLATE_sys_LanguageEntity
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $translate
     * @return TRANSLATE_any_Entity
     */
    public function setTranslate($translate) {
        $this->translate = $translate;
        return $this;
    }

    /**
     * @return string
     */
    public function getTranslate() {
        return $this->translate;
    }

    /**
     * @param int $messageId
     * @return TRANSLATE_any_Entity
     */
    public function setMessageId($messageId) {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMessageId() {
        return $this->messageId;
    }

}
