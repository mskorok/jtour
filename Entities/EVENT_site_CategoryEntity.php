<?php
namespace Jazz\Entities;
use Jazz\Other\Utils\DefaultValueService As DefService;
use DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 20.11.12
 * Time: 10:23
 * To change this template use File | Settings | File Templates.
 *  @description категория событий
 *
 * @Entity
 * @Table(name="event_category_entities")
 */
class EVENT_site_CategoryEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @var
     * @Column(type="string", length=250)
     */
    private $category;
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

}
