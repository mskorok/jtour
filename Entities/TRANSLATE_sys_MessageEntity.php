<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 07.09.12
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
/**
 * @Entity
 * @Table(name="messages_entities")
 */
class TRANSLATE_sys_MessageEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события
     */
    const MUTE = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=256)
     * @var string
     */
    protected $linkedEntityName;
    /**
     * @Column(type="integer")
     * @var integer
     */
    protected $linkedEntityId;
    /**
     * @Column(type="string", length=256)
     * @var string
     */
    protected $propertyName;
    /**
     * @ManyToOne(targetEntity="TRANSLATE_sys_LanguageEntity", cascade={"persist"})
     * @var \Jazz\Entities\TRANSLATE_sys_LanguageEntity
     */
    protected $language;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getTitle() {
        return $this->getEntityName() . "_" . $this->getLinkedEntityId() . "_" . $this->getPropertyName() . "_" . $this->getLanguage()->getSymbol();
    }

    /**
     * @param \Jazz\Entities\TRANSLATE_sys_LanguageEntity $language
     * @return TRANSLATE_sys_MessageEntity
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return \Jazz\Entities\TRANSLATE_sys_LanguageEntity
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $propertyName
     * @return TRANSLATE_sys_MessageEntity
     */
    public function setPropertyName($propertyName) {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyName() {
        return $this->propertyName;
    }

    /**
     * @param int $linkedEntityId
     * @return TRANSLATE_sys_MessageEntity
     */
    public function setLinkedEntityId($linkedEntityId) {
        $this->linkedEntityId = $linkedEntityId;
        return $this;
    }

    /**
     * @return int
     */
    public function getLinkedEntityId() {
        return $this->linkedEntityId;
    }

    /**
     * @param string $linkedEntityName
     * @return TRANSLATE_sys_MessageEntity
     */
    public function setLinkedEntityName($linkedEntityName) {
        $this->linkedEntityName = $linkedEntityName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedEntityName() {
        return $this->linkedEntityName;
    }

}
