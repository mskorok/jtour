<?php
namespace Jazz\Entities;

 /**
  * @Entity
  * @Table(name="geo_country_entities")
  */
class GEO_any_CountryEntity extends \Jazz\Entities\GEO_any_TreeEntity{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="SYS_any_CurrencyEntity")
     * @var SYS_any_CurrencyEntity
     */
    protected $currency;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @return GEO_any_CountryEntity
     */
    public function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @return string
     */
    public function getCurrencySymbol() {
        return $this->getCurrency()->getSymbol();
    }

    /**
     * @return string
     */
    public function getCurrencyName() {
        return $this->getCurrency()->getShortName();
    }

    /**
     * @param \Jazz\Entities\SYS_any_CurrencyEntity $currencyEntity
     * @return GEO_any_CountryEntity
     */
    public function setCurrency($currencyEntity) {
        $this->currency = $currencyEntity;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_any_CurrencyEntity
     */
    public function getCurrency() {
        return $this->currency;
    }

//    /**
//     * @param \Jazz\Entities\TRANSLATE_sys_LanguageEntity $language
//     * @return GEO_any_CountryEntity
//     */
//    public function setLanguage($language) {
//        $this->language = $language;
//        return $this;
//    }
//
//    /**
//     * @return \Jazz\Entities\TRANSLATE_sys_LanguageEntity
//     */
//    public function getLanguage() {
//        return $this->language;
//    }
//
//    /**
//     * @return \Jazz\Entities\TRANSLATE_sys_LanguageEntity
//     */
//    public function getLanguageTitle() {
//        return $this->getLanguage()->getTitle();
//    }
//
//    /**
//     * @return \Jazz\Entities\TRANSLATE_sys_LanguageEntity
//     */
//    public function getLanguageSymbol() {
//        return $this->getLanguage()->getSymbol();
//    }

}