<?php
namespace Jazz\Entities;
/**
 * @class \Jazz\Entities\POLICY_any_ChildAgesEntity
 * @date: 06.01.13 - 18:10
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @Entity
 * @Table(name="policy_child_ages_entities")
 */
class POLICY_any_ChildAgesEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /**
     * @ManyToOne(targetEntity="POLICY_any_ChildPolicyEntity", inversedBy="children")
     * @JoinColumn(name="policy_id", referencedColumnName="id", nullable=true)
     * @var \Jazz\Entities\POLICY_any_ChildPolicyEntity
     */
    protected $policy;

    /**
     * @ManyToMany(targetEntity="SYS_sys_IntRangeEntity")
     * @JoinTable(name="policy_ages_ranges",
     *      joinColumns={@JoinColumn(name="age_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="range_id", referencedColumnName="id")}
     *      )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $ages;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $childOrder;

    function __construct() {
        parent::__construct();
        $this->ages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->childOrder = 1;
    }

    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle() {
        return $this->getChildOrder() . ' ребенок от '. $this->getMinAge() .' до '. $this->getMaxAge();
    }

    /******************************************************************************************************************
     * Additional methods
     *****************************************************************************************************************/
    /**
     * Возвращает минимальный возраст
     * @return int
     */
    public function getMinAge(){
        $min = 0;

        $this->getAges()->map(function($range)use(&$min){
            /**
             * @var $range \Jazz\Entities\SYS_sys_IntRangeEntity
             */
            $minRange = $range->getMin();
            if( $minRange < $min ){
                $min = $minRange;
            }
        });

        return $min;
    }

    /**
     * Возвращает максимальный возраст
     * @return int
     */
    public function getMaxAge(){
        $max = 0;

        $this->getAges()->map(function($range)use(&$max){
            /**
             * @var $range \Jazz\Entities\SYS_sys_IntRangeEntity
             */
            $maxRange = $range->getMax();
            if( $maxRange > $max ){
                $max = $maxRange;
            }
        });

        return $max;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $ages
     * @return POLICY_any_ChildAgesEntity
     */
    public function setAges($ages) {
        $this->ages = $ages;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAges() {
        return $this->ages;
    }

    /**
     * @param int $childOrder
     * @return POLICY_any_ChildAgesEntity
     */
    public function setChildOrder($childOrder) {
        $this->childOrder = $childOrder;
        return $this;
    }

    /**
     * @return int
     */
    public function getChildOrder() {
        return $this->childOrder;
    }

    /**
     * @param \Jazz\Entities\POLICY_any_ChildPolicyEntity $policy
     * @return POLICY_any_ChildAgesEntity
     */
    public function setPolicy($policy) {
        $this->policy = $policy;
        return $this;
    }

    /**
     * @return \Jazz\Entities\POLICY_any_ChildPolicyEntity
     */
    public function getPolicy() {
        return $this->policy;
    }
}
