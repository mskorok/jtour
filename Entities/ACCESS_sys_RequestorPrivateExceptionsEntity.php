<?php
namespace Jazz\Entities;
use  DateTime, \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.10.12
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс создающий таблицу соответствия сущностей и персональных исключений
 * в соответствии с персональной политикой доступа
 *
 * @Entity
 * @Table(name="access_private_exceptions_entities")
 */
class ACCESS_sys_RequestorPrivateExceptionsEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{

    /**
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_private_exceptions",
     * joinColumns={@JoinColumn(name="requestor_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     *  @description массив идентификаторов групп, входящих в исключения
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $privateId;
    /**
     * @description идентификатор пользователя, создавшего исключение(администратора)
     *
     * @ManyToMOne(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_private_exceptions_creator",
     * joinColumns={@JoinColumn(name="creator_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $creator;
    /**
     * @description Дата и время создания исключения
     * @Column(type="datetime")
     * @var datetime
     */
    protected $creationDate;

    /**
     * @description условия применения политики - разрешить/запретить, показать/скрыть
     * @Column(type="boolean")
     * @var bool
     */
    protected $conditions;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {
        parent::__construct();
        $this->privateId = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creator = new \Doctrine\Common\Collections\ArrayCollection();


    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param $privateId
     * @return ACCESS_sys_RequestorPrivateExceptionsEntity
     */
    public function setPrivateId($privateId)
    {
        $this->privateId = $privateId;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPrivateId()
    {
        return $this->privateId;
    }

    /**
     * @param $creator
     * @return ACCESS_sys_RequestorPrivateExceptionsEntity
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return ACCESS_sys_RequestorPrivateExceptionsEntity
     */
    public function setCreationDate()
    {
        $this->creationDate =  new \DateTime("now");
        return $this;
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param $conditions
     * @return ACCESS_sys_RequestorPrivateExceptionsEntity
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getConditions()
    {
        return $this->conditions;
    }




}
