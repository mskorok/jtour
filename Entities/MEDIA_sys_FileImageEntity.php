<?php
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена
 */
namespace Jazz\Entities;
/**
 * @description Таблица изображений
 * @Entity
 * @Table(name="media_file_image_entities")
 */
class MEDIA_sys_FileImageEntity extends \Jazz\Entities\MEDIA_sys_LibraryEntity{
    /******************************************************************************************************************
    * Data Base Property / Свойства в базе данных
    ******************************************************************************************************************/
    /**
     * @description высота изображения
     * @var int
     * @Column(type="integer")
     */
    protected $high;
    /**
     * @description ширина изображения
     * @var int
     * @Column(type="integer")
     */
    protected $width;
    /**
    * @description Позиция водяного знака: left == 0 , right == 1, top == 2, bottom == 3, center == 4
    * @var int
    * @Column(type="integer")
    */
    protected $waterMarkPosition;

    /**
     * @param int $high
     * @return MEDIA_sys_FileImageEntity ${HINT}
     */

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @return MEDIA_sys_FileImageEntity
     */
    public function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    public function setHigh($high)
    {
        $this->high = $high;
        return $this;
    }

    /**
     * @return int
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * @param int $waterMarkPosition
     * @return MEDIA_sys_FileImageEntity ${HINT}
     */
    public function setWaterMarkPosition($waterMarkPosition)
    {
        $this->waterMarkPosition = $waterMarkPosition;
        return $this;
    }

    /**
     * @return int
     */
    public function getWaterMarkPosition()
    {
        return $this->waterMarkPosition;
    }

    /**
     * @param int $width
     * @return MEDIA_sys_FileImageEntity ${HINT}
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}
