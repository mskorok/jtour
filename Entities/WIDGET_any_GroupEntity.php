<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 08.11.12
 * Time: 2:10
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;

use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Данная сущность описывает группы однотипных виджетов, например различные виджеты для показа меню
 * По этим группам будет строится экран списка доступных виджетов а так же экран добавления нового виджета на страницу
 * @Entity
 * @Table(name="widget_groups_entities")
 */
class WIDGET_any_GroupEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * Приоритет группы виджетов, чем приоритет больше тем выше он в списке
     * @Column(type="integer")
     * @var integer
     */
    protected $priority;
    /**
     * Системное имя группы однотипных виджетов
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * Описание группы однотипных виджетов
     * @Column(type="text")
     * @var string
     */
    protected $description;
    /**
     * Список типов виджетов принадлежаших данной группе
     * @OneToMany(targetEntity="WIDGET_any_TypeEntity", mappedBy="widgetGroup")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $widgetTypes;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->widgetTypes = new ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $description Описание группы однотипных виджетов
     * @return WIDGET_any_TypeEntity
     */
    public function setDescription($description) {
        $this->setTranslateString('description', $description);
        return $this;
    }

    /**
     * @return string Описание группы однотипных виджетов
     */
    public function getDescription() {
        return $this->getTranslateString('description');
    }

    /**
     * @param $name
     * @param string $name Системное имя группы однотипных виджетов
     * @return WIDGET_any_GroupEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string Системное имя группы однотипных виджетов
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $widgetTypes Список типов виджетов принадлежаших данной группе
     * @return WIDGET_any_GroupEntity
     */
    public function setWidgetTypes($widgetTypes) {
        $this->widgetTypes = $widgetTypes;
        return $this;
    }

    /**
     * @param WIDGET_any_TypeEntity $widgetType Список типов виджетов принадлежаших данной группе
     * @return WIDGET_any_GroupEntity
     */
    public function addWidgetType($widgetType) {
        $widgetType->setWidgetGroup($this);
        $collection = $this->widgetTypes;
        $collection->add($widgetType);
        $this->widgetTypes = $collection;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection Список типов виджетов принадлежаших данной группе
     */
    public function getWidgetTypes() {
        return $this->widgetTypes;
    }

    /**
     * @param int $priority Приоритет группы виджетов
     * @return WIDGET_any_GroupEntity
     */
    public function setPriority($priority) {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return int Приоритет группы виджетов
     */
    public function getPriority() {
        return $this->priority;
    }
}
