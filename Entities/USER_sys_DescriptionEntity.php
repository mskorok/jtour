<?php

namespace Jazz\Entities;

/** @Entity @Table(name="user_descriptions_entities") */
class USER_sys_DescriptionEntity extends \Jazz\Entities\ENTITY_sys_Abstract {
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @OneToOne(targetEntity="USER_sys_Entity", mappedBy="description")
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $user;
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $firstName;
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $lastName;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct($firstName = "", $lastName = "") {
        parent::__construct();
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getTitle() {
        return "{$this->getFirstName()} {$this->getLastName()}";
    }

    /**
     * @param string $firstName
     * @return USER_sys_DescriptionEntity
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @return USER_sys_DescriptionEntity
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param \Jazz\Entities\USER_sys_Entity $user
     * @return USER_sys_DescriptionEntity
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \Jazz\Entities\USER_sys_Entity
     */
    public function getUser() {
        return $this->user;
    }

}