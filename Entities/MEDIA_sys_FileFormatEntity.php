<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="file_format_entities")
 * @description типы форматов файлов
 */
class MEDIA_sys_FileFormatEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * @ManyToOne(targetEntity="SYS_sys_TemplateEntity", cascade={"persist"})
     * @var SYS_sys_TemplateEntity
     */
    protected $templateEntity;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @return MEDIA_sys_FileFormatEntity
     */
    function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $name
     * @return GEO_any_TypeEntity ${HINT}
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \Jazz\Entities\SYS_sys_TemplateEntity $templateEntity
     * @return GEO_any_TypeEntity ${HINT}
     */
    public function setTemplateEntity($templateEntity)
    {
        $this->templateEntity = $templateEntity;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_TemplateEntity
     */
    public function getTemplateEntity()
    {
        return $this->templateEntity;
    }


}