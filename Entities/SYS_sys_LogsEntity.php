<?php
namespace Jazz\Entities;
use Jazz, \Jazz\Services\CacheService ;
use Jazz\MVC\Models\SystemEvent;
use Jazz\Other\Utils\DefaultValueService As DefService;
use Jazz\Entities\USER_sys_Entity, DateTime;


/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 30.11.12
 * Time: 5:26
 * To change this template use File | Settings | File Templates.
 *
 * @description класс создающий сущности, логирующие все события, происходящие с сущностями, как то создание, модификация, удаление
 * @Entity
 * @Table(name="logs_entities")
 */
class SYS_sys_LogsEntity extends ENTITY_sys_Abstract{
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события
     */
    const MUTE = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description Идентификатор события. Со стороны вызова используются именованные глобальные константы событий
     * @var integer
     * @Column(type="integer")
     */
    protected $eventId;
    /**
     * @description Время создания события
     * @var \DateTime
     * @Column(type="datetime")
     */
    protected $date;
    /**
     * Идентификатор пользователя создавшего действие
     * @var int
     * @Column(type="integer")
     */
    protected $userInitiatorId;
    /**
     * Title пользователя создавшего действие ( на случай если он будет удален )
     * @var string
     * @Column(type="string")
     */
    protected $userInitiatorTitle;
    /**
     * @description Класс сущности в которой произошло событие
     * @var string
     * @Column(type="string", length=256)
     */
    protected $entityShortClassName;
    /**
     * @description  Идентификатор сущности в которой произошло событие
     * @var null|integer
     * @Column(type="integer")
     */
    protected $entityId;
    /**
     * @description Имя измененного поля в сущности
     * @var null|string
     *  @Column(type="string", length=256, nullable=true)
     */
    protected $fieldName;
    /**
     * @description Старое значение измененного поля в сущности
     * @var null | mixed
     * @Column(nullable=true)
     */
    protected $oldValue;

    /**
     * @description Новое значение измененного поля в сущности
     * @var null | mixed
     * @Column(nullable=true)
     */
    protected $newValue;


    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @param \Jazz\MVC\Models\SystemEvent $event
     */
    function __construct(SystemEvent $event) {
        parent::__construct();

        $this->eventId              = $event->getEventId();
        $this->userInitiatorId      = $event->getUserInitiatorId();
        $this->userInitiatorTitle   = $event->getUserInitiatorTitle();
        $this->date                 = $event->getDate();
        $this->entityId             = $event->getEntityId();
        $this->entityShortClassName = $event->getEntityShortClassName();
        $this->fieldName            = $event->getFieldName();
        $this->oldValue             = $this->convertValueForDb($event->getOldValue());
        $this->newValue             = $this->convertValueForDb($event->getNewValue());

    }

    /**
     * @param \Jazz\Entities\ENTITY_sys_Abstract $entity
     * @return string
     */
    private function convertEntity($entity){
        return array( 'type' => $entity->getShortClassName(), 'id' => $entity->getId() );
    }

    public function convertValueForDb($value){
        if( $value instanceof \Jazz\Entities\ENTITY_sys_Abstract ){
            $value = json_encode($this->convertEntity($value));
        };

        if( is_array($value) || $value instanceof \Doctrine\Common\Collections\ArrayCollection ){
            $res = array(
                'items' => array()
            );
            $dRes = &$res;
            foreach( $value as $val ){
                if( !isset($res['type']) ){
                    $res['type'] = $val->getShortClassName();
                }
                $res['items'][] = $val->getId();
                unset($val);
            }
            $value = json_encode($res);
            unset($dRes, $res);
        }

        return $value;
    }

    public   function getTitle()
    {
        return "LogEntity";
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param int $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int|null $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityShortClassName
     */
    public function setEntityShortClassName($entityShortClassName)
    {
        $this->entityShortClassName = $entityShortClassName;
    }

    /**
     * @return string
     */
    public function getEntityShortClassName()
    {
        return $this->entityShortClassName;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param null|string $fieldName
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return null|string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param $oldValue
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;
    }

    /**
     * @return mixed|null
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @param $newValue
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;
    }

    /**
     * @return mixed|null
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @param int $userInitiatorId
     * @return SYS_sys_LogsEntity
     */
    public function setUserInitiatorId($userInitiatorId) {
        $this->userInitiatorId = $userInitiatorId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserInitiatorId() {
        return $this->userInitiatorId;
    }

    /**
     * @param int $userInitiatorTitle
     * @return SYS_sys_LogsEntity
     */
    public function setUserInitiatorTitle($userInitiatorTitle) {
        $this->userInitiatorTitle = $userInitiatorTitle;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserInitiatorTitle() {
        return $this->userInitiatorTitle;
    }


}
