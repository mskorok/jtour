<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="subscribers_entities")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({ "NOTIF_sys_SubscriberEntity" = "NOTIF_sys_SubscriberEntity", "NOTIF_sys_SubscriberUserEntity" = "NOTIF_sys_SubscriberUserEntity", "NOTIF_sys_SubscriberGroupEntity" = "NOTIF_sys_SubscriberGroupEntity"})
 */
class NOTIF_sys_SubscriberEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToMany(targetEntity="NOTIF_sys_SubscriptionEntity", inversedBy="subscribers", cascade={"persist"})
     * @JoinTable(name="subscriptions_subscribers")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $subscriptions;
    /**
     * Заголовок оповещения
     * @Column(type="string", length=128)
     * @var string
     */
    protected $notificationTitle;
    /**
     * Текст оповещения
     * @Column(type="text")
     * @var string
     */
    protected $notificationDescription;
    /**
     * Пометка что событие важное
     * @Column(type="boolean")
     * @var bool
     */
    protected $important;
    /**
     * @var
     */
    protected $className;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct($className = "") {
        parent::__construct();
        $this->subscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->important     = false;
        $this->className     = $className;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              По сути он используется в EntityGridRowBuilder для отображения Заголовка сущности
     * @return string
     */
    public function getTitle() {
        return $this->getClassName();
    }

    /**
     * @param boolean $important
     * @return NOTIF_sys_SubscriberEntity
     */
    public function setImportant($important) {
        $this->important = $important;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getImportant() {
        return $this->important;
    }

    /**
     * @param string $notificationDescription
     * @return NOTIF_sys_SubscriberEntity
     */
    public function setNotificationDescription($notificationDescription) {
        $this->notificationDescription = $notificationDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotificationDescription() {
        return $this->notificationDescription;
    }

    /**
     * @param string $notificationTitle
     * @return NOTIF_sys_SubscriberEntity
     */
    public function setNotificationTitle($notificationTitle) {
        $this->notificationTitle = $notificationTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotificationTitle() {
        return $this->notificationTitle;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $subscriptions
     * @return NOTIF_sys_SubscriberEntity
     */
    public function setSubscriptions($subscriptions) {
        $this->subscriptions = $subscriptions;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubscriptions() {
        return $this->subscriptions;
    }

    /**
     * @param  $className
     */
    public function setClassName($className)
    {
        $this->className = $className;
    }

    /**
     * @return
     */
    public function getClassName()
    {
        return $this->className;
    }


}