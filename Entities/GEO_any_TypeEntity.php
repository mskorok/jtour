<?php
namespace Jazz\Entities;

 /**
  * @Entity
  * @Table(name="geo_type_entities") Таблица типов GEO-объектов
  * @description Сущность GEO_any_TypeEntity описывает типы GEO-объектов.
  *              Содержит свойства: city-город, region-регион, country-страна, marketingRegion-маркетинговый регион.
  */
class GEO_any_TypeEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * @ManyToOne(targetEntity="SYS_sys_TemplateEntity", cascade={"persist"})
     * @var SYS_sys_TemplateEntity
     */
    protected $templateEntity;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $name
     * @return GEO_any_TypeEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTemplate(){
        return $this->getTemplateEntity()->getTemplate();
    }

    /**
     * @param string $template
     * @return \Jazz\Entities\GEO_any_TypeEntity
     */
    public function setTemplate($template){
        $this->getTemplateEntity()->setTemplate($template);
        return $this;
    }

    /**
     * @param \Jazz\Entities\SYS_sys_TemplateEntity $templateEntity
     * @return GEO_any_TypeEntity
     */
    public function setTemplateEntity($templateEntity) {
        $this->templateEntity = $templateEntity;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_TemplateEntity
     */
    public function getTemplateEntity() {
        return $this->templateEntity;
    }

}