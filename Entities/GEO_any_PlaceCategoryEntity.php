<?php
namespace Jazz\Entities;

 /**
  * Категории доступных гео мест
  * @Entity
  * @Table(name="geo_place_category_entities")
  */
class GEO_any_PlaceCategoryEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * Список данной категории
     * @OneToMany(targetEntity="GEO_any_PlaceEntity", mappedBy="categoryPlace")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $places;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->places = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $name
     * @return GEO_any_TypeEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

}