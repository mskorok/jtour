<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="templates_entities")
 */
class SYS_sys_TemplateEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * @Column(type="text")
     * @var string
     */
    protected $template;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct($name = "", $template = ""){
        parent::__construct();
        $this->setName($name);
        $this->template = $template;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $template
     * @return SYS_sys_TemplateEntity
     */
    public function setTemplate($template) {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * @param string $name
     * @return SYS_sys_TemplateEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

}