<?php
namespace Jazz\Entities;
use DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 19.11.12
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 *
 * @description публичное событие
 *
 * @Entity
 * @Table(name="public_event_entities")
 *
 */
class EVENT_site_PublicEntity extends ENTITY_sys_GeneralAbstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description ссылка на тип события
     * @ManyToOne(targetEntity="EVENT_site_TypeEntity")
     * @JoinColumn(name="event_type_id", referencedColumnName="id")
     * @var \Jazz\Entities\EVENT_site_TypeEntity
     *
     */
    protected $eventType;

    /**
     * @description дата и время начала события
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $openDate;
    /**
     * @description дата и время окончания события
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $closeDate;
    /**
     * @description описание события
     * @Column(type="text")
     * @var string
     */
    protected $description;
    /**
     * @description связь с гео объектом
     * @ManyToOne(targetEntity="GEO_any_TreeEntity", inversedBy="event")
     * @var \Jazz\Entities\GEO_any_TreeEntity
     */
    protected $geo;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /**
     * Метод отвечает за генерацию Url для пользовательской страницы.
     * По умолчанию возвращает пустую строку поэтому именно сущность потомка должна отвечать за то как именно
     * генерировать url.
     * @return string Url для пользовательской страницы
     */
    protected function getUrlForUserPage() {
        $geoUrl = $this->getGeo()->getUserUrl();
        return "{$geoUrl}events/{$this->getId()}/";
    }

    /**
     * @param \DateTime $closeDate
     * @return EVENT_site_PublicEntity
     */
    public function setCloseDate($closeDate) {
        $this->closeDate = $closeDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCloseDate() {
        return $this->closeDate;
    }

    /**
     * @param string $description
     * @return EVENT_site_PublicEntity
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param \Jazz\Entities\EVENT_site_TypeEntity $eventType
     * @return EVENT_site_PublicEntity
     */
    public function setEventType($eventType) {
        $this->eventType = $eventType;
        return $this;
    }

    /**
     * @return \Jazz\Entities\EVENT_site_TypeEntity
     */
    public function getEventType() {
        return $this->eventType;
    }

    /**
     * @param \Jazz\Entities\GEO_any_TreeEntity $geo
     * @return EVENT_site_PublicEntity
     */
    public function setGeo($geo) {
        $this->geo = $geo;
        return $this;
    }

    /**
     * @return \Jazz\Entities\GEO_any_TreeEntity
     */
    public function getGeo() {
        return $this->geo;
    }

    /**
     * @param \DateTime $openDate
     * @return EVENT_site_PublicEntity
     */
    public function setOpenDate($openDate) {
        $this->openDate = $openDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOpenDate() {
        return $this->openDate;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/


}
