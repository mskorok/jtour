<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="subscriptions_entities")
 */
class NOTIF_sys_SubscriptionEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="integer")
     * @var integer
     */
    protected $eventId;
    /**
     * @Column(type="string", length=256)
     * @var string
     */
    protected $subscribeEntityName;
    /**
     * @Column(type="integer", nullable=true)
     * @var integer
     */
    protected $subscribeEntityId;
    /**
     * @Column(type="string", length=256, nullable=true)
     * @var string
     */
    protected $subscribeFieldName;

    /**
     * @ManyToMany(targetEntity="NOTIF_sys_SubscriberEntity", mappedBy="subscriptions", cascade={"persist"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $subscribers;


    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->subscribers          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscribeEntityName  = "";
        $this->subscribeEntityId    = 0;
    }

    /**
     * Данный метод создает экземпляры NOTIF_sys_Entity по списку своих подписчиков
     * @param \Jazz\MVC\Models\SystemEvent $event
     * @throws \Exception
     * @return array
     */
    public function createNotifications($event)
    {
        $this->joinSubscribers();
        $subscriber=$this->getSubscribers();
        $lifecycleNotificationsPull=array();
        foreach($subscriber as $subs)
        {
            /** @var $subs \Jazz\Entities\NOTIF_sys_SubscriberEntity */
            $notification=new \Jazz\Entities\NOTIF_sys_Entity($event, $subs);
            $lifecycleNotificationsPull[]=$notification;
            unset($notification);
        }
        unset($subs);

        return $lifecycleNotificationsPull;
        //throw new \Exception("Нельзя вызывать данный метод у класса родителя");
    }
    public  function  joinSubscribers()
    {
        $application = \Jazz\Application::getInstance();
        $query="SELECT n, a FROM Jazz\Entities\NOTIF_sys_SubscriptionEntity n JOIN n.subscribers a WHERE n.id={$this->getId()}";
        $query=$application->getEntityManager()->createQuery($query);
        $query->getResult();
        return $this;
    }

    /******************************************************************************************************************
     * Get Labels methods / Методы возвращают
     ******************************************************************************************************************/
    protected function getFieldLabelForEventId()                { return "Событие"; }
    protected function getFieldLabelForSubscribeEntityName()    { return "Объект подписки"; }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              По сути он используется в EntityGridRowBuilder для отображения Заголовка сущности
     * @return string
     */
    public function getTitle() {
        $this->subscribeEntityId  ?  $str1=" с идентификатором {$this->subscribeEntityId}" :  $str1="";
        $this->subscribeFieldName ?  $str2=" по полю {$this->subscribeFieldName}"          :  $str2="";
        $title="Подписка на событие {$this->eventId} для сущности {$this->subscribeEntityName}". $str1 . $str2;
        return  $title;
    }

    /**
     * @param string $entityName
     * @return NOTIF_sys_SubscriptionEntity
     */
    public function setSubscribeEntityName($entityName) {
        $this->subscribeEntityName = $entityName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscribeEntityName() {
        return $this->subscribeEntityName;
    }

    /**
     * @param int $eventId
     * @return NOTIF_sys_SubscriptionEntity
     */
    public function setEventId($eventId) {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return int
     */
    public function getEventId() {
        return $this->eventId;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $subscribers
     * @return NOTIF_sys_SubscriptionEntity
     */
    public function setSubscribers($subscribers) {
        $this->subscribers = $subscribers;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubscribers() {
        return $this->subscribers;
    }

    /**
     * @param string $subscribeFieldName
     * @return NOTIF_sys_SubscriptionEntity
     */
    public function setSubscribeFieldName($subscribeFieldName) {
        $this->subscribeFieldName = $subscribeFieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscribeFieldName() {
        return $this->subscribeFieldName;
    }

    /**
     * @param int $subscribeEntityId
     * @return NOTIF_sys_SubscriptionEntity
     */
    public function setSubscribeEntityId($subscribeEntityId) {
        $this->subscribeEntityId = $subscribeEntityId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSubscribeEntityId() {
        return $this->subscribeEntityId;
    }




}