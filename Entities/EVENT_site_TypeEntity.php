<?php
namespace Jazz\Entities;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 20.11.12
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 *
 *  @description тип  события
 *
 * @Entity
 * @Table(name="event_type_entities")
 */
class EVENT_site_TypeEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description ссылка на категорию  события
     * @ManyToOne(targetEntity="EVENT_site_CategoryEntity")
     * @JoinColumn(name="event_id", referencedColumnName="id")
     *
     * @var
     */
    protected $typeCategory;

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param  $typeCategory
     */
    public function setTypeCategory($typeCategory)
    {
        $this->typeCategory = $typeCategory;
    }

    /**
     * @return
     */
    public function getTypeCategory()
    {
        return $this->typeCategory;
    }
}
