<?php
namespace Jazz\Entities;
use  DateTime, \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.10.12
 * Time: 17:49
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс создающий таблицу соответствия сущностей и групповых исключений
 * в соответствии с групповой политикой  доступа
 *
 * @Entity
 * @Table(name="access_group_exceptions_entities")
 *
 */
class ACCESS_sys_RequestorGroupExeptionsEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{


    /**
     * @ManyToOne(targetEntity="USER_sys_GroupEntity")
     * @JoinTable(name="access_group_exceptions",
     * joinColumns={@JoinColumn(name="requestor_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")})
     *
     *
     *  @description массив идентификаторов групп, входящих в исключения
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $groupId;

    /**
     * @description идентификатор пользователя, создавшего исключение(администратора)
     *
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_group_exceptions_creator",
     * joinColumns={@JoinColumn(name="creator_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $creator;
    /**
     * @description Дата и время создания исключения
     * @Column(type="datetime")
     * @var datetime
     */
    protected $creationDate;

    /**
     * @description условия применения политики - разрешить/запретить, показать/скрыть
     * @Column(type="boolean")
     * @var bool
     */
    protected $conditions;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {
        $this->groupId = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creator = new \Doctrine\Common\Collections\ArrayCollection();

    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/







    /**
     * @param $groupId
     * @return ACCESS_sys_RequestorGroupExeptionsEntity
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGroupId()
    {
        return $this->groupId;
    }


    /**
     * @param $creator
     * @return ACCESS_sys_RequestorGroupExeptionsEntity
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return ACCESS_sys_RequestorGroupExeptionsEntity
     */
    public function setCreationDate()
    {
        $this->creationDate = new \DateTime("now");
        return $this;
    }
    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param boolean $conditions
     * @return ACCESS_sys_RequestorGroupExeptionsEntity ${HINT}
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getConditions()
    {
        return $this->conditions;
    }

}
