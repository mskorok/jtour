<?php
namespace Jazz\Entities;
use Jazz, Jazz\Services\CacheService ;
use Jazz\Other\Utils\DefaultValueService As DefService;
use Jazz\Entities\USER_sys_Entity,  DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 30.11.12
 * Time: 6:28
 * To change this template use File | Settings | File Templates.
 * @description класс создающий нотификации
 * @Entity
 * @Table(name="notifications_entities")
 */
class NOTIF_sys_Entity extends ENTITY_sys_Abstract
{
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события изменения
     */
    const MUTE_UPDATE = true;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события изменения полей
     */
    const MUTE_UPDATE_FIELD = true;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события удаления
     */
    const MUTE_REMOVE = true;
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description Идентификатор события. Со стороны вызова используются именованные глобальные константы событий
     * @var integer
     * @Column(type="integer")
     */
    protected $eventId;
    /**
     * @description Время создания события
     * @var \DateTime
     * @Column(type="datetime")
     */
    protected $date;
    /**
     * @description Пользователь иниициатор - действия которого привели к созданию события
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinColumn(name="initiator_id", referencedColumnName="id")
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $userInitiator;
    /**
     * @description подписчик на событие
     * @ManyToOne(targetEntity="NOTIF_sys_SubscriberEntity")
     * @JoinColumn(name="subscriber_id", referencedColumnName="id")
     * @var
     */
    protected $subscriber;

    /**
     * @description Класс сущности в которой произошло событие
     * @var string
     * @Column(type="string", length=256)
     */
    protected $entityShortClassName;
    /**
     * @description  Идентификатор сущности в которой произошло событие
     * @var null|integer
     * @Column(type="integer")
     */
    protected $entityId;
    /**
     * @description Имя измененного поля в сущности
     * @var null|string
     *  @Column(type="string", length=256, nullable=true)
     */
    protected $fieldName;
    /**
     * @description Старое значение измененного поля в сущности
     * @var mixed
     * @Column(nullable=true)
     */
    protected $oldValue;

    /**
     * @description Новое значение измененного поля в сущности
     * @var mixed
     * @Column(nullable=true)
     */
    protected $newValue;
    /**
     * @description Флаг определяющий прочитана ли нотификация
     * @var bool
     */
    protected $hasRead;


    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @param \Jazz\MVC\Models\SystemEvent $event
     * @param NOTIF_sys_SubscriberEntity $subscriber
     */
    function __construct(Jazz\MVC\Models\SystemEvent $event, \Jazz\Entities\NOTIF_sys_SubscriberEntity $subscriber) {
        parent::__construct();
        $this->eventId              = $event->getEventId();
        $this->userInitiator        = $event->getUserInitiator();
        $this->subscriber           = $subscriber;
        $this->date                 = $event->getDate();
        $this->entityId             = $event->getEntityId();
        $this->entityShortClassName = $event->getEntityShortClassName();
        $this->fieldName            = $event->getFieldName();
        $this->oldValue             = $event->getOldValue();
        $this->newValue             = $event->getNewValue();
        $this->hasRead              = false;
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    public function getTitle()
    {
        return $this->getSubscriber()->getNotificationTitle();
    }
    public function getDescription()
    {
        return $this->getSubscriber()->getNotificationDescription();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param int $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int|null $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityShortClassName
     */
    public function setEntityShortClassName($entityShortClassName)
    {
        $this->entityShortClassName = $entityShortClassName;
    }

    /**
     * @return string
     */
    public function getEntityShortClassName()
    {
        return $this->entityShortClassName;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Jazz\Entities\USER_sys_Entity $userInitiator
     */
    public function setUserInitiator($userInitiator)
    {
        $this->userInitiator = $userInitiator;
    }

    /**
     * @return \Jazz\Entities\USER_sys_Entity
     */
    public function getUserInitiator()
    {
        return $this->userInitiator;
    }

    /**
     * @param null|string $fieldName
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return null|string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param $oldValue
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;
    }

    /**
     * @return mixed|null
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @param $newValue
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;
    }

    /**
     * @return mixed|null
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @param $subscriber
     */
    public function setSubscriber($subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @return NOTIF_sys_SubscriberEntity
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }





}
