<?php
namespace Jazz\Entities;

 /**
  * @Entity
  * @Table(name="html_widgets_entities")
  */
class WIDGET_sys_HtmlEntity extends \Jazz\Entities\WIDGET_any_Entity{
    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/
    const HTML = "";

    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="text")
     * @var string
     */
    protected $html;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->html = self::HTML;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $html
     * @return WIDGET_sys_HtmlEntity
     */
    public function setHtml($html) {
        $this->html = $html;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtml() {
        return $this->html;
    }

}