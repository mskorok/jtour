<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 29.08.12
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;

/**
 * @MappedSuperclass
 * @description От данного класса должны наследоваться все простые сущности - конфиги, списки и т.д.
 */
abstract class ENTITY_sys_SimpleAbstract extends ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=256)
     * @var string
     */
    protected $title;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $title
     * @return ENTITY_sys_GeneralAbstract
     */
    public function setTitle($title) {
        $this->setTranslateString('title', $title);
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->getTranslateString('title');
    }

}
