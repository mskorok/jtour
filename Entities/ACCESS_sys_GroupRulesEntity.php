<?php

namespace Jazz\Entities;
use   DateTime,  \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 18.10.12
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс описывает групповые правила доступа
 *
 *
 * @Entity
 * @Table(name="access_group_rules_entities")
 */
class ACCESS_sys_GroupRulesEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /**
     * @Column(type="string", length=64, unique=true)
     * @var string
     */
    protected $groupRules;
    /**
     * @description идентификатор пользователя, создавшего правило(администратора)
     *
     * @ManyToMany(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_group_rules_creator",
     * joinColumns={@JoinColumn(name="creator_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $creator;
    /**
     * @description Дата и время создания правила
     *
     *
     * @Column(type="datetime")
     * @var datetime
     */
    protected $creationDate;

    /**
     * @description свойство, определяющее необходимость использовать исключения
     *
     * @Column(type="boolean")
     * @var bool
     */
    protected $isHaveExceptions;

    /**
     * @description свойство, определяющее приоритет групповых правил над индивидуальными
     *
     * @Column(type="boolean")
     * @var bool
     */
    protected $inversRules;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {

        $this->creator = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isHaveExceptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inversRules=false;
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param $groupRules
     */
    public function setGroupRules($groupRules)
    {
        $this->groupRules = $groupRules;
    }

    /**
     * @return string
     */
    public function getGroupRules()
    {
        return $this->groupRules;
    }





    /**
     * @param $creator
     * @return ACCESS_sys_GroupRulesEntity
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return ACCESS_sys_GroupRulesEntity
     */
    public function setCreationDate()
    {
        $this->creationDate = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param $isHaveExceptions
     * @return ACCESS_sys_GroupRulesEntity
     */
    public function setIsHaveExceptions($isHaveExceptions)
    {
        $this->isHaveExceptions = $isHaveExceptions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsHaveExceptions()
    {
        return $this->isHaveExceptions;
    }

    /**
     * @param boolean $inversRules
     * @return ACCESS_sys_GroupRulesEntity ${HINT}
     */
    public function setInversRules($inversRules)
    {
        $this->inversRules = $inversRules;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getInversRules()
    {
        return $this->inversRules;
    }
}
