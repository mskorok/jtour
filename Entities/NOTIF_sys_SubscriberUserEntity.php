<?php
namespace Jazz\Entities;

/**
 * @Entity
 */
class NOTIF_sys_SubscriberUserEntity extends \Jazz\Entities\NOTIF_sys_SubscriberEntity{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="USER_sys_Entity", inversedBy="subscribers", cascade={"persist"})
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $user;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              По сути он используется в EntityGridRowBuilder для отображения Заголовка сущности
     * @return string
     */
    public function getTitle() {
        return $this->getNotificationTitle() . $this->getUser()->getTitle();
    }

    /**
     * @param \Jazz\Entities\USER_sys_Entity $user
     * @return NOTIF_sys_SubscriberUserEntity
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \Jazz\Entities\USER_sys_Entity
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getUserTitle() {
        return $this->getUser()->getTitle();
    }


}