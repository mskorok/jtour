<?php
namespace Jazz\Entities;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="sections_entities")
 */
class PAGE_sys_SectionEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=256)
     * @description System title - text to be displayed in the grid in the administrations interfaces
     * @description Системное название - текст который будет отображаться в гриде в админке
     * @var string Системное название
     */
    protected $title;
    /**
     * @OneToMany(targetEntity="WIDGET_any_Entity", mappedBy="section", cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $widgets;
    /**
     * @ManyToOne(targetEntity="PAGE_sys_Entity", inversedBy="sections", cascade={"persist"})
     * @var \Jazz\Entities\PAGE_sys_Entity
     */
    protected $page;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->widgets = new ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $widgets
     * @return PAGE_sys_SectionEntity
     */
    public function setWidgets($widgets) {
        $this->widgets = $widgets;
        return $this;
    }

    /**
     * @param \Jazz\Entities\WIDGET_any_Entity $widget
     * @return PAGE_sys_SectionEntity
     */
    public function addWidget($widget) {
        $widget->setSection($this);
        $this->widgets->add($widget);
        return $this;
    }

    /**
     * @param int $key
     * @return PAGE_sys_SectionEntity
     */
    public function removeWidget($key) {
        /**
         * @var $widget \Jazz\Entities\WIDGET_any_Entity
         */
        $widget = $this->widgets->get($key);
        $this->widgets->remove($key);
        $widget->remove();

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getWidgets() {
        return $this->widgets;
    }

    /**
     * @param string $title
     * @return PAGE_sys_SectionEntity
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param \Jazz\Entities\PAGE_sys_Entity $page
     * @return PAGE_sys_SectionEntity
     */
    public function setPage($page) {
        $this->page = $page;
        return $this;
    }

    /**
     * @return \Jazz\Entities\PAGE_sys_Entity
     */
    public function getPage() {
        return $this->page;
    }
}