<?php
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена
 */
namespace Jazz\Entities;
    /**
     * @description Таблица документов
     * @Entity
     * @Table(name="media_file_document_entities")
     */
class MEDIA_sys_FileDocumentEntity extends \Jazz\Entities\MEDIA_sys_LibraryEntity{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @return  MEDIA_sys_FileDocumentEntity
     */
    public function __construct() {
        parent::__construct();
    }


}
