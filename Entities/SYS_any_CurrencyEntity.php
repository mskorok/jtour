<?php
/**
 * Created by IntelliJ IDEA.
 */
namespace Jazz\Entities;
/** @Entity
 *  @Table(name="currency_entities") Таблица валюты
 *  @description Cущность SYS_any_CurrencyEntity описывает валюту.
 *
 *               Содержит следующие свойства:
 *                  $symbol- знак валюты,
 *                  $shortName - короткое имя согласно стандарту ISO 4217, которое сосотоит из 3-х символов,
 */
class SYS_any_CurrencyEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=4) символ валюты
     * @var string
     */
    protected $symbol;
    /**
     * @Column(type="string", length=3) короткое имя согласно ISO 4217
     * @var string
     */
    protected $shortName;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct($symbol = "", $name = "") {
        parent::__construct();
        $this->symbol = $symbol;
        $this->shortName = $name;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param $shortName
     * @return SYS_any_CurrencyEntity
     */
    public function setShortName($shortName){
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortName(){
        return $this->shortName;
    }

    /**
     * @param string $symbol
     * @return SYS_any_CurrencyEntity
     */
    public function setSymbol($symbol){
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(){
        return $this->symbol;
    }
}