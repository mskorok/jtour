<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="metakeywords_entities")
 */
class PAGE_seo_MetaKeywordEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string")
     * @var string
     */
    protected $metaKeyword;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct($symbol = "") {
        parent::__construct();
        $this->symbol = $symbol;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getTitle() {
        return $this->getMetaKeyword();
    }

    /**
     * @param string $metaKeyword
     * @return PAGE_seo_MetaKeywordEntity
     */
    public function setMetaKeyword($metaKeyword) {
        $this->metaKeyword = $metaKeyword;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeyword() {
        return $this->metaKeyword;
    }
}