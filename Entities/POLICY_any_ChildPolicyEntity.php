<?php
namespace Jazz\Entities;
/**
 * @class \Jazz\Entities\POLICY_any_ChildPolicyEntity
 * @date: 06.01.13 - 18:23
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @Entity
 * @Table(name="policy_child_policy_entities")
 */
class POLICY_any_ChildPolicyEntity extends ENTITY_sys_SimpleAbstract {
    /**
     * @OneToMany(targetEntity="POLICY_any_ChildAgesEntity", mappedBy="policy")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $children;

    function __construct() {
        parent::__construct();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Additional methods
     *****************************************************************************************************************/

    /******************************************************************************************************************
     * Getter / Setter
     *****************************************************************************************************************/
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $children
     * @return POLICY_any_ChildPolicyEntity
     */
    public function setChildren($children) {
        $this->children = $children;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

}
