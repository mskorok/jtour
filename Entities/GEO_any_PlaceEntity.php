<?php
namespace Jazz\Entities;

 /**
  * Таблица гео мест ( магазины, рестораны, и т.д. )
  * @Entity
  * @Table(name="geo_place_entities")
  */
class GEO_any_PlaceEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * Категория данного места
     * @ManyToOne(targetEntity="GEO_any_PlaceCategoryEntity", inversedBy="places")
     * @var GEO_any_PlaceCategoryEntity
     */
    protected $categoryPlace;
    /**
     * Места, которым принадлежат достопримечательности
     * @ManyToMany(targetEntity="GEO_any_TreeEntity", inversedBy="places")
     * @JoinTable(name="geo_places",
     * joinColumns={@JoinColumn(name="places_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="geo_id", referencedColumnName="id", unique=true)}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $geo;
    /**
     * @Column(type="text")
     * @var string
     */
    protected $description;


    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->geo   = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param \Jazz\Entities\GEO_any_PlaceCategoryEntity $categoryPlace
     * @return GEO_any_PlaceEntity
     */
    public function setCategoryPlace($categoryPlace) {
        $this->categoryPlace = $categoryPlace;
        return $this;
    }

    /**
     * @return \Jazz\Entities\GEO_any_PlaceCategoryEntity
     */
    public function getCategoryPlace() {
        return $this->categoryPlace;
    }



    /**
     * @param string $description
     * @return GEO_any_PlaceEntity
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $geo
     */
    public function setGeo($geo)
    {
        $this->geo = $geo;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGeo()
    {
        return $this->geo;
    }

}