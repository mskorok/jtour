<?php
/**
 * Created by IntelliJ IDEA.
 * User: Наумова Елена
 */
namespace Jazz\Entities;

/**
 * @description Таблица аудио-файлов
 * @Entity
 * @Table(name="media_file_audio_entities")
 */
class MEDIA_sys_FileAudioEntity extends \Jazz\Entities\MEDIA_sys_LibraryEntity{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/

    /**
     * @description Разрешение аудио-файла
     * @var string
     * @Column(type="string")
     */
    protected $resolution;
    /**
     * @description Ссылка на внешний источник
     * @Column(type="text", length=128)
     * @var string
     */
    protected $externalUrl;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    /**
     * @return MEDIA_sys_FileAudioEntity
     */
    public function __construct() {
        parent::__construct();
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    public function setExternalUrl($externalUrl)
    {
        $this->externalUrl = $externalUrl;
        return $this;
    }

    public function getExternalUrl()
    {
        return $this->externalUrl;
    }

    /**
     * @param string $resolution
     * @return MEDIA_sys_FileAudioEntity ${HINT}
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
        return $this;
    }

    /**
     * @return string
     */
    public function getResolution()
    {
        return $this->resolution;
    }
}
