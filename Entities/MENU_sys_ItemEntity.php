<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="menu_items_entities")
 */
class MENU_sys_ItemEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/
    /**
     * @description Значение по умолчанию от куда берем url
     */
    const USE_CUSTOM_URL = false;
    /**
     * @description Значение по умолчанию от куда берем title
     */
    const USE_CUSTOM_TITLE = false;

    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="SYS_sys_PackageEntity")
     * @description Указатель на пакет
     * @var \Jazz\Entities\SYS_sys_PackageEntity
     */
    protected $package;
    /**
     * @ManyToOne(targetEntity="MENU_sys_ItemEntity", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @description Указатель на родительский пункт меню
     * @var \Jazz\Entities\MENU_sys_ItemEntity
     */
    protected $parent;
    /**
     * @OneToMany(targetEntity="MENU_sys_ItemEntity", mappedBy="parent")
     * @description Коллекция дочерних пунктов меню
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $children;
    /**
     * @ManyToOne(targetEntity="PAGE_sys_Entity", inversedBy="menuItems",cascade={"persist"})
     * @JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     * @description Указатель на связанную сущность типа PAGE_sys_Entity
     *              Связь Многие к Одному односторонняя:
     *                  - один пункт меню может быть связанн только с одной PAGE_sys_Entity
     * @var \Jazz\Entities\PAGE_sys_Entity
     */
    protected $page;
    /**
     * @Column(type="boolean", nullable=true)
     * @description Флаг указатель на то должен ли данный пункт меню проиндексированн поисковиком,
     *              либо его нужно скрыть от поискового робота путем вывода его только через Javascript
     * @var bool
     */
    protected $renderForJS;
    /**
     * @Column(type="boolean", nullable=true)
     * @description Флаг указатель какой url использовать для перехода - тот что указан в свойстве customUrl или брать
     *              из прилинкованного компонента PAGE_sys_Entity->getUrl().
     *              По умолчанию установленн в false - то есть берем из PAGE_sys_Entity
     * @var bool
     */
    protected $useCustomUrl;
    /**
     * @Column(type="boolean", nullable=true)
     * @description Флаг указатель какой title использовать текста в ссылке - тот что указан в свойстве customTitle или
     *              брать из прилинкованного компонента PAGE_sys_Entity->getTitle().
     *              По умолчанию установленн в false - то есть берем из PAGE_sys_Entity
     * @var bool
     */
    protected $useCustomTitle;
    /**
     * @Column(type="string", length=128, nullable=true)
     * @description системное имя пункта меню, не обязательное, по умолчанию пустое
     * @var string
     */
    protected $name;
    /**
     * @Column(type="string", length=512, nullable=true)
     * @description Произвольный url. Основное использование - создание внешних ссылок. Например menuItem со ссылкой
     *              на страницу http://google.ru/
     * @var string
     */
    protected $customUrl;
    /**
     * @Column(type="string", length=128, nullable=true)
     * @description Произвольный title. Основное использование - создание текста отличающегося от текста в PAGE_sys_Entity.
     * @var string
     */
    protected $customTitle;
    /**
     * @var bool
     */
    protected $isActive;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        $this->children         = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customUrl        = "";
        $this->customTitle      = "";
        $this->useCustomTitle   = static::USE_CUSTOM_TITLE;
        $this->useCustomUrl     = static::USE_CUSTOM_URL;
        $this->name             = "";
        $this->isActive         = false;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Собственно если есть произвольный Title то используем его иначе берем из PAGE_sys_Entity.
     *              По умолчанию берем из PAGE_sys_Entity
     * @return string
     */
    public function getTitle() {
        return $this->getUseCustomTitle() ? $this->getCustomTitle() : $this->getPage()->getTitle();
    }

    /**
     * @description Собственно если есть произвольный Url то используем его иначе берем из PAGE_sys_Entity.
     *              По умолчанию берем из PAGE_sys_Entity
     * @return string
     */
    public function getUrl() {
        return $this->getUseCustomUrl() ? $this->getCustomUrl() : $this->getPage()->getUrl();
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $children
     * @return MENU_sys_ItemEntity
     */
    public function setChildren($children) {
        $this->children = $children;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * @param string $customTitle
     * @return MENU_sys_ItemEntity
     */
    public function setCustomTitle($customTitle) {
        $this->customTitle = $customTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomTitle() {
        return $this->customTitle;
    }

    /**
     * @param string $customUrl
     * @return MENU_sys_ItemEntity
     */
    public function setCustomUrl($customUrl) {
        $this->customUrl = $customUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomUrl() {
        return $this->customUrl;
    }

    /**
     * @param \Jazz\Entities\PAGE_sys_Entity $page
     * @return MENU_sys_ItemEntity
     */
    public function setPage($page) {
        $this->page = $page;
        return $this;
    }

    /**
     * @return \Jazz\Entities\PAGE_sys_Entity
     */
    public function getPage() {
        return $this->page;
    }

    /**
     * @param \Jazz\Entities\MENU_sys_ItemEntity $parent
     * @return MENU_sys_ItemEntity
     */
    public function setParent($parent) {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return \Jazz\Entities\MENU_sys_ItemEntity
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @param boolean $renderForJS
     * @return MENU_sys_ItemEntity
     */
    public function setRenderForJS($renderForJS) {
        $this->renderForJS = $renderForJS;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRenderForJS() {
        return $this->renderForJS;
    }

    /**
     * @param boolean $useCustomTitle
     * @return MENU_sys_ItemEntity
     */
    public function setUseCustomTitle($useCustomTitle) {
        $this->useCustomTitle = $useCustomTitle;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getUseCustomTitle() {
        return $this->useCustomTitle;
    }

    /**
     * @param boolean $useCustomUrl
     * @return MENU_sys_ItemEntity
     */
    public function setUseCustomUrl($useCustomUrl) {
        $this->useCustomUrl = $useCustomUrl;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getUseCustomUrl() {
        return $this->useCustomUrl;
    }

    /**
     * @param string $name
     * @return MENU_sys_ItemEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param \Jazz\Entities\SYS_sys_PackageEntity $package
     * @return MENU_sys_ItemEntity
     */
    public function setPackage($package) {
        $this->package = $package;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_PackageEntity
     */
    public function getPackage() {
        return $this->package;
    }

    /**
     * @param boolean $isActive
     * @return MENU_sys_ItemEntity
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive() {
        return $this->isActive;
    }

}