<?php
namespace Jazz\Entities;
/**
 * @class POLICY_any_ChildPolicyEntity
 * @date: 06.01.13 - 18:05
 * @author Константин "Konstantin.R.Dark" Родионов ( Проколенко ) Konstantin.R.Dark@gmail.com
 * @Entity
 * @Table(name="sys_int_range_entities")
 */
class SYS_sys_IntRangeEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /**
     * Минимальное ограничение
     * @Column(type="integer")
     * @var int
     */
    protected $min;
    /**
     * Максимальное ограничение
     * @Column(type="integer")
     * @var int
     */
    protected $max;

    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle() {
        return $this->getMin() . ' - ' . $this->getMax();
    }

    /**
     * @param int $max
     * @return SYS_sys_IntRangeEntity
     */
    public function setMax($max) {
        $this->max = $max;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax() {
        return $this->max;
    }

    /**
     * @param int $min
     * @return SYS_sys_IntRangeEntity
     */
    public function setMin($min) {
        $this->min = $min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMin() {
        return $this->min;
    }
}
