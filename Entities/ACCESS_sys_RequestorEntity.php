<?php
namespace Jazz\Entities;
use    \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 08.12.12
 * Time: 23:55
 * To change this template use File | Settings | File Templates.
 *
 * @description
 *
 *
 * @Entity
 * @Table(name="access_requestor_entities")
 *
 */
class ACCESS_sys_RequestorEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /**
     * @var
     * @Column(type="string", length=255, unique=true)
     */
    private $identity;
    /**
     * @var
     * @description
     * @OneToMany(targetEntity="ACCESS_sys_RequestorPrivateRulesEntity", onDelete="cascade")
     * @JoinColumn(name="privateRules_id", referencedColumnName="id")
     *
     */
    private $privateRules;
    /**
     * @var
     * @description
     * @OneToMany(targetEntity="ACCESS_sys_RequestorGroupRulesEntity", onDelete="cascade")
     * @JoinColumn(name="groupRules_id", referencedColumnName="id")
     */
    private $groupRules;



    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {

        $this->privateRules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupRules = new \Doctrine\Common\Collections\ArrayCollection();



    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param  $privateRules
     */
    public function setPrivateRules($privateRules)
    {
        $this->privateRules = $privateRules;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPrivateRules()
    {
        return $this->privateRules;
    }

    /**
     * @param  $groupRules
     */
    public function setGroupRules($groupRules)
    {
        $this->groupRules = $groupRules;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGroupRules()
    {
        return $this->groupRules;
    }


    /**
     * @param  $identity
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
    }

    /**
     * @return
     */
    public function getIdentity()
    {
        return $this->identity;
    }


}
