<?php
namespace Jazz\Entities;
use  DateTime, \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 18.10.12
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс создающий таблицу соответствия сущностей с персональными
 * политиками доступа
 *
 * @Entity
 * @Table(name="access_requestor_private_entities")
 *
 */
class ACCESS_sys_RequestorPrivateRulesEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{


    /**
     * @description Политики доступа - видимости, редактирования, создания, удаления
     *
     * @ManyToMany(targetEntity="ACCESS_sys_PolicyEntity")
     * @JoinTable(name="access_requestor_private_policy",
     * joinColumns={@JoinColumn(name="requestor_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="policy_id", referencedColumnName="id")})
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $policy;



    /**
     * @description идентификатор доступа к таблице определяющей индивидуальные
     * правила доступа для текущего реквестора при одном и том же уровне приоритета использования правил
     *
     *
     * @ManyToOne(targetEntity="ACCESS_sys_PrivateRulesEntity")
     * @JoinTable(name="access_requestor_private_rules",
     * joinColumns={@JoinColumn(name="requestor_private_rules_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="private_rules_id", referencedColumnName="id")})
     *
     *
     * @var  \Doctrine\Common\Collections\ArrayCollection
     */
    protected $privateRules;

    /**
     * @description условия применения политики - разрешить/запретить, показать/скрыть
     * @Column(type="boolean")
     * @var bool
     */
    protected $conditions;
    /**
     *
     * @description приоритет применения политики 1- применяется в первую очередь, потом 2,3 и т.д.
     * @Column(type="integer")
     * @var int
     */
    protected $priority;
    /**
     * @description идентификатор пользователя, создавшего исключение(администратора)
     *
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_requestor_private_creator",
     * joinColumns={@JoinColumn(name="creator_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $creator;
    /**
     * @description Дата и время создания исключения
     * @Column(type="datetime")
     * @var datetime
     */
    protected $creationDate;
    /**
     * @var
     * @description
     * @OneToMany(targetEntity="ACCESS_sys_RequestorPrivateExceptionsEntity", onDelete="cascade")
     * @JoinColumn(name="privateExceptions_id", referencedColumnName="id")
     */
    private $exceptions;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {
        parent::__construct();
        $this->privateRules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->policy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->exceptions = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param  boolean $conditions
     * @return ACCESS_sys_RequestorPrivateRulesEntity
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $policy
     * @return ACCESS_sys_RequestorPrivateRulesEntity
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPolicy()
    {
        return $this->policy;
    }


    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $privateRules
     */
    public function setPrivateRules($privateRules)
    {
        $this->privateRules = $privateRules;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPrivateRules()
    {
        return $this->privateRules;
    }


    /**
     * @param int $priority
     * @return ACCESS_sys_RequestorPrivateRulesEntity ${HINT}
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }



    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $creator
     * @return ACCESS_sys_RequestorPrivateRulesEntity ${HINT}
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return ACCESS_sys_RequestorPrivateRulesEntity
     */
    public function setCreationDate()
    {
        $this->creationDate = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param  $exceptions
     */
    public function setExceptions($exceptions)
    {
        $this->exceptions = $exceptions;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }

}
