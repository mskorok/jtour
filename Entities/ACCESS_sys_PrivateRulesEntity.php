<?php

namespace Jazz\Entities;
use   DateTime,  \Jazz\Entities\ENTITY_sys_SimpleAbstract, \Doctrine\Common\Collections\ArrayCollection;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 18.10.12
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 *
 * @description Класс описывает персональные правила доступа
 *
 *
 * @Entity
 * @Table(name="access_private_rules_entities")
 *
 */
class ACCESS_sys_PrivateRulesEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract
{
    /**
     * @Column(type="string", length=64, unique=true)
     * @var string
     */
    protected $privateRules;
    /**
     * @description идентификатор пользователя, создавшего правило(администратора)
     *
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinTable(name="access_private_rules_creator",
     * joinColumns={@JoinColumn(name="creator_id", referencedColumnName="id",
     * onDelete="cascade")},
     * inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $creator;
    /**
     * @description Дата и время создания правила
     * @Column(type="datetime")
     * @var datetime
     */
    protected $creationDate;
    /**
     * @description свойство, определяющее необходимость использовать исключения
     * @Column(type="boolean")
     * @var bool
     */
    protected $isHaveExceptions;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct()
    {

        $this->isHaveExceptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creator = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param $privaterules
     * @return ACCESS_sys_PrivateRulesEntity
     */
    public function setPrivateRules($privaterules)
    {
        $this->privateRules = $privaterules;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrivateRules()
    {
        return $this->privateRules;
    }

    /**
     * @param $creator
     * @return ACCESS_sys_PrivateRulesEntity
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return ACCESS_sys_PrivateRulesEntity
     */
    public function setCreationDate()
    {
        $this->creationDate =  new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param $isHaveExceptions
     * @return ACCESS_sys_PrivateRulesEntity
     */
    public function setIsHaveExceptions($isHaveExceptions)
    {
        $this->isHaveExceptions = $isHaveExceptions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsHaveExceptions()
    {
        return $this->isHaveExceptions;
    }

}
