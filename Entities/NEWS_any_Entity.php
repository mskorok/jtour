<?php
namespace Jazz\Entities;
use Jazz\Other\Utils\DefaultValueService As DefService;
use Jazz\Entities\USER_sys_Entity, DateTime;

/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 13.11.12
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 *
 * @description сущность новость
 *
 * @Entity
 * @Table(name="news_entities")
 */
class NEWS_any_Entity extends ENTITY_sys_GeneralAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     *  @description ссылка на пользователя который добавил новость
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     *
     * TODO определить механизм если автор неизвестен
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $author;
    /**
     * @description флаг, определяющий нужно ли использовать внешний источник по умолчанию
     * @Column(type="boolean")
     * @var bool
     *
     */
    protected $extSource;
    /**
     * @description флаг, определяющий нужно ли использовать имя внешнего источника
     * @Column(type="boolean")
     * @var bool
     *
     */
    protected $extText;
    /**
     * @description ссылка на внешний источник
     * @Column(type="string", length=255)
     * @var string
     */
    protected $extUrl;
    /**
     * @description текстовое название внешнего источника
     *
     * @Column(type="string", length=255)
     * @var string
     */
    protected $extTextName;
    /**
     * @description дата и время создания новости
     * @Column(type="datetime")
     *
     * @var datetime
     */
    protected $creationDate;
    /**
     * @description текст preview
     * @Column(type="string", length=255)
     * @var string
     */
    protected $preview;

    /**
     * @description указатель пакета
     *
     * @ManyToMany(targetEntity="SYS_sys_PackageEntity")
     * @JoinTable(name="news_packages",
     * joinColumns={@JoinColumn(name="news_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="package_id", referencedColumnName="id", unique=false)}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $sysPackages;

    /**
     * @description основной текст
     * @Column(type="text")
     * @var
     *
     */
    protected $textNews;
    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->extSource        = false;
        $this->extText          = false;
        $this->extUrl           = "";
        $this->extTextName      = "";
        $this->creationDate     = new \DateTime("now");
        $this->sysPackages      = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /**
     * Метод отвечает за генерацию Url для пользовательской страницы.
     * По умолчанию возвращает пустую строку поэтому именно сущность потомка должна отвечать за то как именно
     * генерировать url.
     * @return string Url для пользовательской страницы
     */
    protected function getUrlForUserPage() {
        return "/news/{$this->getID()}/";
    }


    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @return string
     */
    public function getExtSourceTitle() {
        return $this->getExtSource() ? $this->getExtTextName() : "";
    }

    /**
     * @return string
     */
    public function getExtSourceUrl() {
        return $this->getExtSource() ? $this->getExtUrl() : "#";
    }

    /**
     * @return string
     */
    public function getAuthorTitle() {
        return $this->getExtSource() ? $this->getExtTextName() : $this->getAuthor()->getTitle();
    }


    /**
     * @return string
     */
    public function getAuthorUrl() {
        $domain = \Jazz\Application::getInstance()->getUserDomain();
        // ToDO не ясно как брать ссылку на страницу автора
        return $this->getExtSource() ? $this->getExtUrl() : "{$domain->getHref()}/#";
    }


    /**
     * @param $author
     * @return NEWS_any_Entity
     */
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

    /**
     * @return USER_sys_Entity
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @return NEWS_any_Entity
     */
    public function setCreationDate() {
        $this->creationDate = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate() {
        return $this->creationDate;
    }

    /**
     * @param boolean $extSource
     * @return NEWS_any_Entity
     */
    public function setExtSource($extSource) {
        $this->extSource = $extSource;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getExtSource() {
        return $this->extSource;
    }

    /**
     * @param string $extTextName
     * @return NEWS_any_Entity
     */
    public function setExtTextName($extTextName) {
        $this->extTextName = $extTextName;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtTextName() {
        return $this->extTextName;
    }

    /**
     * @param string $extUrl
     * @return NEWS_any_Entity
     */
    public function setExtUrl($extUrl) {
        $this->extUrl = $extUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtUrl() {
        return $this->extUrl;
    }

    /**
     * @param string $preview
     * @return NEWS_any_Entity
     */
    public function setPreview($preview) {
        $this->preview = $preview;
        return $this;
    }

    /**
     * @return string
     */
    public function getPreview() {
        return $this->preview;
    }

    /**
     * @param boolean $extText
     * @return NEWS_any_Entity
     */
    public function setExtText($extText) {
        $this->extText = $extText;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getExtText() {
        return $this->extText;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $sysPackages
     * @return NEWS_any_Entity
     */
    public function setSysPackages($sysPackages) {
        $this->sysPackages = $sysPackages;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSysPackages() {
        return $this->sysPackages;
    }

    /**
     * @param  $textNews
     * @return NEWS_any_Entity
     */
    public function setTextNews($textNews) {
        $this->textNews = $textNews;
        return $this;
    }

    /**
     * @return
     */
    public function getTextNews() {
        return $this->textNews;
    }
}
