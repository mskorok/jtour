<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 28.08.12
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;

interface ENTITY_sys_Interface {


    /**
     * @abstract
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              Он вызывается для показа клиенту заголовка сущности.
     * @return string
     */
    public function getTitle();

    /**
     * @abstract
     * @description Отвечает за механизм сохранения ( update ) сущности и всех связанных с ней.
     *              Реализует паттерн "Шаблонный метод".
     *              Перед вызовом flush сначала вызывает метод doSave()
     * @param array $data
     */
    public function save($data);

    /**
     * @abstract
     * @description Отвечает за механизм удаления сущности и всех связанных с ней из базы данных.
     *              Реализует паттерн "Шаблонный метод"
     */
    public function remove();

}
