<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 29.08.12
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
use Jazz\Other\Utils\DefaultValueService As DefService;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 * @description От данного класса должны наследоваться все сущности которым нужна страница
 */
abstract class ENTITY_sys_GeneralAbstract extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/
    const READY_FOR_USE = false;

    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="PAGE_site_UserEntity", cascade={"persist"})
     * @JoinColumn(name="user_page_id", referencedColumnName="id", nullable=true)
     * @var \Jazz\Entities\PAGE_site_UserEntity
     */
    protected $userPage;
    /**
     * @Column(type="boolean", nullable=true)
     * @description Флаг указатель говорит о том что данная страница уже корректно заполенна,
     *              проверена и может быть доступна не авторизированным пользователям.
     *              Собственно будет специальный человек который будет следить за заполненностью и ставить данный указатель.
     * @var bool
     */
    protected $readyToUse;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->readyToUse = self::READY_FOR_USE;
    }

    /**
     * @PostPersist
     */
    public function doCreateUserPage(){
        $application = \Jazz\Application::getInstance();

        $userPage = new PAGE_site_UserEntity();
        $userPage->save(array(
            'title'             => $this->getTitleForUserPage(),
            'metaTitle'         => "",
            'metaDescription'   => "",
            'systemPackage'     => DefService::getSysPackSite(),
            'url'               => $this->getUrlForUserPage(),
            'pageClass'         => $this->getPageClassForUserPage(),
            'pageLayout'        => $this->getPageLayoutForUserPage(),
            'domain'            => $application->getUserDomain(),
            'entityType'        => $this->getEntityName(),
            'entityId'          => $this->getId(),
            'readyToUse'        => false
        ));

        $this->setUserPage($userPage);
        $this->save(array( 'userPage' => $userPage));
    }

    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /**
     * Возвращает url связанной пользовательской страницы
     * @return string
     */
    public function getUserUrl(){
        return $this->getUserPage()->getUrl();
    }

    /**
     * Возвращает PageClass для пользовательской страницы
     * Возвращаемые значения:
     *      array           - параметры внутри массива будут использоваться для поиска уже существующей сущности
     *      int             - принимается по умолчанию за идентификатор существующей сущности
     *      PAGE_sys_ClassEntity - новый экзепляр сущности PAGE_sys_ClassEntity
     *
     * @return array|int|PAGE_sys_ClassEntity
     */
    protected function getPageClassForUserPage(){
        return DefService::getControllerSiteEntity();
    }

    /**
     * Возвращает шаблон для пользовательской страницы
     * Возвращаемые значения:
     *      array           - параметры внутри массива будут использоваться для поиска уже существующей сущности
     *      int             - принимается по умолчанию за идентификатор существующей сущности
     *      SYS_sys_TemplateEntity  - новый экзепляр сущности SYS_sys_TemplateEntity
     *
     * @return array|int|SYS_sys_TemplateEntity
     */
    protected function getPageLayoutForUserPage(){
        return DefService::getTemplateEntity();
    }

    /**
     * Метод отвечает за генерацию Title для пользовательской страницы
     * @return string Title для пользовательской страницы
     */
    protected function getTitleForUserPage(){
        return "Пользовательская страница для {$this->getTitle()}";
    }

    /**
     * Метод отвечает за генерацию Url для пользовательской страницы.
     * По умолчанию возвращает пустую строку поэтому именно сущность потомка должна отвечать за то как именно
     * генерировать url.
     * @return string Url для пользовательской страницы
     */
    abstract protected function getUrlForUserPage();

    /**
     * Переводит строку на русском в транслит
     * @param $str
     * @return string
     */
    protected function translitString($str){
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "_", "."=> "", "/"=> "_"
        );
        return strtr($str,$tr);
    }

    /**
     * Переводит строку на русском в корректный url
     * @param $urlStr
     * @return mixed|string
     */
    protected function translitUrl($urlStr){
        if (preg_match('/[^A-Za-z0-9_\-]/', $urlStr)) {
            $urlStr = $this->translitString($urlStr);
            $urlStr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlStr);
        }
        return $urlStr;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param boolean $readyToUse
     * @return ENTITY_sys_GeneralAbstract
     */
    public function setReadyToUse($readyToUse) {
        $this->readyToUse = $readyToUse;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getReadyToUse() {
        return $this->readyToUse;
    }

    /**
     * @param \Jazz\Entities\PAGE_site_UserEntity $userPage
     * @return ENTITY_sys_GeneralAbstract
     */
    public function setUserPage($userPage) {
        $this->userPage = $userPage;
        return $this;
    }

    /**
     * @return \Jazz\Entities\PAGE_site_UserEntity
     */
    public function getUserPage() {
        return $this->userPage;
    }

}
