<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 08.11.12
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Данный класс используется для типизации виджетов разных типов, именно из этого списка пользователь будет выбирать
 * какой именно тип виджета он хочет добавить. В свойстве type хранится имя класса сущности которым обрабатываются все
 * виджеты данного типа. Именно по этому своству в коде можно будет читать и отпределять что за виджет у нас в руках
 * и какой тип виджета создавать.
 *
 * Вообще у данного класса сущносте довлльно большой спектр применения.
 * P.S. Я так думаю :)
 * @Entity
 * @Table(name="widget_types_entities")
 */
class WIDGET_any_TypeEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * Имя класса типа виджета
     * @Column(type="string", length=128)
     * @var string
     */
    protected $type;
    /**
     * Описание типа виджета
     * @Column(type="text")
     * @var string
     */
    protected $description;
    /**
     * Группа данного типа виджета
     * @ManyToOne(targetEntity="WIDGET_any_GroupEntity", inversedBy="widgetTypes")
     * @var WIDGET_any_GroupEntity
     */
    protected $widgetGroup;
    /**
     * Список виджетов данного типа
     * @OneToMany(targetEntity="WIDGET_any_Entity", mappedBy="widgetType")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $widgets;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        parent::__construct();
        $this->widgets = new ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $type Имя класса типа виджета
     * @return WIDGET_any_TypeEntity
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string Имя класса типа виджета
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $description Описание типа виджета
     * @return WIDGET_any_TypeEntity
     */
    public function setDescription($description) {
        $this->setTranslateString('description', $description);
        return $this;
    }

    /**
     * @return string Описание типа виджета
     */
    public function getDescription() {
        return $this->getTranslateString('description');
    }

    /**
     * @param \Jazz\Entities\WIDGET_any_GroupEntity $widgetGroup Группа данного типа виджета
     * @return WIDGET_any_TypeEntity
     */
    public function setWidgetGroup($widgetGroup) {
        $this->widgetGroup = $widgetGroup;
        return $this;
    }

    /**
     * @return \Jazz\Entities\WIDGET_any_GroupEntity Группа данного типа виджета
     */
    public function getWidgetGroup() {
        return $this->widgetGroup;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $widgets Список виджетов данного типа
     * @return WIDGET_any_TypeEntity
     */
    public function setWidgets($widgets) {
        $this->widgets = $widgets;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection Список виджетов данного типа
     */
    public function getWidgets() {
        return $this->widgets;
    }
}
