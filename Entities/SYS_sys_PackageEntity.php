<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="system_package_entities")
 */
class SYS_sys_PackageEntity  extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $name;
    /**
     * @ManyToOne(targetEntity="SYS_sys_DomainEntity", inversedBy="systemPackages", cascade={"persist"})
     * @description Домен которому принадлежит данная сущность
     * @var \Jazz\Entities\SYS_sys_DomainEntity
     */
    protected $domain;

    /******************************************************************************************************************
     * Additional methods / Вспомогательные методы
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getPackageLayoutClassName() {
        return ucfirst($this->name) . "PackageLayout";
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param \Jazz\Entities\SYS_sys_DomainEntity $domain
     * @return SYS_sys_PackageEntity
     */
    public function setDomain($domain) {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_DomainEntity
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * @param string $name
     * @return SYS_sys_PackageEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

}