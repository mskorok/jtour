<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(name="user_pages_entities")
 */

class PAGE_site_UserEntity extends \Jazz\Entities\PAGE_sys_Entity{
    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/
    const READY_FOR_USE = false;

    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="SYS_sys_TemplateEntity", cascade={"persist"})
     * @description Шаблон которым отображать данную страницу. ( Page Layout )
     * @var \Jazz\Entities\SYS_sys_TemplateEntity
     */
    protected $pageLayout;
    /**
     * @Column(type="string", length=256, nullable=true)
     * @var string
     */
    protected $entityType;
    /**
     * @Column(type="integer", nullable=true)
     * @var int
     */
    protected $entityId;
    /**
     * @Column(type="string", length=80)
     * @description Meta заголовок у данной страницы
     * @var string
     */
    protected $metaTitle;
    /**
     * @Column(type="string", length=256)
     * @description Meta описание у данной страницы
     * @var string
     */
    protected $metaDescription;
    /**
     * @ManyToMany(targetEntity="PAGE_seo_MetaKeywordEntity")
     * @JoinTable(name="pages_metakeywords",
     * joinColumns={@JoinColumn(name="page_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="metaKeyword_id", referencedColumnName="id", unique=true)})
     * @description Набор ключевых слов для поисковика.
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $metaKeywords;
    /**
     * @Column(type="boolean")
     * @description Флаг указатель говорит о том что данная страница уже корректно заполенна,
     *              проверена и может быть доступна не авторизированным пользователям.
     *              Собственно будет специальный человек который будет следить за заполненностью и ставить данный указатель.
     * @var bool
     */
    private $readyToUse;
    /**
     * @OneToMany(targetEntity="PAGE_sys_SectionEntity", mappedBy="page", cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $sections;

    /******************************************************************************************************************
     * Entity Property / Свойства сущности
     ******************************************************************************************************************/
    /**
     * @var \Jazz\Entities\ENTITY_sys_GeneralAbstract
     */
    protected $_linkedEntity;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->readyToUse = self::READY_FOR_USE;
        $this->metaKeywords = new \Doctrine\Common\Collections\ArrayCollection();
        $this->metaDescription = "";
    }

    /**
     * @PostPersist
     */
    public function doPostPersistReadyToUseEvent(){
        $this->updateReadyToUse();
    }

    /**
     * @PostUpdate
     */
    public function doPostUpdateReadyToUseEvent(){
        $this->updateReadyToUse();
    }

    private function updateReadyToUse(){
        if( !$this->getReadyToUse() && $this->getMetaTitle() !== "" && $this->getMetaDescription() !== "" ){
            $this->setReadyToUse(true);
            $app = \Jazz\Application::getInstance();
            $app->entityFlush($this);
        }
    }
    /******************************************************************************************************************
     * Get Labels methods / Методы возвращают
     ******************************************************************************************************************/
    protected function getFieldLabelForTitle()          { return "Заголовок"; }
    protected function getFieldLabelForUrl()            { return "Ссылка на пользовательскую страницу"; }
    protected function getFieldLabelForReadyToUse()     { return "Готова к выводу на сайт"; }
    protected function getFieldLabelForMetaTitle()      { return "Meta заголовок"; }
    protected function getFieldLabelForMetaDescription(){ return "Meta описание"; }
    /******************************************************************************************************************
     * Additional methods / Вспомогательные методы
     ******************************************************************************************************************/
    public function getLinkedEntity(){
        $linkedEntity = $this->_linkedEntity;
        $entityType = $this->getEntityType();

        if( !isset($linkedEntity) && isset($entityType) ){
            $repo = \Jazz\Application::ENTITY_REPO;
            $linkedEntity = $this->getEntityManager()->find($repo.$entityType."Entity", $this->getEntityId());
            $this->_linkedEntity = $linkedEntity;
        }

        return $linkedEntity;
    }

    /**
     * @return array
     */
    public function getPageItemName(){
        return array();
    }

    public function getPageTemplate(){
        return $this->getPageLayout()->getTemplate();
    }
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $metaKeywords
     * @return PAGE_sys_Entity
     */
    public function setMetaKeywords($metaKeywords) {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMetaKeywords() {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaTitle
     * @return PAGE_sys_Entity
     */
    public function setMetaTitle($metaTitle) {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle() {
        return $this->metaTitle;
    }

    /**
     * @param boolean $readyToUse
     * @return PAGE_sys_Entity
     */
    public function setReadyToUse($readyToUse) {
        $this->readyToUse = $readyToUse;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getReadyToUse() {
        return $this->readyToUse;
    }

    /**
     * @param string $metaDescription
     * @return PAGE_sys_Entity
     */
    public function setMetaDescription($metaDescription) {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription() {
        return $this->metaDescription;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $sections
     * @return PAGE_sys_Entity
     */
    public function setSections($sections) {
        $this->sections = $sections;
        return $this;
    }

    /**
     * @param \Jazz\Entities\PAGE_sys_SectionEntity $section
     * @return PAGE_sys_Entity
     */
    public function addSection($section) {
        $section->setPage($this);
        $this->sections->add($section);
        return $this;
    }

    /**
     * @param int $key
     * @return PAGE_sys_Entity
     */
    public function removeSection($key) {
        /**
         * @var $section \Jazz\Entities\PAGE_sys_SectionEntity
         */
        $section = $this->sections->get($key);
        $this->sections->remove($key);
        $section->remove();

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSections() {
        return $this->sections;
    }

    /**
     * @param int $entityId
     * @return PAGE_site_UserEntity
     */
    public function setEntityId($entityId) {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId() {
        return $this->entityId;
    }

    /**
     * @param string $entityType
     * @return PAGE_site_UserEntity
     */
    public function setEntityType($entityType) {
        $this->entityType = $entityType;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityType() {
        return $this->entityType;
    }

    /**
     * @param \Jazz\Entities\SYS_sys_TemplateEntity $pageLayout
     * @return PAGE_site_UserEntity
     */
    public function setPageLayout($pageLayout) {
        $this->pageLayout = $pageLayout;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_TemplateEntity
     */
    public function getPageLayout() {
        return $this->pageLayout;
    }

}