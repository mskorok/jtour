<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 07.09.12
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
/**
 * @Entity
 * @Table(name="languages_entities")
 */
class TRANSLATE_sys_LanguageEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=2)
     * @var string
     */
    protected $symbol;
    /**
     * @Column(type="string", length=256)
     * @var string
     */
    protected $title;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct($symbol = "") {
        parent::__construct();
        $this->symbol = $symbol;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $symbol
     * @return TRANSLATE_sys_LanguageEntity
     */
    public function setSymbol($symbol) {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol() {
        return $this->symbol;
    }

    /**
     * @param string $title
     * @return TRANSLATE_sys_LanguageEntity
     */
    public function setTitle($title) {
        $this->setTranslateString('title', $title);
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->getTranslateString('title');
    }

}
