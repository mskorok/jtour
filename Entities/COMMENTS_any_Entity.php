<?php
namespace Jazz\Entities;
use Jazz\Entities\USER_sys_Entity, DateTime;
/**
 * Created by JetBrains PhpStorm.
 * User: mike
 * Date: 21.11.12
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 *
 * @description Менеджер задач
 *
 * @Entity
 * @Table(name="comments_entities")
 */
class COMMENTS_any_Entity   extends \Jazz\Entities\ENTITY_sys_Abstract
{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @description тип сущности, к которой принадлежим
     * @Column(type="string")
     * @var string
     */
    protected $linkedEntityType;
    /**
     * @description id сущности, к которой принадлежим
     * @Column(type="integer")
     * @var int
     */
    protected $linkedEntityId;
    /**
     * @description Комментарий к выполнению задачи
     * @Column(type="text")
     * @var string
     */
    protected $text;
    /**
     * @description Дата создания комментария
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $created;
    /**
     * @description Автор комментария
     * @ManyToOne(targetEntity="USER_sys_Entity")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     * @var \Jazz\Entities\USER_sys_Entity
     */
    protected $author;
    /**
     * @description комментарий удален
     * @Column(type="boolean")
     * @var bool
     */
    protected $removed;
    /**
     * @description комментарий был отредактирован
     * @Column(type="boolean")
     * @var bool
     */
    protected $edited;
    /**
     * @description комментарий является дочерним
     * @Column(type="boolean")
     * @var bool
     */
    protected $isChild;

    /**
     * @description Идентификаторы дочерних комментариев
     * @var array
     */
    protected $ChildrenId;

    /**
     * @OneToMany(targetEntity="COMMENTS_any_Entity", mappedBy="parent")
     */
    private $children;

    /**
     * @ManyToOne(targetEntity="COMMENTS_any_Entity", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct()
    {
        parent::__construct();
        $this->text       = "";
        $this->created    = new \DateTime("now");
        $this->removed    = false;
        $this->edited     = false;
        $this->isChild    = false;
        $this->childrenId = array();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    public function getTitle(){
        $date = $this->getCreated()->format("d.m.Y");
        return "Комментарий пользователя {$this->getAuthor()} от {$date}";
    }

    /**
     * @param \Jazz\Entities\USER_sys_Entity $author
     * @return COMMENTS_any_Entity
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return \Jazz\Entities\USER_sys_Entity
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param \DateTime $created
     * @return COMMENTS_any_Entity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $linkedEntityId
     * @return COMMENTS_any_Entity
     */
    public function setLinkedEntityId($linkedEntityId)
    {
        $this->linkedEntityId = $linkedEntityId;
        return $this;
    }

    /**
     * @return int
     */
    public function getLinkedEntityId()
    {
        return $this->linkedEntityId;
    }

    /**
     * @param string $linkedEntityType
     * @return COMMENTS_any_Entity
     */
    public function setLinkedEntityType($linkedEntityType)
    {
        $this->linkedEntityType = $linkedEntityType;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedEntityType()
    {
        return $this->linkedEntityType;
    }

    /**
     * @param string $text
     * @return COMMENTS_any_Entity
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param boolean $removed
     * @return COMMENTS_any_Entity
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * @param boolean $edited
     * @return COMMENTS_any_Entity
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEdited()
    {
        return $this->edited;
    }

    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param boolean $isChild
     * @return COMMENTS_any_Entity
     */
    public function setIsChild($isChild)
    {
        $this->isChild = $isChild;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsChild()
    {
        return $this->isChild;
    }

    /**
     * @param array $ChildrenId
     * @return COMMENTS_any_Entity
     */
    public function setChildrenId($ChildrenId)
    {
        $this->ChildrenId = $ChildrenId;
        return $this;
    }

    /**
     * @return array
     */
    public function getChildrenId()
    {
        return $this->ChildrenId;
    }


    /******************************************************************************************************************
     * Additional methods
     *****************************************************************************************************************/
    /**
     *
     * @static
     * @param $dql
     * @return array
     */
    protected static function getDqlResult($dql){
        $query = \Jazz\Application::getInstance()->getEntityManager()->createQuery($dql);
        return $query->getResult();
    }

    /**
     * Находит все комментарии которые принадлежат комментируемой сущности
     * @static
     * @param $entity \Jazz\Entities\ENTITY_sys_Abstract
     * @return array
     */
    public static function getCommentsByEntity($entity){
        return \Jazz\Application::getInstance()->getEntityFindBy('COMMENTS_any_Entity', array(
            'linkedEntityType'  => $entity->getShortClassName(),
            'linkedEntityId'    => $entity->getId(),
        ));
    }

    /**
     * Возвращает кол-во комментариев
     * @static
     * @param $entity \Jazz\Entities\ENTITY_sys_Abstract
     * @return int
     */
    public static function getCommentCount($entity){
        $dql = "
            SELECT COUNT(c.id) AS cnt
            FROM \\Jazz\\Entities\\COMMENTS_any_Entity As c
            WHERE
                c.linkedEntityType = '{$entity->getShortClassName()}'
                AND c.linkedEntityId = {$entity->getId()}
                AND c.removed != 1
        ";
        $result = self::getDqlResult($dql);

        return $result[0]['cnt'];
    }

    /**
     * Возвращает кол-во комментариев в виде строки:
     * 0 комментариев
     *
     * 1 комментарий
     *
     * 2 комментария
     * ...
     * 4 комментария
     *
     * 5 комментариев
     * ...
     * n комментариев
     *
     * @static
     * @param $entity \Jazz\Entities\ENTITY_sys_Abstract
     * @return string
     */
    public static function getCommentsToString($entity){
        $count = self::getCommentCount($entity);
        $result = "{$count} комментари";

        if( $count == 1 ) $result .= "й";
        if( $count == 0 || $count >= 5 ) $result .= "ев";
        if( $count >= 2 && $count <= 4 ) $result .= "я";
        return $result;
    }

}
