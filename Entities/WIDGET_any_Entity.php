<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @Table(name="widgets_entities")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"WIDGET_any_Entity" = "WIDGET_any_Entity", "WIDGET_sys_HtmlEntity" = "WIDGET_sys_HtmlEntity"})
 */
class WIDGET_any_Entity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Default property values / Значения свойств по умолчанию
     ******************************************************************************************************************/
    /**
     * @description By default show title / По умолчанию показываем заголовок
     */
    const SHOW_HEADER_TITLE = true;
    /**
     * @description By default the widget is not collapsible / По умолчанию виджет не сворачиваемый
     */
    const IS_COLLAPSIBLE = false;
    /**
     * @description By default an empty css-class / По умолчанию виджет не сворачиваемый
     */
    const CUSTOM_CSS_CLASS = "";

    /******************************************************************************************************************
     * Properties / Свойства
     ******************************************************************************************************************/
    /**
     * Название заголовка
     * @Column(type="string", length=256)
     * @description Title name
     * @var \Jazz\Entities\TRANSLATE_sys_MessageEntity Название заголовка
     */
    protected $headerTitle;
    /**
     * Показывать ли заголовок
     * @Column(type="boolean")
     * @description Whether to show the title
     * @var boolean Показывать ли заголовок
     */
    protected $showHeaderTitle;
    /**
     * Может ли виджет сворачиваться
     * @Column(type="boolean")
     * @description Can widget collapsed
     * @var boolean Может ли виджет сворачиваться
     */
    protected $isCollapsible;
    /**
     * Произвольный сss сlass - он будет навешиваться на контейнер с виджетом. Нужен для задания произвольного вида виджета.
     * @Column(type="string", length=256)
     * @description Random css class - it will be hinged on the container with widget. Need for a random widget style.
     * @var string Произвольный сss сlass
     */
    protected $customCssClass;
    /**
     * Секция в которой рассположен данный виджет
     * @ManyToOne(targetEntity="PAGE_sys_SectionEntity", inversedBy="widgets")
     * @var \Jazz\Entities\PAGE_sys_SectionEntity
     */
    protected $section;
    /**
     * Тип виджета
     * @ManyToOne(targetEntity="WIDGET_any_TypeEntity", inversedBy="widgets")
     * @var \Jazz\Entities\WIDGET_any_TypeEntity
     */
    protected $widgetType;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->showHeaderTitle  = self::SHOW_HEADER_TITLE;
        $this->isCollapsible    = self::IS_COLLAPSIBLE;
        $this->customCssClass   = self::CUSTOM_CSS_CLASS;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $headerTitle Название заголовка
     * @return WIDGET_any_Entity
     */
    public function setHeaderTitle($headerTitle) {
        $this->setTranslateString('headerTitle', $headerTitle);
        return $this;
    }

    /**
     * @return \Jazz\Entities\TRANSLATE_sys_MessageEntity Название заголовка
     */
    public function getHeaderTitle() {
        return $this->getTranslateString('headerTitle');
    }

    /**
     * @param string $customCssClass Произвольный сss сlass
     * @return WIDGET_any_Entity
     */
    public function setCustomCssClass($customCssClass) {
        $this->customCssClass = $customCssClass;
        return $this;
    }

    /**
     * @return string Произвольный сss сlass
     */
    public function getCustomCssClass() {
        return $this->customCssClass;
    }

    /**
     * @param boolean $isCollapsible Может ли виджет сворачиваться
     * @return WIDGET_any_Entity
     */
    public function setIsCollapsible($isCollapsible) {
        $this->isCollapsible = $isCollapsible;
        return $this;
    }

    /**
     * @return boolean Может ли виджет сворачиваться
     */
    public function getIsCollapsible() {
        return $this->isCollapsible;
    }

    /**
     * @param boolean $showHeaderTitle Показывать ли заголовок
     * @return WIDGET_any_Entity
     */
    public function setShowHeaderTitle($showHeaderTitle) {
        $this->showHeaderTitle = $showHeaderTitle;
        return $this;
    }

    /**
     * @return boolean Показывать ли заголовок
     */
    public function getShowHeaderTitle() {
        return $this->showHeaderTitle;
    }

    /**
     * @param \Jazz\Entities\PAGE_sys_SectionEntity $section Секция в которой рассположен данный виджет
     * @return WIDGET_any_Entity
     */
    public function setSection($section) {
        $this->section = $section;
        return $this;
    }

    /**
     * @return \Jazz\Entities\PAGE_sys_SectionEntity Секция в которой рассположен данный виджет
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * @param \Jazz\Entities\WIDGET_any_TypeEntity $widgetType Тип виджета
     * @return WIDGET_any_Entity
     */
    public function setWidgetType($widgetType) {
        $this->widgetType = $widgetType;
        return $this;
    }

    /**
     * @return \Jazz\Entities\WIDGET_any_TypeEntity Тип виджета
     */
    public function getWidgetType() {
        return $this->widgetType;
    }

}