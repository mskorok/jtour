<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="domains_entities")
 */
class SYS_sys_DomainEntity  extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $href;
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $pageClassName;
    /**
     * @OneToMany(targetEntity="SYS_sys_PackageEntity", mappedBy="domain", cascade={"persist"})
     * @description Список доступных пакетов
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $systemPackages;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->systemPackages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $href
     * @return SYS_sys_DomainEntity
     */
    public function setHref($href) {
        $this->href = $href;
        return $this;
    }

    /**
     * @return string
     */
    public function getHref() {
        return $this->href;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $systemPackages
     * @return SYS_sys_DomainEntity
     */
    public function setSystemPackages($systemPackages) {
        $this->systemPackages = $systemPackages;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSystemPackages() {
        return $this->systemPackages;
    }

    /**
     * @param string $pageClassName
     * @return SYS_sys_DomainEntity
     */
    public function setPageClassName($pageClassName) {
        $this->pageClassName = $pageClassName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageClassName() {
        return $this->pageClassName;
    }


}