<?php

namespace Jazz\Entities;
use DateTime, Jazz\Entities\USER_sys_GroupEntity, Jazz\Entities\USER_sys_DescriptionEntity;

/** @Entity
 * @Table(name="users_entities")
 */
class USER_sys_Entity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=32)
     * @var string
     */
    protected $login;
    /**
     * @Column(type="string", length=128)
     * @var string
     */
    protected $email;
    /**
     * @Column(type="string", length=32)
     * @var string
     */
    protected $password;
    /**
     * @Column(type="string", length=32)
     * @var string
     */
    protected $hash;
    /**
     * @Column(nullable=true)
     * @var mixed
     */
    protected $isAuthorized;
    /**
     * @OneToOne(targetEntity="USER_sys_DescriptionEntity", inversedBy="user", orphanRemoval=true, cascade={"persist"})
     * @var \Jazz\Entities\USER_sys_DescriptionEntity
     */
    protected $description;
    /**
     * @ManyToMany(targetEntity="USER_sys_GroupEntity", inversedBy="users", cascade={"persist"})
     * @JoinTable(name="users_groups")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $groups;
    /**
     * @OneToMany(targetEntity="NOTIF_sys_SubscriberUserEntity", mappedBy="user", cascade={"persist"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $subscribers;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->hash             = "";
        $this->groups           = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscribers      = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isAuthorized     = null;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @return string
     */
    public function getTitle(){
        $desc = $this->getDescription();
        return "{$desc->getFirstName()} {$desc->getLastName()}";
    }

    /**
     * @param \Jazz\Entities\USER_sys_DescriptionEntity $description
     * @return USER_sys_Entity
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \Jazz\Entities\USER_sys_DescriptionEntity
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $email
     * @return USER_sys_Entity
     */
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $groups
     * @return USER_sys_Entity
     */
    public function setGroups($groups) {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @param \Jazz\Entities\USER_sys_GroupEntity $group
     * @return USER_sys_Entity
     */
    public function addGroup($group) {
        $group->addUser($this);
        $this->groups->add($group);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGroups() {
        return $this->groups;
    }

    /**
     * @param string $login
     * @return USER_sys_Entity
     */
    public function setLogin($login) {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * @param string $password
     * @return USER_sys_Entity
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param $isAuthorized
     * @return USER_sys_Entity
     */
    public function setIsAuthorized($isAuthorized) {
        $this->isAuthorized = $isAuthorized;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getIsAuthorized() {
        return $this->isAuthorized;
    }

    /**
     * @param string $hash
     * @return USER_sys_Entity
     */
    public function setHash($hash) {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash() {
        return $this->hash;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $subscribers
     * @return USER_sys_Entity
     */
    public function setSubscribers($subscribers) {
        $this->subscribers = $subscribers;
        return $this;
    }

    /**
     * @param \Jazz\Entities\NOTIF_sys_SubscriberUserEntity $subscriber
     * @return USER_sys_GroupEntity
     */
    public function addSubscriber($subscriber) {
        $this->subscribers->add($subscriber);
        $subscriber->setUser($this);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubscribers() {
        return $this->subscribers;
    }
}