<?php
namespace Jazz\Entities;

 /**
  * @Entity
  * @InheritanceType("JOINED")
  * @Table(name="geo_tree_entities")
  * @DiscriminatorColumn(name="discrim", type="string")
  * @DiscriminatorMap({"GEO_any_TreeEntity" = "GEO_any_TreeEntity", "GEO_any_CountryEntity" = "GEO_any_CountryEntity", "GEO_any_RegionEntity" = "GEO_any_RegionEntity", "GEO_any_CityEntity" = "GEO_any_CityEntity", "GEO_any_MarketingRegionEntity" = "GEO_any_MarketingRegionEntity"})
  */
class GEO_any_TreeEntity extends \Jazz\Entities\ENTITY_sys_GeneralAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @ManyToOne(targetEntity="GEO_any_TreeEntity", inversedBy="children") связь один ко многим со ссылкой на себя же
     * @JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @var GEO_any_TreeEntity
     */
    protected $parent;
    /**
     * @OneToMany(targetEntity="GEO_any_TreeEntity", mappedBy="parent")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $children;
    /**
     * @ManyToOne(targetEntity="GEO_any_TypeEntity")
     * @var GEO_any_TypeEntity
     */
    protected $type;
    /**
     *
     * @OneToMany(targetEntity="EVENT_site_PublicEntity", mappedBy="geo")
     * @var
     */
    protected $event;
    /**
     * @ManyToMany(targetEntity="GEO_any_PlaceEntity", mappedBy="geo")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $places;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->readyToUse = false;
        $this->event   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->places = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Метод отвечает за генерацию Title для пользовательской страницы
     * @return string Title для пользовательской страницы
     */
    protected function getTitleForUserPage() {
        return $this->getTitle();
    }

    /**
     * Метод отвечает за генерацию Url для пользовательской страницы.
     * По умолчанию возвращает пустую строку поэтому именно сущность потомка должна отвечать за то как именно
     * генерировать url.
     * @return string Url для пользовательской страницы
     */
    protected function getUrlForUserPage() {
        $parent = $this->getParent();

        $url = (isset($parent) ? $parent->getUserPage()->getUrl() : "") . "/{$this->translitUrl($this->getTitle())}/";

        return $url;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $children
     * @return GEO_any_TreeEntity
     */
    public function setChildren($children) {
        $this->children = $children;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * @param GEO_any_TreeEntity $parent
     * @return GEO_any_TreeEntity
     */
    public function setParent($parent) {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return GEO_any_TreeEntity
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @param \Jazz\Entities\GEO_any_TypeEntity $type
     * @return GEO_any_TreeEntity
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * @return \Jazz\Entities\GEO_any_TypeEntity
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param  $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEvent()
    {
        return $this->event;
    }
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $places
     * @return GEO_any_CityEntity
     */
    public function setPlaces($places) {
        $this->places = $places;
        return $this;
    }

    /**
     * @param \Jazz\Entities\GEO_any_PlaceEntity $place
     * @return GEO_any_TreeEntity
     */
    public function addPlace($place) {
        $this->places->add($place);
        $place->setGeo($this);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPlaces() {
        return $this->places;
    }

}