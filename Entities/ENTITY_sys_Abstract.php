<?php
/**
 * Created by IntelliJ IDEA.
 * User: Konstantin.R.Dark
 * Date: 29.08.12
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */
namespace Jazz\Entities;
use Dcfw\Components\Fields\Validators\IsRequiredValidator;

use Jazz\Services\CacheService as CacheService;
use Jazz\Services\EventService;
use Jazz\Services\CacheCallbacks\Entities\MessageCacheCallback;
use Jazz\Services\CacheCallbacks\Entities\TranslateMessageCacheCallback;
use Jazz\Other\Utils\FieldValidatorRules\FieldValidatorRule;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 */
abstract class ENTITY_sys_Abstract implements ENTITY_sys_Interface, \Serializable{
    /******************************************************************************************************************
     * Constants / Константы
     ******************************************************************************************************************/
    const DEFAULT_SCHEMA_NAME = 'default';
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события
     */
    const MUTE = false;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события создания
     */
    const MUTE_CREATE = false;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события изменения
     */
    const MUTE_UPDATE = false;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события изменения полей
     */
    const MUTE_UPDATE_FIELD = false;
    /**
     * Сущность у которой будет перекрыта данная константа, и определена как true
     * - не будет публиковать события удаления
     */
    const MUTE_REMOVE = false;

    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;
    /******************************************************************************************************************
     * Entity Property / Свойства сущности
     ******************************************************************************************************************/
    /**
     * @var string
     */
    private $_classMetaData;
    /**
     * @var string
     */
    private $_fullClassName;
    /**
     * @var string
     */
    private $_shortClassName;
    /**
     * @var string
     */
    private $_entityName;
    /**
     * @var array
     */
    private $_validators;
    /**
     * При создании переводимых полей - сюда будут добавляться ключи для создания TRANSLATE_sys_MessageEntity
     * @var array
     */
    private $_newMessageEntities;
    /**
     * При изменении или создании сущности сюда записываются все измененные поля, в последствии по которым происходит
     * публикация событий
     * @var array
     */
    private $_changedFields;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    function __construct() {
        $this->_newMessageEntities = array();
        $this->_changedFields = array();
    }

    /**
     * @static
     * @return ENTITY_sys_Abstract
     */
    public static function create(){
        return new static();
    }
    /******************************************************************************************************************
     * Events / События
     ******************************************************************************************************************/
    /**
     * @PrePersist
     */
    public function doPrePersistEvent(){
        $this->updateChangeFields();
    }

    /**
     * @PreUpdate
     */
    public function doPreUpdateEvent(){
        $this->updateChangeFields();
    }

    /**
     * @PostPersist
     */
    public function doPostPersistEvent(){
        if( $this->isNewMessageEntity() ){
            $this->createNewMessageEntity();
        }

        $this->updateTags();

        EventService::createEntityTriggerEvent($this);
        $this->triggerChangedFieldEvents();
    }

    /**
     * @PostUpdate
     */
    public function doPostUpdateEvent(){
        $this->updateTags();

        EventService::updateEntityTriggerEvent($this);
        $this->triggerChangedFieldEvents();
    }

    /**
     * @PreRemove
     */
    public function doPreRemoveEvent(){
        $this->updateTags();

        EventService::removeEntityTriggerEvent($this);
    }

    private function triggerChangedFieldEvents(){
        $changed = $this->getChangedField();

        if( is_array($changed) ){
            foreach( $changed as $fieldName => $value ){
                EventService::updateEntityFieldTriggerEvent($this, $fieldName, $value[0], $value[1]);
            }
        }
    }

    private function updateChangeFields(){
        $em = $this->getEntityManager();
        $uow = $em->getUnitOfWork();
        $arr = $uow->getEntityChangeSet($this);

        foreach( $arr as $key => $val ){
            // Тут происходит определение какие именно поля изменились а так же известно старое и новое значение
            $this->_changedFields[$key] = $val;
        }
    }

    private function updateTags(){
        // Обновим теги в кеше для сущности
        CacheService::getInstance()->updateTagVersion($this->getShortClassName());
        CacheService::getInstance()->updateTagVersion($this->getShortClassName() . "_" . $this->getId());
    }

    /**
     * Создает TRANSLATE_sys_MessageEntity строк которые нужнаются в переводимости
     */
    final private function createNewMessageEntity(){
        $messages = $this->getNewMessageEntities();
        $app = \Jazz\Application::getInstance();
        if( count($messages) ){
            foreach( $messages as $propertyName ){
                $message = new TRANSLATE_sys_MessageEntity();
                $message
                    ->setLinkedEntityName($this->getShortClassName())
                    ->setLinkedEntityId($this->getId())
                    ->setLanguage($app->getCheckedLanguageEntity())
                    ->setPropertyName($propertyName)
                ;
                $app->entityPersist($message);
            }
        }
        $app->entityFlush();
    }

    /******************************************************************************************************************
     * getValidators and Validate / Валидация и валидаторы
     ******************************************************************************************************************/
    /**
     * @protected
     * @description Возвращает массив правил типа FieldValidatorRule которые содержат в себе нужные валидаторы
     *              Именно его нужно перекрывать в потомках
     * @return array Массив правил типа FieldValidatorRule
     */
    protected function getValidators(){
        return array(
            new FieldValidatorRule(array('id'), new IsRequiredValidator(), array(self::DEFAULT_SCHEMA_NAME)),
        );
    }

    /**
     * @private
     * @description Возвращает массив правил типа FieldValidatorRule и сохраняет их в $this->_validators?
     *              в дальнейшем все обращения идут в это свойство.
     *              Это сделано специально для того чтобы не создавать постоянно одни и теже экземпляры валидаторов,
     *              а использовать один и тот же экземпляр.
     * @return array Массив правил типа FieldValidatorRule
     */
    private function _getValidators(){
        $validators = $this->_validators;

        if( !isset($validators) ){
            $validators = $this->getValidators();
            $this->_validators = $validators;
        }

        return $validators;
    }

    /**
     * @description Вернет массив ошибок или true
     * @param $data
     * @param string $schemaName
     * @return array|bool Вернет массив ошибок или true
     */
    public function validate($data, $schemaName = self::DEFAULT_SCHEMA_NAME){
        $rules = $this->_getValidators();
        $isStopValidate = false;
        $results = array();

        foreach( $data as $key => $value ){
            /**
             * @var $rule \Jazz\Other\Utils\FieldValidatorRules\FieldValidatorRule
             */
            foreach( $rules as $rule ){
                if( $rule->containsProperty($key) && $rule->schemaNames($schemaName) ){
                    $validator = $rule->getValidator();

                    if( !$validator->validate($value) ){
                        $isStopValidate = true;
                        $results[$key][] = $validator->getErrorText();
                    }
                }
            }
        }

        return $isStopValidate ? $results : true;
    }

    /**
     * @description Возвращает массив валидаторов для клиентской стороны по имени свойства и имени схемы валидации
     * @param string $propertyName Имя свойства
     * @param string $schemaName Имя схемы для валидации
     * @return array
     */
    public function getValidatorsForClient($propertyName, $schemaName = self::DEFAULT_SCHEMA_NAME){
        $rules = $this->_getValidators();
        $results = array();

        /**
         * @var $rule \Jazz\Other\Utils\FieldValidatorRules\FieldValidatorRule
         */
        foreach( $rules as $rule ){
            if( $rule->containsProperty($propertyName) && $rule->schemaNames($schemaName) && !$rule->isServerValidateOnly() ){
                $results[] = $rule->getValidator();
            }
        }

        return $results;

    }

    /******************************************************************************************************************
     * Serialize and UnSerialize / Сериализация и десериализация
     ******************************************************************************************************************/
    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize() {
        return serialize($this->_doSerialize());
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return mixed the original value unserialized.
     */
    public function unserialize($serialized) {
        $this->_doUnSerialize(unserialize($serialized));
    }

    /**
     * @description Вызывается при сохранении сущности в кеш. В данном методе нужно позаботится чтобы все свойства
     *              данной сущности корректно перенеслись в простой массив.
     * @return array
     */
    protected function _doSerialize(){
        $fieldMapping = $this->getFieldMapping();
        $ser = array();

        foreach( $fieldMapping as $propertyKey => $propertyValue ){
            $ser[$propertyKey] = $this->$propertyKey;
        }

        return $ser;
    }

    /**
     * @description Вызывается при восстановлении сущности из кеша. В данном методе нужно позаботится чтобы все свойства
     *              данной сущности корректно перенеслись из простого массива в поля сущности.
     * @param $data
     */
    public function _doUnSerialize($data){
        $em = $this->getEntityManager();
        /**
         * @description Собственно восстанавливаем коннект к бд.
         */
        if( property_exists($this, '_entityPersister') && property_exists($this, '_identifier')){
            $this->_entityPersister = $em->getUnitOfWork()->getEntityPersister($this->getFullClassName());
            $this->_identifier = array(
                'id' => $data['id']
            );
        }

        $fieldMapping = $this->getFieldMapping();

        foreach( $fieldMapping as $propertyKey => $propertyValue ){
            $this->$propertyKey = $data[$propertyKey];
        }

    }

    /******************************************************************************************************************
     * Save and Remove entity / Сохранение и удаление сущности
     ******************************************************************************************************************/
    /**
     * @description Отвечает за механизм сохранения ( update ) сущности и всех связанных с ней.
     *              Реализует паттерн "Шаблонный метод".
     *              Перед вызовом flush сначала вызывает метод validate() если все хорошо то метод _doSave()
     * @param array $data
     * @param array $data
     * @param string $schemaName
     * @throws \Exception
     * @return \Jazz\Entities\ENTITY_sys_Abstract
     */
    public function save($data, $schemaName = self::DEFAULT_SCHEMA_NAME) {
        // TODO Убрать нафиг от сюда - вынести валидацию в методы PrePersist и PreUpdate.
        // Дабы она срабатывала непосредственно перед сохранением сущности в базу даже если объект был создан
        // и свойства ему были проставленны напрямую через setters. А так мы тупо ограничиваем использование валидации
        // Только при сохранении карточки через этот метод. А это ни есть true.
        $result = $this->validate($data, $schemaName);
        if( $result === true ){

            $this->_doSave($data);

            $app = \Jazz\Application::getInstance();
            $app->entityPersist($this)->entityFlush();
        }else{
            //TODO Обработать результаты с ошибками
            throw new \Exception("!!! Карамба - Валидация упала !!! :)");
        }

        return $this;
    }

    /**
     * @description Предназначение метода - транспортировка своих свойств из массива $data в свойства сущности.
     *              Предпологается что данный массив приходит с "клиента".
     * @param array $data
     */
    private function _doSave($data) {

        // Сначала позаботимся о простых полях
        $this->_doSaveSimpleProperty($data);
        // Потом пробежимся по связанным полям
        $this->_doSaveAssociationProperty($data);
    }

    /**
     * @description Метод заботится о сохранении простых свойств. Точнее за проставление их в модель
     * @param array $data
     * @throws \Exception
     */
    private function _doSaveSimpleProperty(&$data){
        $shortName = $this->getShortClassName();
        $fieldMapping = $this->getFieldMapping();

        // Сначала пробежимся по простым полям
        foreach( $fieldMapping as $fieldName => $fieldMap ){
            // Пропускаем id - это поле не изменяемое
            if( $fieldName == 'id' ) continue;

            //На клиенте не заполнили данные по полю которое не может быть пустым nullable - кидаем ошибку
            $isSetField = isset($data[$fieldName]);

            // Данные пришли - все хорошо
            if( $isSetField ){
                $value = $data[$fieldName];
                $valueType = $fieldMap['type'];

                //Почистим пробелы в строковых значениях
                if( $valueType == 'string' || $valueType == 'text' ){
                    $value = trim($value);
                }
                //Все int приведем к нужному типу
                if( $valueType == 'integer' || $valueType == 'smallint' || $valueType == 'bigint' ){
                    $value = (int)$value;
                }

                $this->_doSaveCheckMethods($fieldName, $value);
                continue;
            }

            $isNullable = $fieldMap['nullable'];
            $getMethod = "get" . ucfirst($fieldName);
            $getValue = $this->$getMethod();

            //Проверяем если в модели данных нет и при этом они не пришли с сервер,
            //но не могут быть null - кинем Exception
            if( !isset($getValue) && (!$isSetField && !$isNullable)){
                throw new \Exception("ENTITY_sys_Abstract::_doSave() : Отсутствуют данные по полю {$shortName}::{$fieldName} - 'это поле не может быть пустым ( nullable = false )");
            }

        }
    }

    /**
     * @description Метод заботится о сохранении связанных сущностей. Точнее за проставление их в модель
     * @param array $data
     * @throws \Exception
     */
    private function _doSaveAssociationProperty(&$data){
        $shortName = $this->getShortClassName();
        $associationMappings = $this->getAssociationMapping();

        foreach( $associationMappings as $fieldName => $fieldMap ){
            $joinColumns = isset($fieldMap['joinColumns']) ? $fieldMap['joinColumns'] : false;
            $targetEntityName = $fieldMap['targetEntity'];
            //На клиенте не заполнили данные по полю которое не может быть пустым nullable - кидаем ошибку
            $isSetField = isset($data[$fieldName]);

            //OneToOne bidirectional || OneToOne unidirectional
            if( $joinColumns ){  // Если зашли сюда - у нас одиночная сущность
                // Проверим чтобы сущестовал елемент с ключом 0,
                // обрабатываем ситуацию когда свойство joinColumns пустое
                $joinColumn = isset($joinColumns[0]) ? $joinColumns[0] : array();
                // Пошерстим по массиву - может там указан ключ nullable
                $isNullable = isset($joinColumn['nullable']) ? $joinColumn['nullable'] : false;

                if( $isSetField ){
                    $this->_doSaveCheckMethods($fieldName, $this->doSaveFindEntity($targetEntityName, $data[$fieldName]));
                    continue;
                }

                $getMethod = "get" . ucfirst($fieldName);
                $getValue = $this->$getMethod();

                //Проверяем если в модели данных нет и при этом они не пришли с сервер,
                //но не могут быть null - кинем Exception
                if( !isset($getValue) && (!$isSetField && !$isNullable)){
                    throw new \Exception("ENTITY_sys_Abstract::_doSaveAssociationProperty() : Отсутствуют данные по полю {$shortName}::{$fieldName} - 'это поле не может быть пустым ( nullable = false )");
                }
            }

            // Собсвтенно у нас множество
            if( $isSetField ){
                $this->_doSaveCheckMethods($fieldName, $this->doSaveFindListEntity($targetEntityName, $data[$fieldName]));
            }
        }
    }

    /**
     * @description Поищем в классе сущности метод save для данного свойства,
     *              если он есть вызовем его,
     *              иначе вызовем простой сеттер
     * @param string $fieldName
     * @param mixed $value
     */
    private function _doSaveCheckMethods($fieldName, $value){
        $name = ucfirst($fieldName);
        $saveMethod = "save{$name}";
        $setMethod  = "set{$name}";

        if( method_exists($this, $saveMethod) ){
            $this->$saveMethod($value);
        }else{
            $this->$setMethod($value);
        }
    }

    /**
     * @description Общий класс который ищет в базе нужную entity.
     * @param string $fullEntityClassName
     * @param array $param
     * @throws \Exception
     * @return \Jazz\Entities\ENTITY_sys_Abstract
     */
    protected function doSaveFindEntity($fullEntityClassName, $param) {
        $em = $this->getEntityManager();

        if( !$param instanceof $fullEntityClassName ){
            $entityRepo = $em->getRepository($fullEntityClassName);

            //  Проверяем если переданный параметр типа integer
            //  то подразумеваем что сюда был переданн id зачит ищем по нему
            if( is_int($param) ){
                $entity = $entityRepo->findOneBy(array( 'id' => $param ));
                //  Проверяем если переданный параметр типа array
            }else if( is_array($param) ){

                //  проверим есть ли в переданном объекте ключ id,
                //  если да то ищем по нему.
                if( isset($param['id']) ){
                    $entity = $entityRepo->findOneBy(array( 'id' => $param['id'] ));
                    //  Иначе подразумеваем что нам передали набор параметров для поиска
                }else{
                    $entity = $entityRepo->findOneBy($param);
                }
            }

            else{
                throw new \Exception("Метод конвертации не работает с таким типом данных {$fullEntityClassName} {$param}");
            }
        }else{
            $entity = $param;
        }

        if( !isset($entity) ){
            throw new \Exception("Пытались найти сущность но не смогли. {$fullEntityClassName} {$param}");
        }
        return $entity;
    }

    /**
     * @description Общий класс который ищет в базе список прилинкованных entities.
     * @param $fullEntityClassName
     * @param array $param
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    protected function doSaveFindListEntity($fullEntityClassName, $param) {
        $list = array();

        foreach( $param as $row ){
            $list[] = $this->doSaveFindEntity($fullEntityClassName, $row);
        }

        return new \Doctrine\Common\Collections\ArrayCollection($list);
    }

    /**
     * @description Отвечает за механизм удаления сущности
     */
    public function remove(){
        \Jazz\Application::getInstance()->entityRemove($this)->entityFlush();
    }

    /**
     * @description Deletes all linked entities.
     * @param $repo
     * @param $fieldName
     * @param $value
     * @return ENTITY_sys_Abstract
     */
    protected function cleanEntities($repo, $fieldName, $value){
        $app = \Jazz\Application::getInstance();
        $repoName = ucfirst($repo) . 'Entity';
        $entities = $app->getEntityFindBy($repoName, array( $fieldName => $value ));

        if ($entities) {
            /**
             * @var $entity \Jazz\Entities\ENTITY_sys_Abstract
             */
            foreach ($entities as $entity) {
                $entity->remove();
            }
        }
        return $this;
    }

    /******************************************************************************************************************
     * Additional Methods / Вспомогательные методы
     ******************************************************************************************************************/
    /**
     * Метод определяет текущая сущность новая или же уже существует запись о ней в базе.
     * Собственно тут идет утверждение что если у сущности отсутствует id то она явно новая.
     * @return boolean
     */
    final public function isNewEntity() {
        $id = $this->getId();
        return !isset($id);
    }

    /**
     * Возварщает текущий активный экземпляр EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    final public function getEntityManager(){
        return \Jazz\Application::getInstance()->getEntityManager();
    }

    /**
     * Возвращает полное название класса ( с namespace )
     * @return string
     */
    final public function getFullClassName(){
        $className = $this->_fullClassName;

        if( !isset($className) ){
            $className = get_class($this);
            $this->_fullClassName = $className;
        }

        return $className;
    }

    /**
     * Возвращает сокращенное название класса ( без namespace )
     * @return string
     */
    final public function getShortClassName(){
        $className = $this->_shortClassName;

        if( !isset($className) ){
            $classNames = explode('\\', $this->getFullClassName());
            $className = $classNames[count($classNames) - 1];
            $this->_shortClassName = $className;
        }

        return $className;
    }

    /**
     * Имя сущности без последнего postfix - Entity
     * @return string
     */
    final public function getEntityName() {
        $entityName = $this->_entityName;

        if( !isset($entityName) ){
            $shortClassName = $this->getShortClassName();
            $entityName = preg_replace('/Entity$/', "", $shortClassName);
            $this->_entityName = $entityName;
        }

        return $entityName;
    }

    /**
     * Возвращает массив мета данных doctrine для данного класса
     * @return string
     */
    public function getClassMetaData() {
        $classMetaData = $this->_classMetaData;

        if( !isset($classMetaData) ){
            $classMetaData = $this->getEntityManager()->getClassMetadata($this->getFullClassName());
            $this->_classMetaData = $classMetaData;
        }

        return $classMetaData;
    }

    /**
     * Возвращает значение свойства fieldMappings ( список простых свойств ) из мета данных класса
     * @return array
     */
    public function getFieldMapping() {
        return $this->getClassMetaData()->fieldMappings;
    }

    /**
     * Возвращает значение свойства associationMappings ( список ссылочных свойств ) из мета данных класса
     * @return array
     */
    public function getAssociationMapping() {
        return $this->getClassMetaData()->associationMappings;
    }

    /**
     * Возвращает массив ключей для создания TRANSLATE_sys_MessageEntity
     * Используется только когда сущность создается
     * @return array
     */
    protected function getNewMessageEntities() {
        return $this->_newMessageEntities;
    }

    /**
     * Добавляет новый ключ ( имя свойства ) в массив со строками для перевода
     * @param string $propertyName Ключ ( имя свойства )
     * @return array Массив ключей для создания TRANSLATE_sys_MessageEntity
     */
    private function addNewMessageEntities($propertyName) {
        $this->_newMessageEntities[] = $propertyName;
        return $this->_newMessageEntities;
    }

    /**
     * Определяет есть ли записи для создания новых сущностей типа TRANSLATE_sys_MessageEntity
     * @return bool
     */
    private function isNewMessageEntity() {
        return count($this->_newMessageEntities) >= 0;
    }

    /**
     * Собственно создает новую TRANSLATE_any_Entity и добавляет ее в persist entityManager
     * @param integer $messageId Идентификатор сущности для которой делаем перевод
     * @param string $value Значение
     * @return TRANSLATE_any_Entity Экземпляр TRANSLATE_any_Entity
     */
    private function addNewTranslateEntities($messageId, $value) {
        $app = \Jazz\Application::getInstance();

        $translate = new TRANSLATE_any_Entity();
        $translate
            ->setMessageId($messageId)
            ->setLanguage($app->getCheckedLanguageEntity())
            ->setTranslate($value);
        $app->entityPersist($translate);

        return $translate;
    }

    /**
     * Метод отвечает за всю логику переводов полей в базе.
     * Собственно определяем если сущность новая - отложено создаем TRANSLATE_sys_MessageEntity
     * Если сущность редактируется то определяем по текущему языку и языку оригинала сообщения,
     * что именно мы хотим изменить. Если языки совпадают то мы хотим именить оригинальное сообщение, иначе изменяем
     * перевод
     * @param string $propertyKey
     * @param string $value
     * @return string
     */
    protected function setTranslateString($propertyKey, $value){
        $app = \Jazz\Application::getInstance();
        $cache = CacheService::getInstance();

        if( $this->isNewEntity() ){                                                                                     // Сущность создается если у нее отсутствует ID
            $this->addNewMessageEntities($propertyKey);                                                                 // Отложим создание TRANSLATE_sys_MessageEntity до полного сохранения
            $this->$propertyKey = $value;
        }else{                                                                                                          // Сущность явно прочитана из базы т.к. присутствует поле id
            /**
             * @var $message \Jazz\Entities\TRANSLATE_sys_MessageEntity
             */
            // Данный колбек сохраняяет в кеш язык оригинала для базового значения свойства
            $messageCache = $cache->getWithCallback(new MessageCacheCallback($this, $propertyKey));                     // array('originalLanguageSymbol', 'messageId' )
            $originalLanguageSymbol = $messageCache['originalLanguageSymbol'];                                          // Всегда возвращает символ языка оригинала сообщения
            $messageId = (int)$messageCache['messageId'];

            $currentLanguageSymbol = $app->getLanguage();

            if( $originalLanguageSymbol === $currentLanguageSymbol ){                                                   // Собственно сравниваем символы двух языков
                $this->$propertyKey = $value;                                                                           // Символы совпадают - кто-то ( пользователь, система ) изменяет оригинал переводимого сообщения
            }else{
                $message = $cache->getWithCallback(new TranslateMessageCacheCallback($value, $messageId));

                if( !$message ){
                    $this->addNewTranslateEntities($messageId, $value);                                                 // Позаботимся о создании строки с переводом ( а в value именно он )
                }else{
                    /**
                     * @var $translate \Jazz\Entities\TRANSLATE_any_Entity
                     */
                    $translate = $app->getEntityFind("TRANSLATE_any_Entity", $message['translateId'] );                      // По id полученному из кеша найдем нужный экземпляр

                    $translate
                        ->setMessageId($messageId)
                        ->setLanguage($app->getCheckedLanguageEntity())
                        ->setTranslate($value);

                    $app->entityPersist($translate);
                }
            }
        }

        return $this;
    }

    /**
     * Метод реализует логику получения строки перевода с условием текущего выбранного языка.
     * Если перевод не найден - вернет оригинальное сообщение
     * @param string $propertyKey
     * @return string
     */
    protected function getTranslateString($propertyKey){
        $cache = CacheService::getInstance();
        $message = $this->$propertyKey;
        /**
         * @var $message \Jazz\Entities\TRANSLATE_sys_MessageEntity
         */
        // Данный колбек сохраняяет в кеш язык оригинала для базового значения свойства
        $messageCache = $cache->getWithCallback(new MessageCacheCallback($this, $propertyKey));                         // array('originalLanguageSymbol', 'messageId' )
        $originalLanguageSymbol = $messageCache['originalLanguageSymbol'];                                              // Содержит символ языка оригинала сообщения

        $currentLanguageSymbol = \Jazz\Application::getInstance()->getLanguage();

        if( $originalLanguageSymbol !== $currentLanguageSymbol ){                                                       // Собственно сравниваем символы двух языков
            /**                                                                                                         // Если они не совпадают - берем перевод из кеша
             * @var $message \Jazz\Entities\TRANSLATE_any_Entity
             */
            $message = $cache->getWithCallback(new TranslateMessageCacheCallback($message, $messageCache['messageId']));

            $message = $message ? $message['message'] : $this->$propertyKey;
        }

        return $message;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param int $id
     * @return PAGE_sys_Entity
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    protected function getChangedField(){
        return $this->_changedFields;
    }

    /**
     * Нужно ли публиковать событие для этой сущности
     * @return bool
     */
    public function isTriggerEvent(){
        return !static::MUTE;                           // false - то публикуем иначе молчим
    }

    /**
     * Нужно ли публиковать событие "Create" для этой сущности
     * @return bool
     */
    public function isTriggerEventCreate(){
        return !static::MUTE && !static::MUTE_CREATE;   // false && false - то публикуем иначе молчим
    }

    /**
     * Нужно ли публиковать событие "Update" для этой сущности
     * @return bool
     */
    public function isTriggerEventUpdate(){
        return !static::MUTE && !static::MUTE_UPDATE;     // false && false - то публикуем иначе молчим
    }

    /**
     * Нужно ли публиковать событие "UpdateField" для этой сущности
     * @return bool
     */
    public function isTriggerEventUpdateField(){
        return !static::MUTE && !static::MUTE_UPDATE_FIELD;
    }
    /**
     * Нужно ли публиковать событие "Remove" для этой сущности
     * @return bool
     */
    public function isTriggerEventRemoveField(){
        return !static::MUTE && !static::MUTE_REMOVE;
    }

    /**
     * @param array $changedFields
     * @return ENTITY_sys_Abstract ${HINT}
     */
    public function setChangedFields($changedFields)
    {
        $this->_changedFields = $changedFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getChangedFields()
    {
        return $this->_changedFields;
    }
}