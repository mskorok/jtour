<?php

namespace Jazz\Entities;
use Jazz\Entities\USER_sys_Entity;

/**
 * @Entity
 * @Table(name="user_groups_entities")
 */
class USER_sys_GroupEntity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=256, nullable=true)
     * @var string
     */
    protected $descriptions;
    /**
     * @ManyToMany(targetEntity="USER_sys_Entity", mappedBy="groups")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $users;
    /**
     * @OneToMany(targetEntity="NOTIF_sys_SubscriberGroupEntity", mappedBy="group", cascade={"persist"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $subscribers;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->users        = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscribers  = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $users
     * @return USER_sys_GroupEntity
     */
    public function setUsers($users) {
        $this->users = $users;
        return $this;
    }

    /**
     * @param \Jazz\Entities\USER_sys_Entity $user
     * @return USER_sys_Entity
     */
    public function addUser($user) {
        $this->users->add($user);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * @param string $descriptions
     * @return USER_sys_GroupEntity
     */
    public function setDescriptions($descriptions) {
        $this->descriptions = $descriptions;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptions() {
        return $this->descriptions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $subscribers
     * @return USER_sys_GroupEntity
     */
    public function setSubscribers($subscribers) {
        $this->subscribers = $subscribers;
        return $this;
    }

    /**
     * @param \Jazz\Entities\NOTIF_sys_SubscriberGroupEntity $subscriber
     * @return USER_sys_GroupEntity
     */
    public function addSubscriber($subscriber) {
        $this->subscribers->add($subscriber);
        $subscriber->setGroup($this);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubscribers() {
        return $this->subscribers;
    }

}