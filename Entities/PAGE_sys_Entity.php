<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @Table(name="pages_entities")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"PAGE_sys_Entity" = "PAGE_sys_Entity", "PAGE_admin_AdminEntity" = "PAGE_admin_AdminEntity", "PAGE_site_UserEntity" = "PAGE_site_UserEntity"})
 */
class PAGE_sys_Entity extends \Jazz\Entities\ENTITY_sys_SimpleAbstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * @Column(type="string", length=512)
     * @description Url по которому будут обращаться к данной странице
     * @var string
     */
    protected $url;
    /**
     * @ManyToOne(targetEntity="PAGE_sys_ClassEntity", cascade={"persist"})
     * @JoinColumn(name="page_class_id", referencedColumnName="id")
     * @description Имя класса который отвечает за обработку данной сущности
     * @var \Jazz\Entities\PAGE_sys_ClassEntity
     */
    protected $pageClass;
    /**
     * @ManyToOne(targetEntity="SYS_sys_PackageEntity", cascade={"persist"})
     * @description Пакет которому принадлежит данная сущность
     * @var \Jazz\Entities\SYS_sys_PackageEntity
     */
    protected $systemPackage;
    /**
     * @OneToMany(targetEntity="MENU_sys_ItemEntity", mappedBy="page", cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $menuItems;

    /**
     * @var string
     */
    private $urlKey;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct() {
        parent::__construct();
        $this->menuItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->urlKey = "";
    }

    /**
     * @return array
     */
    public function getPageItemName(){
        return array();
    }

    public function getPageTemplate(){
        return "";
    }

    /**
     * "Декоративный" метод - возвращает имя Page Class при помощи которого строить данную страницу
     * @return string
     */
    public function getPageClassName(){
        return $this->getPageClass()->getClassName();
    }

    /**
     * "Декоративный" метод - возвращает заголовок SystemPackage, директория где хранятся страницы для текущего пакета
     * @return string
     */
    public function getPackageTitle(){
        return $this->getSystemPackage()->getTitle();
    }

    /**
     * "Декоративный" метод - возвращает имя SystemPackage, директория где хранятся страницы для текущего пакета
     * @return string
     */
    public function getPackageName(){
        return ucfirst($this->getSystemPackage()->getName());
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @param string $url
     * @return PAGE_sys_Entity
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param \Jazz\Entities\SYS_sys_PackageEntity $systemPackage
     * @return PAGE_sys_Entity
     */
    public function setSystemPackage($systemPackage) {
        $this->systemPackage = $systemPackage;
        return $this;
    }

    /**
     * @return \Jazz\Entities\SYS_sys_PackageEntity
     */
    public function getSystemPackage() {
        return $this->systemPackage;
    }

    /**
     * @param string $urlKey
     * @return PAGE_sys_Entity
     */
    public function setUrlKey($urlKey) {
        $this->urlKey = $urlKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlKey() {
        return $this->urlKey;
    }

    /**
     * @param \Jazz\Entities\PAGE_sys_ClassEntity $pageClass
     * @return PAGE_sys_Entity
     */
    public function setPageClass($pageClass) {
        $this->pageClass = $pageClass;
        return $this;
    }

    /**
     * @return \Jazz\Entities\PAGE_sys_ClassEntity
     */
    public function getPageClass() {
        return $this->pageClass;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $menuItems
     * @return PAGE_sys_Entity
     */
    public function setMenuItems($menuItems) {
        $this->menuItems = $menuItems;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMenuItems() {
        return $this->menuItems;
    }


}