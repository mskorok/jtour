<?php
namespace Jazz\Entities;

/**
 * @Entity
 * @Table(name="page_classes_entities")
 */
class PAGE_sys_ClassEntity extends \Jazz\Entities\ENTITY_sys_Abstract{
    /******************************************************************************************************************
     * Data Base Property / Свойства в базе данных
     ******************************************************************************************************************/
    /**
     * Название класса
     * @Column(type="string", length=128)
     * @var string
     */
    protected $className;

    /******************************************************************************************************************
     * Constructor / Конструктор
     ******************************************************************************************************************/
    public function __construct($className = "") {
        parent::__construct();
        $this->className = $className;
    }

    /******************************************************************************************************************
     * Getters and Setters for properties / Геттеры и Сеттеры для свойств
     ******************************************************************************************************************/
    /**
     * @description Данный метод должен обязательно быть у всех сущностей.
     *              По сути он используется в EntityGridRowBuilder для отображения Заголовка сущности
     * @return string
     */
    public function getTitle() {
        return $this->getClassName();
    }

    /**
     * @param string $className
     * @return PAGE_sys_ClassEntity
     */
    public function setClassName($className) {
        $this->className = $className;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName() {
        return $this->className;
    }
}